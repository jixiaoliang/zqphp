<?php

use  think\facade\Db;

class index
{
    public function main() {
        echo 'Hello';
    }

    public function think() {
        Db::setConfig([
            'default' => 'mysql', // 默认数据连接标识
            'connections' => [

                'mysql' => [

                    'type' => 'mysql', //数据库类型

                    'hostname' => '127.0.0.1',//Mysql IP

                    'username' => 'root', //Mysql用户名

                    'password' => 'root',//Mysql密码

                    'database' => 'zqphpmysql', //数据库名字

                    'charset' => 'utf8'  //编码
                ]
            ]

        ]);
        $list = Db::table('table_userlist')->select();
        ps('=========dump输出=========');
        dump($list);
        ps('=========dumpJson输出=========');
        dumpJson($list);
    }
}