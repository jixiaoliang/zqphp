<?php

/**
 * 获取文件名 // @=zqphp框架目录，#=启动目录，~=项目目录，/=View根目录
 * 通过require来引入
 * 默认类名/方法名
 * @param null|string|bool $File //文件名 |是否生成静态
 * @param bool $Type //true=生成静态  false关闭生成静态
 * @return string
 */
function ViewFile($File = null, $Type = false) {
    return Frame::ViewFile($File, $Type);
}

/**
 * 获取Get参数(分隔符后的参数)
 * @param int|null $Key //从0起
 * @param null $Default
 * @return mixed|null
 */
function GetWeb($Key = null, $Default = null) {
    return Frame::GetWeb($Key, $Default);
}

/**
 * 获取路由绑定域名数据
 * @param null $Default //没有数据时的默认值
 * @return mixed
 */
function RoutingData($Default = null) {
    return Frame::RoutingData($Default);
}

/**
 * 完整类名指向文件名 (只支持)
 * @param string|array $Name //完整类名  [完整类名=>'文件名',完整类名=>'文件名']
 * @param string $Data //文件名
 */
function SetClassFile($Name, $Data = null) {
    Frame::SetClassFile($Name, $Data);
}

/**
 * 命名空间指向文件夹
 * @param string|array $Name //命名空间   [命名空间=>'文件夹',命名空间=>['文件夹1','文件夹2']]
 * @param string|array $Data //文件夹
 */
function SetClassDir($Name, $Data = null) {
    Frame::SetClassDir($Name, $Data);
}

/**
 * 发送文件内容
 * @param $FilePlace //发送文件路径
 * @param bool $FileName //true=加入默认文件名  可以直接定新文件名,简单说是否开放下载
 */
function SendFile($FilePlace, $FileName = false) {
    Frame::SendFile($FilePlace, $FileName);
}

/**
 * 文件流
 * @param $FilePlace //文件
 * @param bool $FileName //true=加入默认文件名  可以直接定新文件名,简单说是否开放下载  //数字，发送大小
 * @param int $SendSize //发送大小
 */
function FileStream($FilePlace, $FileName = false, $SendSize = 8192) {
    Frame::FileStream($FilePlace, $FileName, $SendSize);
}

/**
 * 发送文件内容
 * @param $FileData //发送文件内容
 * @param $Format //文件格式或者文件名
 */
function SendData($FileData, $Format = null) {
    Frame::SendData($FileData, $Format);
}

/**
 * 设置头部状态
 * @param $Data
 */
function SetStatus($Data = 200) {
    Frame::SetStatus($Data);
}

/**
 * 设置头部信息
 * @param int|string|array $Name
 * @param null|bool|string $Data
 */
function SetHead($Name, $Data = null) {
    Frame::SetHead($Name, $Data);
}

/**
 * 设置Mime
 * @param $Data
 */
function SetMime($Data) {
    Frame::SetMime($Data);
}

/**
 * 获取Mime
 * @param null|string $Key //key键名
 * @param null|string|array|int|bool $Default //不存在时返回默认值
 * @return array|int|string|null
 */
function GetMime($Key = null, $Default = null) {
    return Frame::GetMime($Key, $Default);
}

/**
 * 获取zqphp目录位置
 * @return string
 */
function LoadDir() {
    return Frame::LoadDir();
}

/**
 * 获取启动目录位置
 * @return string
 */
function StartDir() {
    return Frame::StartDir();
}

/**
 * 判断字符串是否json,返回array
 * @param string $Data
 * @param bool $Type
 * @return array|mixed|object
 */
function IsJson($Data, $Type = true) {
    return Frame::IsJson($Data, $Type);
}

/**
 * 数组转Json格式化
 * @param $Data
 * @param bool $Type //是否强制int
 * @return false|string
 */
function JsonFormat($Data, $Type = true) {
    return Frame::JsonFormat($Data, $Type);
}

/**
 * 获取缓冲区内容
 * @param $Method //闭包
 * @return false|string
 */
function ObStart($Method) {
    return Frame::ObStart($Method);
}

/**
 * 程序正常输出
 * @param $Data
 * @throws \Exception
 */
function come($Data = null) {
    Frame::Come($Data);
}

/**
 * 浏览器友好的变量输出
 * @param mixed $Data 变量
 * @param boolean $Type 是否输出(默认为 true，为 false 则返回输出字符串)
 * @return null|string
 */
function dump($Data, $Type = true) {
    return Frame::dump($Data, $Type);
}

/**
 * 调试  json友好输出
 * @param $Data //json数据或者array   (只支持数组和Json)
 * @param bool $Type //是否输出(默认为 true，为 false 则返回输出字符串)
 * @return string|string[]|void|null
 */
function dumpJson($Data, $Type = true) {
    return Frame::dumpJson($Data, $Type);
}

/**
 * <pre>输出
 * @param mixed $Data 变量
 * @param bool $Type 是否输出(默认为 true，为 false 则返回输出字符串)
 * @return mixed
 */
function ps($Data, $Type = true) {
    return Frame::ps($Data, $Type);
}