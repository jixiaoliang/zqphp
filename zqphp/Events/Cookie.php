<?php

use zqphp\AutoLoad;

class Cookie {
    /**
     * 单个存储
     * 多个存储格式：[[...,...,...,...],[...,...,...,...],[...,...,...,...]]
     * @param string|array $Key cookie名称
     * @param string $Value cookie值
     * @param int $MaxAge cookie过期时间
     * @param string $Path 有效的服务器路径
     * @param string $Domain 有效域名/子域名
     * @param bool $Secure 是否仅仅通过HTTPS
     * @param bool $Only 仅可通过HTTP访问
     */
    public static function set($Key, $Value = null, $MaxAge = 0, $Path = '', $Domain = '', $Secure = false, $Only = false) {
        AutoLoad::SelfFun('SetCookie', [$Key, $Value, $MaxAge, $Path, $Domain, $Domain, $Secure, $Secure, $Only]);
    }

    /**
     * 获取
     * @param null|string $Key //null获取全部
     * @param null $Default
     * @return mixed
     */
    public static function get($Key = null, $Default = null) {
        return AutoLoad::SelfFun('GetCookie', [$Key, $Default]);
    }

    /**
     * 单个、多个、删除数据
     * @param null|array|string $Key //null=全部删除
     */
    public static function delete($Key = null) {
        AutoLoad::SelfFun('DeleteCookie', [$Key]);
    }

    /**
     * 获取并删除
     * @param string $Key
     * @return mixed
     */
    public static function pull($Key) {
        return AutoLoad::SelfFun('PullCookie', [$Key]);
    }

    /**
     * 判断是否存在
     * @param string $Key //不管有没值，key存在就返回true
     * @return mixed
     */
    public static function has($Key) {
        return AutoLoad::SelfFun('HasCookie', [$Key]);
    }
}