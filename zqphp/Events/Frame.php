<?php

use \zqphp\AutoLoad;

class Frame {
    /**
     * 获取文件名 // @=zqphp框架目录，#=启动目录，~=项目目录，/=View根目录
     * 通过require来引入
     * 默认类名/方法名
     * @param null|string|bool $File //文件名 |是否生成静态
     * @param bool $Type //true=生成静态  false关闭生成静态
     * @return string
     */
    public static function ViewFile($File = null, $Type = false) {
        return AutoLoad::ViewFile($File, $Type);
    }

    /**
     * 获取Get参数(分隔符后的参数)
     * @param int|null $Key //从0起
     * @param null $Default
     * @return mixed|null
     */
    public static function GetWeb($Key = null, $Default = null) {
        return AutoLoad::GetWeb($Key, $Default);
    }

    /**
     * 获取路由绑定域名数据
     * @param null $Default //没有数据时的默认值
     * @return mixed
     */
    public static function RoutingData($Default = null) {
        return AutoLoad::RoutingData($Default);
    }

    /**
     * 完整类名指向文件名 (只支持)
     * @param string|array $Name //完整类名  [完整类名=>'文件名',完整类名=>'文件名']
     * @param string $Data //文件名
     */
    public static function SetClassFile($Name, $Data = null) {
        AutoLoad::SetClassFile($Name, $Data);
    }

    /**
     * 命名空间指向文件夹
     * @param string|array $Name //命名空间   [命名空间=>'文件夹',命名空间=>['文件夹1','文件夹2']]
     * @param string|array $Data //文件夹
     */
    public static function SetClassDir($Name, $Data = null) {
        AutoLoad::SetClassDir($Name, $Data);
    }

    /**
     * 发送文件内容
     * @param $FilePlace //发送文件路径
     * @param bool $FileName //true=加入默认文件名  可以直接定新文件名,简单说是否开放下载
     */
    public static function SendFile($FilePlace, $FileName = false) {
        AutoLoad::SendFile($FilePlace, $FileName);
    }

    /**
     * 文件流
     * @param $FilePlace //文件
     * @param bool $FileName //true=加入默认文件名  可以直接定新文件名,简单说是否开放下载  //数字，发送大小
     * @param int $SendSize //发送大小
     */
    public static function FileStream($FilePlace, $FileName = false, $SendSize = 8192) {
        AutoLoad::FileStream($FilePlace, $FileName, $SendSize);
    }

    /**
     * 发送文件内容
     * @param $FileData //发送文件内容
     * @param $Format //文件格式或者文件名
     */
    public static function SendData($FileData, $Format = null) {
        AutoLoad::SendData($FileData, $Format);
    }

    /**
     * 设置头部状态
     * @param $Data
     */
    public static function SetStatus($Data = 200) {
        AutoLoad::SetStatus($Data);
    }

    /**
     * 设置头部信息
     * @param int|string|array $Name
     * @param null|bool|string $Data
     */
    public static function SetHead($Name, $Data = null) {
        AutoLoad::SetHead($Name, $Data);
    }

    /**
     * 设置Mime
     * @param $Data
     */
    public static function SetMime($Data) {
        AutoLoad::SetMime($Data);
    }

    /**
     * 获取Mime
     * @param null|string $Key //key键名
     * @param null|string|array|int|bool $Default //不存在时返回默认值
     * @return array|int|string|null
     */
    public static function GetMime($Key = null, $Default = null) {
        return AutoLoad::GetMime($Key, $Default);
    }

    /**
     * 获取zqphp目录位置
     * @return string
     */
    public static function LoadDir() {
        return AutoLoad::LoadDir();
    }

    /**
     * 获取启动目录位置
     * @return string
     */
    public static function StartDir() {
        return AutoLoad::StartDir();
    }

    /**
     * 判断字符串是否json,返回array
     * @param string $Data
     * @param bool $Type
     * @return array|mixed|object
     */
    public static function IsJson($Data, $Type = true) {
        return AutoLoad::IsJson($Data, $Type);
    }

    /**
     * 数组转Json格式化
     * @param $Data
     * @param bool $Type //是否强制int
     * @return false|string
     */
    public static function JsonFormat($Data, $Type = true) {
        return AutoLoad::JsonFormat($Data, $Type);
    }

    /**
     * 获取缓冲区内容
     * @param $Method //闭包
     * @return false|string
     */
    public static function ObStart($Method) {
        return AutoLoad::ObStart($Method);
    }

    /**
     * 程序正常输出
     * @param $Data
     * @throws \Exception
     */
    public static function come($Data = null) {
        AutoLoad::Come($Data);
    }

    /**
     * 浏览器友好的变量输出
     * @param mixed $Data 变量
     * @param boolean $Type 是否输出(默认为 true，为 false 则返回输出字符串)
     * @return null|string
     */
    public static function dump($Data, $Type = true) {
        return AutoLoad::dump($Data, $Type);
    }

    /**
     * 调试  json友好输出
     * @param $Data //json数据或者array   (只支持数组和Json)
     * @param bool $Type //是否输出(默认为 true，为 false 则返回输出字符串)
     * @return string|string[]|void|null
     */
    public static function dumpJson($Data, $Type = true) {
        return AutoLoad::dumpJson($Data, $Type);
    }

    /**
     * <pre>输出
     * @param mixed $Data 变量
     * @param bool $Type 是否输出(默认为 true，为 false 则返回输出字符串)
     * @return mixed
     */
    public static function ps($Data, $Type = true) {
        return AutoLoad::ps($Data, $Type);
    }
}