<?php

namespace zqphp\Lib;

use PDO;
use PDOException;
use tools\RedisController;

class Mysql
{
    public $MyData = [];//sql生成对像
    public static $PdoCache = [];//数据库连接池
    public static $LinkPdo = [];//当前边接对像
    public static $basearray = false;//数据库配置信息

    /**
     * @param string $name 数据库配置名称
     * @param int $link 附加数据库选择
     * @return Mysql
     * @throws \Exception
     */
    public static function link($name = '', $link = 0)
    {
        $self = new self();
        if (!is_array($name)) $name = [$name, $link];

        $self->_add_MyData('ext', self::link_data(isset($name[0]) ? $name[0] : '', isset($name[1]) ? $name[1] : ''));
        return $self;
    }

    /**√使用union前使用代替table
     * @param $data
     * @param $union
     * @return $this
     */
    public function alias($data, $union = false)
    {
        $name = is_array($data) ? trim($this->_join($data), ',') : $this->tabname($data);
        $this->_add_MyData('alias', $name . (empty($union) ? ' union ' : ' union all '));
        return $this;
    }

    /**
     * √@param $data
     * @param bool $type
     * @return $this
     */
    public function union($data, $type = false)
    {
        $union = $this->_sel_MyData('union', []);
        if (!is_array($union)) $union = [];
        if (!is_array($data)) $data = [$data, $type];
        $union[] = $data;
        $this->_add_MyData('union', $union);
        return $this;
    }


    /**
     * √@param $data
     * @param string $type
     * @param array $where
     * @param string $orand
     * @return $this
     */
    public function join($data, $type = 'table', $where = [], $orand = 'and')
    {
        $join = $this->_sel_MyData('join', []);
        if (!is_array($join)) $join = [];
        if (count($data) == count($data, 1)) {
            $join[] = [
                'type' => $type,
                'table' => $data,
                'where' => $where,
                'orand' => $orand,
            ];
        } else {
            $join = array_merge($join, $data);
        }
        $this->_add_MyData('join', $join);
        return $this;
    }

    /**
     * √@param $data
     * @return $this
     */
    public function table($data)
    {
        $this->_add_MyData('table', $this->tabname($data));
        return $this;
    }

    /**
     * √@param $data
     * @return $this
     */
    public function tab($data)
    {
        $this->_add_MyData('table', $data);
        return $this;
    }

    /**
     * √@param $data
     * @param string $key
     * @return $this
     */
    public function field($data, $key = '')
    {
        if ($data !== true) {
            $field = $this->_sel_MyData('field', '');
            $field = !empty($field) ? $field . ',' . $data : $data;
            $this->_add_MyData('field', $field);
            $this->_add_MyData('key', $key);
        } else {
            $this->_add_MyData('key', true);
        }
        return $this;
    }

    /**
     * √mysql 下 计算 两点 经纬度 之间的距离
     * 可以再指向到mapsql输出sql放到field中使用
     * 方法1
     * $map_x=[字段=>经度,字段=>纬度,别名];
     * 方法2
     * @param array $map_x [字段=>经度]
     * @param array $map_y [字段=>纬度]
     * @param string $name as别名 默认map
     * @return $this
     */
    public function map($map_x, $map_y = [], $name = 'map')
    {
        $data = [];
        if (count($map_x) == 1) {
            $data = [$map_x, $map_y, $name];
        } else if (is_array($map_x) and count($map_x) > 1) {
            foreach ($map_x as $k => $v) $data[][$k] = $v;
        }
        $this->_add_MyData('map', $data);
        return $this;
    }

    /**
     * √@param $data
     * @return $this
     */
    public function key($data)
    {
        $this->_add_MyData('key', $data);
        return $this;
    }

    /**附加
     * @param $data
     * @return $this
     */
    public function affix($data)
    {
        $this->_add_MyData('affix', $data);
        return $this;
    }

    /**
     * √@param $data
     * @return $this
     */
    public function data($data)
    {
        $this->_add_MyData('data', $data);
        return $this;
    }

    /**
     * √@param $data
     * @param $andor
     * @return $this
     */
    public function where($data, $andor = 'and')
    {
        if (empty($data)) return $this;
        $where = (Where::where($data, $andor));
        $this->_add_MyData('where_1', ' where ' . $where[1]);
        $this->_add_MyData('where_2', ' where ' . $where[2]);
        $this->_add_MyData('where_data', isset($where[3]) ? $where[3] : []);
        return $this;
    }

    /**
     * √@param $data
     * @return $this
     */
    public function group($data)
    {
        if (empty($data)) return $this;
        $this->_add_MyData('group', ' group by ' . $data);
        return $this;
    }

    /**
     * √@param $data
     * @param string $andor
     * @return $this
     */
    public function having($data, $andor = 'and')
    {
        if (empty($data)) return $this;
        $having = (Where::where($data, $andor));
        $this->_add_MyData('having_1', ' having ' . $having[1]);
        $this->_add_MyData('having_2', ' having ' . $having[2]);
        $this->_add_MyData('having_data', isset($having[3]) ? $having[3] : []);
        //$this->_add_MyData('having', ' having ' . $data);
        return $this;
    }

    /**
     * √@param $data
     * @return $this
     */
    public function order($data)
    {
        if (empty($data)) return $this;
        $this->_add_MyData('order', ' order by ' . $data);
        return $this;
    }

    /**
     * √@param $offset
     * @param string $length
     * @return $this
     */
    public function limit($offset, $length = '')
    {
        $lengths = !empty ($length) ? $length : $offset;
        $offsets = !empty ($length) ? $offset : 0;
        $this->_add_MyData('limit', ' limit ' . $offsets . ',' . $lengths);
        return $this;
    }

    /**
     * √生成表单名称
     * @param $data
     * @param $field
     * @return string
     */
    public function tabname($data, $field = '')
    {
        $table = ($this->MyData['ext']['type'] != 'sqlite') ? $this->MyData['ext']['name'] . '.' . $this->MyData['ext']['table'] : $this->MyData['ext']['table'];
        if ($field) $field = 'select ' . $field . ' from ';
        return $field . $table . $data;
    }

    /**
     * 返回sql
     * @param string $mode
     * @return string
     * @throws \Exception
     */
    public function sql($mode = '2')
    {
        return $this->select($mode, 'sql');
    }

    /**
     * √添加
     * @param string $mode
     * @param string $type
     * @param string $come
     * @return array|mixed|string
     * @throws \Exception
     */
    public function insert($mode = '', $type = '', $come = '')
    {
//        ps($this);
        $field = $value = $_value = '';
        $arr_data = [];
        $_table = ' ' . $this->_sel_MyData('table');
        $_data = $this->_sel_MyData('data');
//        ps($_table);
//        exit;
        if (count($_data) == count($_data, 1)) {
            $_type = 'id,row,data';
            foreach ($_data as $k => $v) {
                $arr_data[] = $v;
                $field .= $k . ',';
                $value .= Where::_string($v) . ',';
                $_value .= '?,';
            }
        } else {
            $_type = 'row,id,data';
            foreach ($_data [0] as $k => $v) {
                $field .= $k . ',';
            }
            $var = $_var = '';
            foreach ($_data as $val) {
                $value = '(';
                $_value = '(';
                foreach ($val as $k => $v) {
                    $arr_data[] = $v;
                    $value .= Where::_string($v) . ',';
                    $_value .= '?,';
                }
                $var .= rtrim($value, ',') . '),';
                $_var .= rtrim($_value, ',') . '),';
            }
            $value = trim(trim(trim($var, ','), ')'), '(');
            $_value = trim(trim(trim($_var, ','), ')'), '(');
        }
        $field = trim($field, ',');
        $value = trim($value, ',');
        $_value = trim($_value, ',');
        $data = $this->handle([
            'sql' => ['1' => 'insert into' . $_table . ' (' . $field . ') values (' . $_value . ');', '2' => 'insert into' . $_table . ' (' . $field . ') values (' . $value . ');'],
            'data' => $arr_data,
        ], $mode, $type, $come, $_type);

        return $data;
    }

    /**
     * √修改
     * @param string $mode
     * @param string $type
     * @param string $come
     * @return array|mixed|string
     * @throws \Exception
     */
    public function update($mode = '', $type = '', $come = '')
    {
        $value = $_value = '';
        $arr_data = [];
        $_table = ' ' . $this->_sel_MyData('table');

        $_affix = $this->_sel_MyData('affix');

        $_data = $this->_sel_MyData('data', []);
        $_where_1 = $this->_sel_MyData('where_1');
        $_where_2 = $this->_sel_MyData('where_2');
        $_where_data = $this->_sel_MyData('where_data', []);

        if (count($_data) > 0) foreach ($_data as $k => $v) {
            $c = Where::_get_str($k, '[', ']');
            if (empty($c)) {
                $value .= $k . '=' . Where::_string($v) . ',';
                $_value .= $k . '=?,';
                $arr_data[] = $v;
            } else if ($c == '.') {
                $k = trim(str_replace('[' . $c . ']', '', trim($k)));
                $value .= $k . '=' . $v . ',';
                $_value .= $k . '=' . $v . ',';
            } else {
                $k = trim(str_replace('[' . $c . ']', '', trim($k)));
                $value .= $k . '=' . $k . $c . $v . ',';
                $_value .= $k . '=' . $k . $c . $v . ',';
            }
        }

        $data = $this->handle([
            'sql' => [
                '1' => 'update' . $_table . ' set ' . $_affix . trim($_value, ',') . $_where_1,
                '2' => 'update' . $_table . ' set ' . $_affix . trim($value, ',') . $_where_2
            ],
            'data' => array_merge($arr_data, $_where_data),
        ], $mode, $type, $come, 'row,data');
        return $data;
    }

    /**查询
     * @param string $mode 执行模式
     * @param string $type 返回类型
     * @param string $come sql/exec
     * @return array|mixed|string
     * @throws \Exception
     */
    public function select($mode = '', $type = '', $come = '')
    {
        return $this->_select($mode, $type, $come);
    }

    /**
     * √查询
     * @param string $mode 执行模式
     * @param string $type 返回类型
     * @param string $come sql/exec
     * @return array|mixed|string
     * @throws \Exception
     */
    public function find($mode = '', $type = '', $come = '')
    {
        $this->_add_MyData('length', 1);
        return $this->_select($mode, $type, $come);
    }

    /**
     * 查询
     * @param string $mode 执行模式
     * @param string $type 返回类型
     * @param string $come sql/exec
     * @return array|mixed|string
     * @throws \Exception
     */
    private function _select($mode = '', $type = '', $come = '')
    {
        $_field = $this->_sel_MyData('field', '*');
        if (!empty($_map = $this->mapsql(''))) $_field .= ',' . $_map;
        $_select = 'select ' . $_field . ' from ';
        $_table = $this->_sel_MyData('table');
        $_table .= $this->_sel_MyData('alias') . $this->tab_union() . ' ' . $this->tab_join();
        $_where_1 = $this->_sel_MyData('where_1');
        $_where_2 = $this->_sel_MyData('where_2');
        $_group = $this->_sel_MyData('group');
        $_having_1 = $this->_sel_MyData('having_1');
        $_having_2 = $this->_sel_MyData('having_2');
        $_order = $this->_sel_MyData('order');
        $_limit = $this->_sel_MyData('limit');
        $_where_data = $this->_sel_MyData('where_data', []);
        $_having_data = $this->_sel_MyData('having_data', []);
        $_data = [
            'sql' => [
                '1' => $_select . $_table . $_where_1 . $_group . $_having_1 . $_order . $_limit,
                '2' => $_select . $_table . $_where_2 . $_group . $_having_2 . $_order . $_limit
            ],
            'data' => array_merge($_where_data, $_having_data),
        ];

        $data = $this->handle($_data, $mode, $type, $come, 'list,data');
        return $data;
    }

    /**
     * √删除
     * @param string $mode
     * @param string $type
     * @param string $come
     * @return array|mixed|string
     * @throws \Exception
     */
    public function delete($mode = '', $type = '', $come = '')
    {
        $_table = ' from ' . $this->_sel_MyData('table');
        $_where_1 = $this->_sel_MyData('where_1');
        $_where_2 = $this->_sel_MyData('where_2');
        $_where_data = $this->_sel_MyData('where_data', []);
        $_data = ['sql' => ['1' => 'delete' . $_table . $_where_1, '2' => 'delete' . $_table . $_where_2], 'data' => $_where_data,];
        $data = $this->handle($_data, $mode, $type, $come, 'row,data');
        return $data;
    }

    /**
     * 统计记录 Marko Checked
     * @param string $field
     * @param string $mode
     * @param string $type
     * @param string $come
     * @return array|mixed|string
     * @throws \Exception
     */
    public function count($field = '*', $mode = '', $type = '', $come = '')
    {
        $field = !empty($field) ? $field : $this->_sel_MyData('data');
        return $this->aggregation('count', $field, $mode, $type, $come);
    }

    /**
     * √统计局列数总和
     * @param string $field
     * @param string $mode $field
     * @param string $type
     * @param string $come
     * @return array|mixed|string
     * @throws \Exception
     */
    public function sum($field = '', $mode = '', $type = '', $come = '')
    {
        $field = !empty($field) ? $field : $this->_sel_MyData('data');
        return $this->aggregation('sum', $field, $mode, $type, $come);
    }

    /**
     * √获得某个列字段的最大值
     * @param string $field
     * @param string $mode
     * @param string $type
     * @param string $come
     * @return array|mixed|string
     * @throws \Exception
     */
    public function max($field = '', $mode = '', $type = '', $come = '')
    {
        $field = !empty($field) ? $field : $this->_sel_MyData('data');
        return $this->aggregation('max', $field, $mode, $type, $come);
    }

    /**
     * √获得某个列字段的最小值
     * @param string $field
     * @param string $mode
     * @param string $type
     * @param string $come
     * @return array|mixed|string
     * @throws \Exception
     */
    public function min($field = '', $mode = '', $type = '', $come = '')
    {
        $field = !empty($field) ? $field : $this->_sel_MyData('data');
        return $this->aggregation('min', $field, $mode, $type, $come);
    }

    /**
     * √获得某个列字段的平均值
     * @param string $field
     * @param string $mode
     * @param string $type
     * @param string $come
     * @return array|mixed|string
     * @throws \Exception
     */
    public function avg($field = '', $mode = '', $type = '', $come = '')
    {
        $field = !empty($field) ? $field : $this->_sel_MyData('data');
        return $this->aggregation('avg', $field, $mode, $type, $come);
    }

    /**
     * √聚合
     * @param string $text
     * @param string $field
     * @param string $mode
     * @param string $type
     * @param string $come
     * @return array|mixed|string
     * @throws \Exception
     */
    private function aggregation($text, $field, $mode = '', $type = '', $come = '')
    {
        $_select = 'select ' . $text . '(' . $field . ')' . ' as count from ';
        $_table = $this->_sel_MyData('table');
        $_table .= $this->tab_join() . $this->_sel_MyData('alias') . $this->tab_union();
        $_where_1 = $this->_sel_MyData('where_1');
        $_where_2 = $this->_sel_MyData('where_2');
        $_group = $this->_sel_MyData('group');
        $_having_1 = $this->_sel_MyData('having_1');
        $_having_2 = $this->_sel_MyData('having_2');
        $_order = $this->_sel_MyData('order');
        $_limit = $this->_sel_MyData('limit');
        $_where_data = $this->_sel_MyData('where_data', []);
        $_having_data = $this->_sel_MyData('having_data', []);
        $_data = [
            'sql' => [
                '1' => $_select . $_table . $_where_1 . $_group . $_having_1 . $_order . $_limit,
                '2' => $_select . $_table . $_where_2 . $_group . $_having_2 . $_order . $_limit
            ],
            'data' => array_merge($_where_data, $_having_data)
        ];
        $data = $this->handle($_data, $mode, $type, $come, 'list');
        return isset($data[key($data)]['count']) ? $data[key($data)]['count'] : $data;
    }

    /**
     * 生成执行
     * @param $info
     * @param $mode
     * @param $type
     * @param $come
     * @param string $array
     * @return array|mixed|string
     * @throws \Exception
     */
    private function handle($info, $mode, $type, $come, $array = '')
    {
        $ext = $this->_sel_MyData('ext');

        $mte = self::mte($ext['mode'], $mode, $type, $come, $array);

        $mte['name'] = $ext['data_name'];
        $mte['length'] = $this->_sel_MyData('length');
        $mte['key'] = $this->_sel_MyData('key');
        if (empty($this->_sel_MyData('DelData', false))) $this->MyData = [];
        $data = ['ext' => $mte, 'sql' => $info['sql'], 'data' => $info['data']];
        return ($mte['come'] == 'demo') ? $data : (($mte['come'] == 'sql') ? $data['sql'][$mte['mode']] : self::exec($data));
    }

    /**
     * @param string $sql
     * @return string
     */
    public function mapsql($sql = '')
    {
        if (!empty($data = $this->_sel_MyData('map'))) {
            $mapxy = function ($map) {
                if (is_array($map) and count($map) > 0) {
                    $key_x = key($map);
                    return ['key' => $key_x, 'data' => $map[$key_x]];
                }
                return false;
            };
            $mapx = $mapxy(isset($data[0]) ? $data[0] : '');
            $mapy = $mapxy(isset($data[1]) ? $data[1] : '');
            $_name = $mapxy(isset($data[2]) ? $data[2] : '');
            $name = isset($_name) ? $_name['data'] : 'map';
            if (!empty($mapx) and !empty($mapy)) {
//                $lon = 'POW(SIN((' . $mapx['data'] . ' * PI() / 180 - ' . $mapx['key'] . ' * PI() / 180) / 2),2)';
//                $lat = 'COS(' . $mapy['data'] . ' * PI() / 180) * COS(' . $mapy['key'] . ' * PI() / 180)+POW(SIN((' . $mapy['data'] . ' * PI() / 180 - ' . $mapy['key'] . ' * PI() / 180) / 2),2)';
//                $map = '6378.137 * 2 * ASIN(SQRT(' . $lon . '*' . $lat . ')) * 1000';
                $map = "6371000 * ACOS(COS( {$mapx['data']} * PI( ) / 180 ) * COS( {$mapx['key']} * PI( ) / 180 ) * COS( {$mapy['data']} * PI( ) / 180 - {$mapy['key']} * PI( ) / 180 ) + SIN( {$mapx['data']} * PI( ) / 180 ) * SIN( {$mapx['key']} * PI( ) / 180 ))";
                $sql .= 'ROUND(' . $map . ') AS ' . $name;
            }
        }
        $this->_sel_MyData('map', '');
        return $sql;
    }

    /**返回join
     * @return string
     */
    private function tab_join()
    {
        $_join = '';
        $data = $this->_sel_MyData('join');
        if (is_array($data) and count($data) > 0) foreach ($data as $k => $v) {
            $table = isset($v['table']) ? $v['table'] : '';
            if (!empty($table)) {
                $type = isset($v['type']) ? $v['type'] : '';
                $type = !empty($type) ? $type : 'table';
                $where = isset($v['where']) ? $v['where'] : [];
                $orand = isset($v['orand']) ? $v['orand'] : '';
                if ($type != 'table') $_join = trim($_join, ',') . ' ';
                $_join .= $this->_join($v['table'], $type, $where, $orand);
            }
        }
        return trim(trim($_join), ',');
    }

    /**处理join
     * @param $data
     * @param string $type
     * @param array $where
     * @param string $orand
     * @return string
     */
    private function _join($data, $type = 'table', $where = [], $orand = 'and')
    {
        $tab = [];
        if (!is_array($data)) $data = ['1' => $data];
        foreach ($data as $k => $w) {
            $c = Where::_get_str($k, '[', ']');
            $k = trim(str_replace('[' . $c . ']', '', trim($k)));
            if ($c == 'true') $w = $this->tabname($w);
            $tab[] = $w . (is_numeric($k) ? '' : ' as ' . $k);
        }
        if ($type == 'table') {
            $sql = join(',', $tab) . ',';
        } else {
            $we = Where::where($where, $orand);
            $wes = (!empty($we[2]) ? ' ' . $we[2] : '');
            if (count($tab) == 1) {
                $tab_1 = '';
                $tab_2 = isset($tab[0]) ? ' ' . $tab[0] . ' ' : '';
            } else {
                $tab_1 = isset($tab[0]) ? $tab[0] . ' ' : '';
                $tab_2 = isset($tab[1]) ? ' ' . $tab[1] . ' ' : '';
            }
            switch ($type) {
                case 'inner':
                case 'full':
                case 'left':
                case 'right':
                    $sql = $tab_1 . $type . ' join' . $tab_2 . 'on' . $wes;
                    break;
                default:
                    $sql = $tab_1 . 'join' . $tab_2 . 'on' . $wes;
                    break;
            }
        }
        return $sql;
    }

    /**返回union
     * @return mixed|string
     */
    private function tab_union()
    {
        $_union = $this->_sel_MyData('union', '');
        if (!empty($_union)) $_union = self::_unionall($_union);
        return $_union;
    }

    /**返回union处理结果
     * @param $data
     * @param bool $type
     * @return string
     */
    private static function _unionall($data, $type = false)
    {
        $info = self::_union($data, $type);
        $sql = (is_array($data) and count($data) > 1) ? '(' . $info['text'] . ')' : $info['text'];
        return $sql;
    }

    /**处理union
     * @param $data
     * @param $type
     * @return array
     */
    private static function _union($data, $type = false)
    {
        if (empty($sql)) $sql = ['text' => '', 'type' => false];
        $ov = count($data);
        foreach ($data as $k => $v) {
            if (is_array($v)) {
                if (count($v) == 2 and !is_string(end($v))) {
                    $text['text'] = $sql['text'] . (isset($v[0]) ? $v[0] : '');
                    $text['type'] = isset($v[1]) ? $v[1] : $type;
                } else {
                    $text = self::_union($v, $type);
                }
                $sql['text'] .= is_numeric($k) ? $text['text'] : '(' . $text['text'] . ') as ' . $k;
                $sql['type'] = $text['type'];
            } else {
                $sql['text'] .= is_numeric($k) ? $v : '(' . $v . ') as ' . $k;
                $sql['type'] = $type;
            }
            --$ov;
            if ($ov > 0) $sql['text'] .= (empty($sql['type']) ? ' union ' : ' union all ');
        }
        return $sql;
    }

    /**
     * @param array $data
     * @param array $type 返回类型/数据
     * @param array $new_data 数据
     * @return array|mixed
     * @throws \Exception
     */
    public static function exec($data, $type = [], $new_data = [])
    {
        if (count($data)) {
            $type_data = !empty($new_data) ? $new_data : $type;
            $new_data = is_array($type_data) ? $type_data : [];
            $type = is_array($type) ? $data['ext']['type'] : $type;
            if (count($new_data) == count($data['data'])) {
                $data['data'] = $new_data;
                $data['ext']['mode'] = 1;
            }
            $data = ($data['ext']['mode'] == 1) ? self::prepare($data['sql'][1], $data['data'], $type, $data['ext']['name'], $data['ext']['length'], $data['ext']['key']) : $data = self::query($data['sql'][2], $type, $data['ext']['name'], $data['ext']['length'], $data['ext']['key']);
        }
        return $data;
    }

    /**数据分页
     * @param int $count 分页数量
     * @param string $page 分页数
     * @return mixed
     * @throws \Exception
     */
    public function page($count, $page = '')
    {
        $this->_add_MyData('DelData', true);
        $_data['page'] = self::_page($this->count(), $count, $page);
        $this->_add_MyData('DelData', false);
        $_data['list'] = $this->limit($_data['page']['head'], $_data['page']['count'])->select();
        return $_data;
    }

    /**数组内容分页计算
     * @param int $totals 总数
     * @param int $count 要分多少页
     * @param string $page 当前页数
     * @return array
     */
    public static function _page($totals, $count, $page = '')
    {
        $arr_sql = array();
        $_page = !empty($_REQUEST ['page']) ? $_REQUEST ['page'] : 1;
        $page = !empty ($page) ? $page : $_page;
        $page = !empty ($page) ? trim($page) : 1;
        $count_page = @ceil($totals / $count);
        $up_page = 1;
        if ($page > 1) $up_page = $page - 1;
        $next_page = $count_page;
        if ($page < $count_page) $next_page = $page + 1;
        if ($page == 1) $up_page = 0;
        if ($page == $count_page) $next_page = 0;
        $arr_sql ['total'] = intval($totals); // 记录总数
        $arr_sql ['pages'] = $count_page; // 总页数
        $arr_sql ['top'] = $up_page; // 上一页
        $arr_sql ['next'] = $next_page; // 下一页
        $arr_sql ['page'] = intval($page); // 当前页数
        $arr_sql ['count'] = intval($count); // 每页个数
        $arr_sql ['head'] = (($page - 1) * $count); // 第几个起
        return $arr_sql;
    }

    /**
     * @param string $sql 语句
     * @param array $data 数据
     * @param string $type 返回类型
     * @param string $name 数据库配置名称
     * @param int $length
     * @param string $key
     * @return array|mixed
     * @throws \Exception
     */
    public static function prepare($sql, $data = [], $type = '', $name = '', $length = 0, $key = '')
    {

        $_pdo_ = self::pdo_sel($name);
        $pdo = $_pdo_['pdo'];
        $res = $pdo->prepare($sql);
        $res->execute($data);
        return self::res_data($pdo, $res, $type, $length, $key);
        /*
        $_pdo_ = self::pdo_sel($name);
        $pdo = $_pdo_['pdo'];
        try {
            $res = (object)$pdo->prepare($sql);
            @$res->execute($data);
            $data = self::res_data($pdo, $res, $type, $length, $key);
            return $data;
        } catch (PDOException $e) {
            if ($e->errorInfo[1] == 2006 or $e->errorInfo[1] == 2013) {
                try {
                    $pdo = NULL;
                    $base = self::my_data($name);
                    $pdo = self::$LinkPdo[self::pdo_name()][$base['ext']['data_name']]['pdo'] = self::link_base($base['base']);
                    $res = $pdo->prepare($sql);
                    $res->execute($data);
                    $data = self::res_data($pdo, $res, $type, $length, $key);
                    return $data;
                } catch (PDOException $ex) {
                    throw $ex;
                }
            }
            throw $e;
        }
        */
    }

    /**
     * @param string $sql 语句
     * @param string $type 返回类型
     * @param string $name 数据库配置名称
     * @param int $length
     * @param string $key
     * @return mixed
     * @throws \Exception
     */
    public static function query($sql, $type = '', $name = '', $length = 0, $key = '')
    {
        $_pdo_ = self::pdo_sel($name);
        $pdo = $_pdo_['pdo'];
        $res = $pdo->query($sql);
        return self::res_data($pdo, $res, $type, $length, $key);
        /*
        $_pdo_ = self::pdo_sel($name);
        $pdo = $_pdo_['pdo'];
        try {
            $res = @$pdo->query($sql);
            $data = self::res_data($pdo, $res, $type, $length, $key);
            return $data;
        } catch (PDOException $e) {
            if ($e->errorInfo[1] == 2006 or $e->errorInfo[1] == 2013) {
                try {
                    $pdo = NULL;
                    $base = self::my_data($name);
                    $pdo = self::$LinkPdo[self::pdo_name()][$base['ext']['data_name']]['pdo'] = self::link_base($base['base']);
                    $res = $pdo->query($sql);
                    $data = self::res_data($pdo, $res, $type, $length, $key);
                    return $data;
                } catch (PDOException $ex) {
                    throw $ex;
                }
            }
            throw $e;
        }
        */
    }

    /**
     * @param $sql
     * @param string $name
     * @return int
     * @throws \Exception
     */
    public static function execs($sql, $name = '')
    {
        $_pdo_ = self::pdo_sel($name);
        $pdo = $_pdo_['pdo'];
        return $pdo->exec($sql);
        /*
        $_pdo_ = self::pdo_sel($name);
        $pdo = $_pdo_['pdo'];
        try {
            $res = @$pdo->exec($sql);
            return $res;
        } catch (PDOException $e) {
            if ($e->errorInfo[1] == 2006 or $e->errorInfo[1] == 2013) {
                try {
                    $pdo = NULL;
                    $base = self::my_data($name);
                    $pdo = self::$LinkPdo[self::pdo_name()][$base['ext']['data_name']]['pdo'] = self::link_base($base['base']);
                    $res = @$pdo->exec($sql);
                    return $res;
                } catch (PDOException $ex) {
                    throw $ex;
                }
            }
            throw $e;
        }
        */
    }

    /**
     * @param object $pdo
     * @param object $res
     * @param string $type
     * @param string $length
     * @param string $key
     * @return mixed
     */
    private static function res_data($pdo, $res, $type, $length = '', $key = '')
    {
        switch ($type) {
            case 'list' :
                if ($key === true and $length != '1') {
                    $data = $res->fetchAll(PDO::FETCH_UNIQUE | PDO::FETCH_ASSOC);
                } else {
                    $fetchAll = $res->fetchAll(PDO::FETCH_ASSOC);
                    $data = $fetchAll;
                    if (count($fetchAll) > 0) {
                        if ($length == '1') {
                            $data = $fetchAll [key($fetchAll)];
                            if (count($data) == 1) $data = $data [key($data)];
                        } else if (!empty($key)) {
                            $data = [];
                            $i = 0;
                            foreach ($fetchAll as $k => $v) {
                                $vkey = isset($fetchAll[key($fetchAll)][$key]) ? $v[$key] : $i;
                                $data[$vkey] = $v;
                                ++$i;
                            }
                        }
                    }
                }
                break;
            case 'id' :
                $data = $pdo->lastInsertId();
                break;
            case 'row' :
                $data = $res->rowCount();
                break;
            default :
                $data = $res;
                break;
        }
        return $data;
    }

    /**
     * @param $name
     * @param string $data
     */
    private function _add_MyData($name, $data = '')
    {
        $this->MyData[$name] = $data;
//        ps($this);
    }

    /**
     * @param $name
     * @param string $data
     * @return mixed|string
     */
    private function _sel_MyData($name, $data = '')
    {
        return !empty($this->MyData[$name]) ? $this->MyData[$name] : $data;
    }

    /**执行选择器
     * @param $_mode
     * @param $mode
     * @param $type
     * @param $come
     * @param $array
     * @return array
     */
    private static function mte($_mode, $mode, $type, $come, $array)
    {
        $ret_type = function ($type) use ($array) {
            $arr = is_array($array) ? $array : explode(',', $array);
            $data = !empty (in_array($type, $arr)) ? $type : $arr [key($arr)];
            return $data;
        };
        $modes = is_numeric($mode) ? (($mode > 2) ? $_mode : $mode) : $_mode;
        $types = is_numeric($mode) ? $ret_type($type) : $ret_type($mode);
        $comes = (!empty ($come) ? $come : (!empty ($type) ? $type : (!empty ($mode) ? $mode : 'exec')));
        return ['mode' => $modes, 'type' => $types, 'come' => $comes];
    }

    /**
     * @return object
     * @throws \Exception
     */
    public function affair_pdo()
    {
        $ext = $this->_sel_MyData('ext');
        $ext = !empty($ext) ? $ext : self::link_data();
        $name = $ext['data_name'];
        $_pdo_ = self::pdo_sel($name);
        return $_pdo_['pdo'];
    }

    /**开启事务
     * @throws \Exception
     */
    public function beg()
    {
        $this->affair_pdo()->beginTransaction();
    }

    /**提交事务
     * @throws \Exception
     */
    public function com()
    {
        $this->affair_pdo()->commit();
    }

    /**回滚事务
     * @throws \Exception
     */
    public function rol()
    {
        $this->affair_pdo()->rollBack();
    }

    /**只支持同一个数据库配置使用
     * 事务调用不可以跨配置，只能同一配置使用,调用前请使用link
     * 跨配置请配合beg/com/rol使用
     * @param $method
     * @return bool
     * @throws \Exception
     */
    public function affair($method)
    {
        $pdo = $this->affair_pdo();
        if (is_callable($method)) {
            $pdo->beginTransaction();
            try {
                $data = $method ();
                if ($data === false) {
                    $pdo->rollBack();
                } else {
                    $pdo->commit();
                }
                return $data;
            } catch (PDOException $e) {
                $pdo->rollBack();
                return false;
            }
        }
        return false;
    }

    /**生成标识
     * @return bool|string
     */
    public static function pdo_name()
    {
        ObjCount::$prefix = 'M';
        return ObjCount::gen_name('Mysql', 'PDO');
    }

    /**取数据库连接
     * @param string $name
     * @return array
     * @throws \Exception
     */
    public static function pdo_sel($name = '')
    {

        $PdoObj = function () use ($name) {

            $base = self::my_data($name);

            $name = !empty($name) ? $name : $base['ext']['data_name'];

            $count = !empty(self::$PdoCache[$name]) ? count(self::$PdoCache[$name]) : 0;

            if (!$count) self::$PdoCache[$name][] = self::link_base($base['base']);

            $data = self::$PdoCache[$name];

            return ['pdo' => $data[key($data)], 'link' => $base['ext']['link']];

        };

        // 判断当前这个 pdo 是否有效连接
        try {
            $pdoarr = $PdoObj();
            $pdo = $pdoarr['pdo'];
            @$pdo->getAttribute(PDO::ATTR_SERVER_INFO);
            return $pdoarr;
        } catch (PDOException $e) {
            self::$PdoCache[$name] = [];
            return $PdoObj();
        }


        /*
        $base = self::my_data($name);
        $name = !empty($name) ? $name : $base['ext']['data_name'];
        $link = $base['ext']['link'];
        $count = !empty(self::$PdoCache[$name]) ? count(self::$PdoCache[$name]) : 0;
        if (!$count) for ($i = 0; $i < $link; ++$i) self::$PdoCache[$name][] = self::link_base($base['base']);
        $PdoName = self::pdo_name();
        if (empty(self::$LinkPdo[$PdoName][$name])) {
            if ($link == 1) {
                $data = self::$PdoCache[$name];
                $pdo = $data[key($data)];
            } else {
                $pdo = array_shift(self::$PdoCache[$name]);
            }

            self::$LinkPdo[$PdoName][$name] = ['pdo' => $pdo, 'link' => $link];
        }

        // 判断当前这个 pdo 是否有效连接
        try {
            $pdo = self::$LinkPdo[$PdoName][$name]['pdo'];
            @$pdo->getAttribute(PDO::ATTR_SERVER_INFO);
        } catch (PDOException $e) {
            $pdo = self::link_base($base['base']);
            self::$LinkPdo[$PdoName][$name] = ['pdo' => $pdo, 'link' => $link];
        }

        return self::$LinkPdo[$PdoName][$name];
        */
    }

    /**把数据库连接放回连接池
     * @param $name
     * @param $data
     * @param $link
     * @throws \Exception
     */
    public static function pdo_add($name, $data, $link)
    {
        if ($link > 1) {
            $base = self::my_data($name);
            $name = $base['ext']['data_name'];
            $count = isset(self::$PdoCache[$name]) ? count(self::$PdoCache[$name]) : 0;
            ($count < ($link * 5)) ? self::$PdoCache[$name][] = $data : $data = NULL;
        }
    }

    /**要复制到的数据库
     * @param string $name
     * @param int $link
     * @return $this
     * @throws \Exception
     */
    public function copy($name = '', $link = 0)
    {
        $this->_add_MyData('copy', self::link_data($name, $link));
        return $this;
    }

    /**
     * @param string $table 原表单名
     * @param string $newtable 新表单名
     * @return array|mixed|string
     * @throws \Exception
     */
    public function creste($table, $newtable = '')
    {
        $ext = $this->_sel_MyData('ext');
        $basedata = $ext['name'];
        $table = $ext['table'] . $table;
        $data_name = $ext['data_name'];
        $type = $ext['type'];
        if ($type == 'sqlite') {
            $rvsql = $this->sqlite_if_table($table, $data_name, false);
        } else {
            $rvsql = $this->mysql_if_table($table, $data_name, $basedata, false);
        }
        $copy = !empty($this->_sel_MyData('copy')) ? $this->_sel_MyData('copy') : $ext;
        if (!empty($copy) and !empty($rvsql)) {
            $new_data_name = $copy['data_name'];
            $new_table = $table;
            if (!empty($newtable)) {
                $new_table = $copy['table'] . $newtable;
                $rvsql = str_replace($table, $new_table, $rvsql);
            }
            if ($copy['type'] != 'sqlite') $rvsql = 'use ' . $copy['name'] . ';' . $rvsql;
            self::execs($rvsql, $new_data_name);
            if ($copy['type'] == 'sqlite') {
                $xf_data = $this->sqlite_if_table($new_table, $new_data_name, false, false);
            } else {
                $xf_data = $this->mysql_if_table($new_table, $new_data_name, $copy['name'], false, false);
            }
            return $xf_data;
        }
        return !empty($rvsql) ? $rvsql : false;
    }


    /**sqlite 数据库中表单是否存在,存在返回sql
     * @param string $table 表单名 /全称
     * @param string $name 配置名称
     * @param bool $type 是否调用配置
     * @param bool $exec 是否返回sql
     * @return bool
     * @throws \Exception
     */
    public function sqlite_if_table($table, $name = '', $type = true, $exec = true)
    {
        if (!empty($type)) {
            $ext = !empty($this->_sel_MyData('ext')) ? $this->_sel_MyData('ext') : $this->_sel_MyData('copy');
            if (empty($ext)) $ext = self::link_data($name);
            $new_table = $ext['table'] . $table;
            $data_name = $ext['data_name'];
        } else {
            $new_table = $table;
            $data_name = $name;
        }
        $sql = "SELECT sql  FROM sqlite_master WHERE type='table' and NAME = '{$new_table}';";
        $data = self::query($sql, 'list', $data_name);
        $xfd = !empty($data[key($data)]['sql']) ? $data[key($data)]['sql'] : false;
        return !empty($exec) ? $xfd : (!empty($xfd) ? true : false);
    }

    /**判断 mysql数数据库是否存在,存在返回sql
     * @param string $table 表单名/全称
     * @param string $name 配置名称
     * @param string $link 附加数据库 /数据库名称
     * @param bool $type 是否调用配置
     * @param bool $exec 是否返回sql
     * @return bool
     * @throws \Exception
     */
    public function mysql_if_table($table, $name = '', $link = '', $type = true, $exec = true)
    {
        if (!empty($type)) {
            $ext = !empty($this->_sel_MyData('ext')) ? $this->_sel_MyData('ext') : $this->_sel_MyData('copy');
            if (empty($ext)) $ext = self::link_data($name);
            $new_table = $ext['table'] . $table;
            $data_name = $ext['data_name'];
            $basedata = $ext['name'];
        } else {
            $new_table = $table;
            $data_name = $name;
            $basedata = $link;
        }
        $sql = "SHOW TABLES FROM {$basedata} LIKE '{$new_table}';";
        $data = self::query($sql, 'list', $data_name);
        if (!empty($data) and !empty($exec)) {
            $qusql = "SHOW CREATE  TABLE {$basedata}.{$new_table};";//mysql查询表
            $data = self::query($qusql, 'list', $data_name);
            return $data[key($data)]['Create Table'];
        }
        return !empty($data) ? true : false;
    }

    /**在执行完成时调使用，主要是把sql放回连接池
     * @throws \Exception
     */
    public static function destruct()
    {
        return;
        $PdoName = self::pdo_name();
        $pdo = isset(self::$LinkPdo[$PdoName]) ? self::$LinkPdo[$PdoName] : [];
        if (count($pdo) > 0) {
            foreach ($pdo as $k => $v) self::pdo_add($k, $v['pdo'], $v['link']);
            unset(self::$LinkPdo[$PdoName]);
            ObjCount::del_client('Mysql', $PdoName);
        }
    }

    /**连接数据库
     * @param array $data 连接内容
     * @return PDO
     */
    public static function link_base($data)
    {
        $charset = !empty ($data ['charset']) ? $data ['charset'] : 'utf8';
        switch ($data ['type']) {
            case 'sqlite' :
                $pdo_data = new PDO ('sqlite:' . $data ['name']);
                break;
            default :
                $port = !empty ($data ['port']) ? $data ['port'] : 3306;
                $host = $data ['type'] . ':host=' . $data ['server'] . ';port=' . $port . ';dbname=' . $data ['name'] . ';charset=' . $charset;
                $pdo_data = new PDO ($host, $data ['username'], $data ['password']);
                break;
        }
        $pdo_data->exec('SET NAMES ' . $charset);
        $pdo_data->exec('SET CHARACTER ' . $charset);
        $pdo_data->exec('SET CHARACTER_SET_RESULTS=' . $charset);
        $pdo_data->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $pdo_data->setAttribute(PDO::ATTR_STRINGIFY_FETCHES, false);
        $pdo_data->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
        return $pdo_data;
    }

    /**按配置选择数据库
     * @param string $name 配置名称
     * @param int $first 选择数据库
     * @return array|mixed 返回选择的配置(当前名称/执行模式/连接数量/数据库名称/表单前缀)
     * @throws \Exception
     */
    public static function link_data($name = '', $first = 0)
    {
        $base = self::my_data($name);

        $first = !empty ($first) ? $first : (!empty ($base ['ext'] ['first']) ? $base ['ext'] ['first'] : 0);
        if (!empty ($first)) {
            !empty ($base ['base'] ['data']) ? ($data = $base ['base'] ['data']) : self::dies('没有附加数据库');
            $first = is_numeric($first) ? ($first - 1) : $first;
            if (is_array(current($data))) {
                !empty ($data [$first]) ? ($d = $data [$first]) : self::dies('附加数据库配置不存在');
                $base ['ext'] ['name'] = $d ['name'];
                $base ['ext'] ['table'] = $d ['table'];
            } else {
                $base ['ext'] ['name'] = $data ['base'] ['name'];
                $base ['ext'] ['table'] = $data ['base'] ['table'];
            }
        }
        return $base['ext'];
    }

    /**获取指定数据库配置
     * @param string $name 配置名称
     * @return array|mixed  返回当前配置名称全部配置(附加ext：当前名称/执行模式/连接数量/数据库名称/表单前缀)
     * @throws \Exception
     */
    public static function my_data($name = '')
    {
        $d = self::config();
        $data_name = key($d);
        $names = empty ($name) ? $data_name : $name;
        $base = isset($d [$data_name] [$names]) ? $d [$data_name] [$names] : false;
        if (empty ($base)) {
            $base = $d [$data_name] [$data_name];
            $ext ['first'] = $name;
            $names = $data_name;
        }
        $ext ['type'] = $base ['type'];
        $ext ['data_name'] = $names;
        $ext ['mode'] = $base ['mode'] = !empty ($base ['mode']) ? $base ['mode'] : 1;
        $ext ['link'] = $base ['link'] = !empty ($base ['link']) ? (is_numeric($base ['link']) ? $base ['link'] : 1) : 1;
        $ext ['name'] = $base ['name'];
        $ext ['table'] = $base ['table'];
        return ['base' => $base, 'ext' => $ext];
    }

    /**错误提示
     * @param $data
     * @throws \Exception
     */
    private static function dies($data)
    {
        throw new \Exception($data, 0);
    }

    /**
     * @return mixed|string|void
     * @throws \Exception
     */
    private static function config()
    {
        //return !empty(self::$basearray) ? (is_array(self::$basearray) ? self::$basearray : require self::$basearray) : self::dies('没有Mysql配置信息');

        // 如果没有设置 Mysql 配置，那么默认去读取项目配置目录的 Mysql.php 文件
        if ( empty(self::$basearray) )
        {
            self::$basearray = \zqphp\AutoLoad::GetConfig('ItemDir').'Config/Mysql.php';
        }

        // 初始化配置数据
        $config = [];

        // 判断是否配置Mysql配置文件信息
        if ( !empty(self::$basearray) )
        {
            // 如果是数组，那么就是数据库配置的数组
            if ( is_array(self::$basearray) )
            {
                $config = self::$basearray ;
            }
            else
            {
                // 判断配置文件是否存在，防止用户乱填写
                if ( is_file(self::$basearray) )
                {
                    // 获取配置
                    $config = require self::$basearray;
                }
                else
                {
                    self::dies('Mysql配置文件不存在：'.self::$basearray);
                }
            }
        }

        // 防止配置文件里面的数据格式不是数组
        if ( empty($config) || !is_array($config) )
        {
            self::dies('没有Mysql配置信息：'.self::$basearray);
        }

        // 返回 Mysql 配置信息
        return $config;

    }
}