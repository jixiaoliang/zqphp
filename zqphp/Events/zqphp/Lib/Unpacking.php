<?php

namespace zqphp\Lib;

/**把压缩包内的PHP删除注释和空格符压缩打包返回
 * Class CopyPhpFile
 * @package ext
 */
class Unpacking
{
    public static $dir = __DIR__ . '/../temp/';//临时解压目录
    public static $zip = __DIR__ . '/../temp/zip/';//打包zip目录
    const remark = 'remark.txt';//备注文件名称

    /**打压缩包解压后再处理PHP文件打包
     * @param string $filename 压缩包名字和位置
     * @param bool $type 是否只保留PHP
     * @return string  返回压缩包的包名
     */
    public static function zip($filename, $type = false)
    {
        $name = time() . rand(1000, 9999);
        $dir_name = self::$dir . $name;//临时解压目录
        $filenames = self::$zip . $name . '.zip';//打包zip包名
        $res = self::sel_zips($filename, $dir_name);
        if (empty($res)) {
            $res = self::sel_zip($filename, $dir_name);
        }
        if ($res === true) {
            $res = is_dir($dir_name) ? self::add_zip($dir_name, $filenames, $type) : '10';
        }
        return ($res === true) ? $filenames : false;
    }


    /**把文件夹打包成zip
     * ZipArchive::OVERWRITE    总是创建一个新的文件，如果指定的zip文件存在，则会覆盖掉 
     *ZipArchive::CREATE        如果指定的zip文件不存在，则新建一个 
     *ZipArchive::EXCL        如果指定的zip文件存在，则会报错    
     * @param string $folder 要打包的文件夹
     * @param string $filename 打包的文件名
     * @param bool $type 是否只保留PHP
     * @return bool mixed 返回true成功
     */
    public static function add_zip($folder, $filename, $type = false)
    {
        (!is_dir(dirname($filename))) ? mkdir(dirname($filename), 0777, true) : false;
        $zip_obj = new \ZipArchive();
        $class = file_exists($filename) ? \ZipArchive::OVERWRITE : \ZipArchive::CREATE;
        $res = $zip_obj->open($filename, $class);
        if ($res === true) {
            self::pack_php($zip_obj, $folder, $type);
            $zip_obj->close();
            rmdir($folder);
        }
        return $res;
    }

    /**zip解压
     *copy('zip://' . $filename . '#' . $name, $folder . '/' . $name);
     *file_put_contents($folder . '/' . $name, file_get_contents('zip://' . $filename . '#' . $name));
     * @param string $filename zip路径包名
     * @param string $folder 解压到文件夹
     * @return bool mixed 返回true成功
     */
    public static function sel_zip($filename, $folder)
    {
        $zip_obj = new \ZipArchive();
        $res = $zip_obj->open($filename);
        if ($res === true) {
            !file_exists($folder) ? mkdir($folder, 0777, true) : false;
            $numFiles = $zip_obj->numFiles;
            for ($i = 0; $i < $numFiles; $i++) {
                $statInfo = $zip_obj->statIndex($i, \ZipArchive::FL_ENC_RAW);
                $name = self::encoding($statInfo['name']);
                if ($statInfo['crc'] == 0) {
                    $dir_name = $folder . '/' . substr($name, 0, -1);
                    (!is_dir($dir_name)) ? mkdir($dir_name) : false;
                } else {
                    if (preg_match("/[\x7f-\xff]/", $name)) {
                        $remark_name = $folder . '/' . self::remark;
                        $remark = "包含有中文无法处理：" . $name . "\r\n";
                        $my_file = fopen($remark_name, 'a');
                        fwrite($my_file, $remark);
                        fclose($my_file);
                    }
                    file_put_contents($folder . '/' . $name, $zip_obj->getFromName($name));
                }
            }
            $zip_obj->close();
        }
        return $res;
    }

    /**zip解压 中文文件名过为空
     * @param string $filename zip路径包名
     * @param string $folder 解压到文件夹
     * @return bool mixed 返回true成功
     */
    public static function sel_zips($filename, $folder)
    {
        $res = false;
        if ($zip_obj = zip_open($filename)) {
            $folders = rtrim($folder, '/') . '/';
            !file_exists($folder) ? mkdir($folder, 0777, true) : false;
            while ($zip = zip_read($zip_obj)) {
                $zips = strrpos(zip_entry_name($zip), "/");
                if ($zips !== false) {
                    $zip_dir = $folders . substr(zip_entry_name($zip), 0, $zips + 1);
                    !file_exists($zip_dir) ? mkdir(trim($zip_dir), 0777, true) : false;
                }
                if (zip_entry_open($zip_obj, $zip, "r")) {
                    $file_name = $folders . zip_entry_name($zip);
                    if (!is_dir($file_name)) {
                        $content = zip_entry_read($zip, zip_entry_filesize($zip));
                        if (preg_match("/[\x7f-\xff]/", basename($file_name))) {
                            $remark = "文件改名：" . $file_name;
                            $file_name = dirname($file_name) . '/' . rand(1000, 9999) . str_replace(' ', '', preg_replace('/([\x80-\xff]*)/i', '', basename($file_name)));
                            $remark_name = $folders . self::remark;
                            $remark .= " -> " . $file_name . "\r\n";
                            $my_file = fopen($remark_name, 'a');
                            fwrite($my_file, $remark);
                            fclose($my_file);
                        }
                        file_put_contents($file_name, $content);
                        chmod($file_name, 0777);
                    }
                    zip_entry_close($zip);
                }
            }
            zip_close($zip_obj);
            $res = true;
        }
        return $res;
    }


    /**打包过程中把php文件处理
     * @param object $zip_obj 压缩对像
     * @param string $folder 文件夹名称
     * @param bool $type 是否只保留PHP
     */
    private static function pack_php($zip_obj, $folder, $type = false)
    {
        if (is_dir($folder) && ($handle = opendir($folder)) !== false) {
            while (($file = readdir($handle)) !== false) {
                if ($file != '.' && $file != '..') {
                    $path_file = $folder . '/' . $file;
                    if (is_dir($path_file)) {
                        self::pack_php($zip_obj, $path_file, $type);
                        rmdir($path_file);
                    } else {
                        if (strtolower(pathinfo($path_file, PATHINFO_EXTENSION)) == 'php') {
                            $content = php_strip_whitespace($path_file);
                            $zip_obj->addFromString(iconv('utf-8', 'gbk', str_replace(self::$dir, '', $path_file)), $content);
                        } else if (empty($type) or basename($path_file) == self::remark) {
                            $content = file_get_contents(iconv('utf-8', 'gbk', $path_file));
                            $zip_obj->addFromString(iconv('utf-8', 'gbk', str_replace(self::$dir, '', $path_file)), $content);
                        }
                        unlink($path_file);
                    }
                }
            }
            closedir($handle);
        }
    }

    /**编码转换
     * @param $name
     * @return false|string
     */
    private static function encoding($name)
    {
        $encodes = ['UTF-8', 'GBK', 'BIG5', 'CP936', 'GB2312'];
        $data = mb_detect_encoding($name, $encodes);
        $encoding = ($data != 'UTF-8') ? iconv($data, 'utf-8//IGNORE', $name) : $name;
        return $encoding;
    }

}

