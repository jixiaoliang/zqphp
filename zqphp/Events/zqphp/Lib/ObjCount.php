<?php

namespace zqphp\Lib;
/**对像标识仓库
 * Class ObjCount
 * @package zqphp\Lib
 */
class ObjCount
{
    public static $prefix = 'ZQ'; //客户端ID前缀
    public static $timeout = 120;//清理2分钟没有动态的对像
    public static $arr = []; //客户端对像仓库信息
    private static $hits = []; //客户端总数

    /**
     *
     */
    public static function demo()
    {
        ps(self::gen_name('ccc', 1));
        self::gen_name('ccc', 2);
        self::gen_name('ccc', 3);
        self::gen_name('ccc', 4);
        self::gen_name('ccc', 5);
        self::gen_name('eee', 6);
        ps(self::gen_name('sss', 7));
        self::gen_name('eee', 8);
        self::gen_name('sss', 9);
        self::del_client('ccc', 'all');
        self::gen_name('ccc', 6);
        echo '<pre>' . print_r(self::name_id(), true) . '</pre>';
        echo '<pre>' . print_r(self::name_id(), true) . '</pre>';
        echo '<pre>' . print_r(self::client_list(), true) . '</pre>';
    }

    private static function session_id()
    {
        $http = isset($_SERVER['HTTP_USER_AGENT']) ? true : false;
        if (!empty($http)) {
            if (empty(session_id())) session_start();
            return session_id();
        }
        $REMOTE_ADDR = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '';
        $REMOTE_PORT = isset($_SERVER['REMOTE_PORT']) ? $_SERVER['REMOTE_PORT'] : '';
        return $REMOTE_ADDR . ':' . $REMOTE_PORT;
    }

    /**生成客户端对像标识名称
     * @param string $style 分组
     * @param string $name 名称
     * @return string
     */
    public static function name_id($style = 'all', $name = '')
    {
        $data = substr(hash('crc32b', $style . '>' . self::session_id() . $name), 0, 8);
        return $data;
    }

    /**生成客户端对像标识id并记录
     * @param string $style 分组
     * @param string $name 名称
     * @return string
     */
    public static function gen_name($style = 'all', $name = '')
    {
        $data = self::name_id($style, $name);
        $name_id = self::_client_count($style, $data);
        return $name_id;
    }

    /** 客户端计数生成名称id
     * @param string $style 分组
     * @param string $name 名称
     * @return int|string
     */
    private static function _client_count($style, $name)
    {
        $client_hits = (!empty(self::$hits[$style]) ? self::$hits[$style] : 0);
        if (empty(self::$arr[$style][$name])) {
            self::$hits[$style] = $_hits = $client_hits + 1;
            $id = self::$prefix . $name . bin2hex(pack('N', $_hits));
            self::$arr[$style][$name] = ['id' => $id, 'time' => time()];
        } else {
            $id = self::$arr[$style][$name]['id'];
            self::$arr[$style][$name]['time'] = time();
        }
        return $id;
    }

    /** 客户端对像总数/数量/列表
     * @return array
     */
    public static function client_list()
    {
        $count = [];
        foreach (self::$arr as $k => $v) {
            $count[$k] = ['total' => self::$hits[$k], 'count' => count($v)];
        }
        return ['count' => $count, 'list' => self::$arr];
    }

    /**注销客户端对像
     * @param $style
     * @param string $name
     * @param int $time
     */
    public static function del_client($style, $name = '', $time = 0)
    {
        $client_list = isset(self::$arr[$style]) ? self::$arr[$style] : [];
        $ov = count($client_list);
        if (empty($ov)) return;
        if ($name == 'all') {
            unset (self::$arr[$style]);
        } elseif (!empty($name)) {
            unset (self::$arr[$style][substr($name, strlen(self::$prefix), 8)]);
        } else {
            $times = time();
            $timeouts = !empty($time) ? $time : self::$timeout;
            foreach ($client_list as $k => $v) {
                if (($times - $v['time']) > $timeouts) {
                    unset (self::$arr[$style][$k]);
                }
            }
        }
    }
}