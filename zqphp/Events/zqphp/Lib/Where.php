<?php

namespace zqphp\Lib;

class Where
{
    const  and_list = 'and,and1,and2,and3,and4,and5,and6,and7,and8,and9,&&';
    const or_list = 'or,or1,or2,or3,or4,or5,or6,or7,or8,or9,||';

    /**
     * @param $data
     * @param string $andor
     * @return array
     */
    public static function where($data, $andor = 'and')
    {
        if (!empty($data)) {
            if (is_array($data)) {
                $andor = !empty($andor) ? $andor : 'and';
                if (count($data) > 1) {
                    $data = [$andor => $data];
                } else if (is_numeric(key($data))) {
                    $data = [$andor => $data[key($data)]];
                }
                return self::wheres($data, $andor);
            } else {
                return [1 => $data, $data, []];
            }
        } else {
            return [1 => '', '', []];
        }
    }

    /**
     * @param array $arr
     * @param string $andor
     * @param string $z
     * @param array $w_str
     * @param int $i
     * @return array
     */
    public static function wheres(array $arr, $andor, $z = '', $w_str = [], $i = 0)
    {
        $w_str [2] = $w_str [1] = '';
        foreach ($arr as $k => $v) {
            ++$i;
            $d_str = ($i > 1 and $z) ? ' ' . $z . ' ' : '';
            if (!empty($ks = self::_and_or($k, $andor))) {
                $wd_str = self::wheres($v, $andor, $ks, $w_str);
                $w_str [1] .= $d_str . '(' . $wd_str [1] . ')';
                $w_str [2] .= $d_str . '(' . $wd_str [2] . ')';
                $w_str [3] = isset($wd_str [3]) ? $wd_str [3] : [];
            } else {
                $w_str [1] .= $d_str;
                $w_str [2] .= $d_str;
                $wd_str = self::_where_str($k, $v);
                $w_str [1] .= $wd_str [1];
                $w_str [2] .= $wd_str [2];
                foreach ($wd_str ['v'] as $u => $h) {
                    $w_str [3] [] = $h;
                }
            }
        }
        return $w_str;
    }

    /**条件处理,要加条件请在这里加上
     * @param $k
     * @param $v
     * @return mixed
     */
    private static function _where_str($k, $v)
    {
        $where ['v'] = [];
        $c = self::_get_str(trim($k), '[', ']');
        $x = trim(str_replace('[' . $c . ']', '', trim($k)));
        $istab = false;
        if ($c == '.') {
            $c = '';
            $istab = true;
        } else if (substr($c, 0, 1) == '.') {
            $c = substr($c, 1, strlen($c));
            $istab = true;
        }
        if (!empty($c) and strpos(',1,2,3,4,5,6,7,8,9,', ',' . substr($c, 0, 1) . ',') !== false) $c = substr($c, 1, strlen($c));
        $c = !empty($c) ? $c : '=';
        $sql = $x . ' ' . $c . ' ';
        switch ($c) {
            case 'not es' :
            case 'es' :
                if ($c != 'es') $x .= ' not';
                $sql_1 = $x . ' exists ' . '(?)';
                $sql_2 = $x . ' exists ' . '(' . trim($v) . ')';
                $where ['v'] = $v;
                break;
            case 'not in' :
            case 'in' :
                $vs = is_array($v) ? $v : explode(',', $v);
                $_v_1 = $_v_2 = '';
                foreach ($vs as $q => $w) {
                    $_v_1 .= ',?';
                    $_v_2 .= ',' . $w;
                }
                $where ['v'] = $vs;
                $sql_1 = $sql . '(' . trim($_v_1, ',') . ')';
                $sql_2 = $sql . '(' . trim($_v_2, ',') . ')';
                break;
            case 'not like' :
            case 'like' :
                $sql_1 = $sql . '?';
                $sql_2 = $sql . '\'%' . $v . '%\'';
                $where ['v'] [] = '%' . $v . '%';
                break;
            case 'not bet' :
            case 'bet' :
                if ($c != 'bet') $x .= ' not';
                $p = !is_array($v) ? ($p = explode(',', $v)) : ($p = $v);
                $where ['v'] [] = $p [0];
                $where ['v'] [] = $p [1];
                $sql_1 = $x . ' between ? and ?';
                $sql_2 = $x . ' between ' . self::_string($p [0]) . ' and ' . self::_string($p [1]);
                break;
            default :
                if (empty($istab)) {
                    $sql_1 = $sql . '?';
                    $sql_2 = $sql . self::_string($v);
                    $where ['v'] [] = $v;
                } else {
                    $sql_1 = $sql . $v;
                    $sql_2 = $sql . $v;
                }
                break;
        }
        $where [1] = $sql_1;
        $where [2] = $sql_2;
        return $where;
    }

    /**where and or key
     * @param $str
     * @param $andor
     * @return bool|int|string
     */
    private static function _and_or($str, $andor)
    {
        if (is_numeric($str)) {
            return $andor;
        } else if (strpos(',' . self:: and_list . ',', ',' . $str . ',') !== false) {
            return 'and';
        } else if (strpos(',' . self:: or_list . ',', ',' . $str . ',') !== false) {
            return 'or';
        }
        return false;
    }

    /** 获取指定字符串之间的内容
     * @param $input
     * @param $start
     * @param $end
     * @return bool|string
     */
    public static function _get_str($input, $start, $end)
    {
        $substr = substr($input, strlen($start) + strpos($input, $start), (strlen($input) - strpos($input, $end)) * (-1));
        return $substr;
    }

    /**判断是字符串加引号
     * @param $data
     * @return string
     */
    public static function _string($data)
    {
        return !empty (is_string($data)) ? "'{$data}'" : $data;
    }
}