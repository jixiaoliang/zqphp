<?php

namespace zqphp\Lib;

use \zqphp\AutoLoad;

class TempLet {

    protected $TplInfo = [];

    /**
     * 模板参数
     * @param $Name
     * @param string $Data
     * @return bool|mixed
     */
    protected function vars($Name, $Data = null) {
        if (is_array($Name)) {
            if (!empty($Name)) {
                foreach ($Name as $k => $v) {
                    $this->TplInfo[$k] = $v;
                }
            }
        } else if (!is_null($Data)) {
            $this->TplInfo[$Name] = $Data;
        } else if (!empty($Name)) {
            return $this->TplInfo[$Name];
        }
        return true;
    }

    /**
     * 普通引入(不编译模板)
     * @param null|string|bool|array $File //支持array多个文件引入
     * @param bool $Type //是否生成静态
     */
    public function show($File = null, $Type = false) {
        if (empty(is_array($File))) {
            self::ReqFIle(AutoLoad::ViewFile($File, $Type));
        } elseif (!empty($File)) {
            foreach ($File as $k => $v) {
                $this->show($v, $Type);
            }
        }
    }

    /**
     * 使用模板引入(编译模板保存编译文件引入)
     * @param null|string|bool|array $File //支持array多个文件引入
     * @param bool $Type //是否生成静态
     */
    protected function html($File = null, $Type = false) {
        if (empty(is_array($File))) {
            self::GeneratePhp(AutoLoad::ViewFile($File, $Type), $Type);
        } elseif (!empty($File)) {
            foreach ($File as $k => $v) {
                $this->html($v, $Type);
            }
        }
    }

    /**
     * 模板编成PHP保存引入
     * @param $ViewFile //视图文件
     * @param bool $Type //是否生成静态
     */
    private function GeneratePhp($ViewFile, $Type) {
        //View目录
        $ViewDir = self::SepFile(self::ConfigDir('ViewDir'));
        //编译模板目录
        $TemplateDir = self::ConfigDir('TemplateDir', self::ConfigDir('TempDir'));
        //编译模板时间
        $TemplateTime = AutoLoad::GetConfig('TemplateTime');
        //编译文件路径
        $TemplateFile = str_replace($ViewDir, $TemplateDir, $ViewFile);
        //编译文件存在,检查模板时间
        if (is_file($TemplateFile)) {
            if ($TemplateTime === true || filemtime($TemplateFile) >= time()) {
                //true或者时间没到的不删除、引入文件
                self::ReqFIle($TemplateFile);
                return;
            }
        }
        //=========编译文件不存在时处理=========
        //创建编译文件目录
        AutoLoad::MkDir(AutoLoad::DelDir($TemplateFile));
        //处理模板
        $Content = self::ProcessTemplate($ViewFile, $ViewDir, $Type);
        //保存模板内容
        file_put_contents($TemplateFile, $Content);
        touch($TemplateFile, time() + (is_numeric($TemplateTime) ? $TemplateTime : 0));
        //引入模板
        self::ReqFIle($TemplateFile);
    }

    /**
     * 处理模板
     * @param $ViewFile
     * @param $ViewDir
     * @param bool $Type //是否生成静态
     * @return bool
     */
    private static function ProcessTemplate($ViewFile, $ViewDir, $Type) {
        $TplLeft = '{';//解析时左边符号
        $TplRight = '}';//解析时右边符号
        //打开视图文件
        $FileData = file_get_contents($ViewFile);
        //处理引入文件
        preg_match_all('#' . $TplLeft . 'file=(\"|\'|)(.*?)(\"|\'|)' . $TplRight . '#i', $FileData, $FileInfo);
        if (!empty($HandleFile = AutoLoad::IsArrData($FileInfo, 2))) {
            foreach ($HandleFile as $k => $v) {
                $v = trim($v);
                $PullFileName = !empty(AutoLoad::PathInfo($v)) ? $v : $v . '.' . AutoLoad::PathInfo($ViewFile);//文件名加上格式
                if (substr($v, 0, 1) == '/') {
                    $PullFile = $ViewDir . trim($PullFileName, '/');
                } else {
                    $PullFile = AutoLoad::DelDir($ViewFile) . DIRECTORY_SEPARATOR . $PullFileName;
                }
                if (empty(is_file($PullFile))) {
                    $PullData = "\r\n<!--" . $v . "文件不存在-->\r\n";
                } else {
                    $PullData = "\r\n<!--" . $v . " top-->\r\n" . self::ProcessTemplate($PullFile, $ViewDir, $Type) . "\r\n<!--" . $v . " foot-->\r\n";//获取引入文件内容
                }
                $FileData = str_replace($TplLeft . $FileInfo[0][$k] . $TplRight, $PullData, $FileData);//替换为内容
            }
        }
        //一些标签替换
        $FileData = str_replace($TplLeft . '/if' . $TplRight, '<?php }?>', $FileData);
        $FileData = str_replace($TplLeft . '/else' . $TplRight, '<?php }else{?>', $FileData);
        $FileData = str_replace($TplLeft . '/loop' . $TplRight, '<?php }?>', $FileData);
        $FileData = str_replace($TplLeft . '/loopelse' . $TplRight, '<?php }else{?>', $FileData);
        $FileData = str_replace($TplLeft . '/foreach' . $TplRight, '<?php }?>', $FileData);
        $FileData = str_replace($TplLeft . '/foreachelse' . $TplRight, '<?php }else{?>', $FileData);
        $FileData = str_replace($TplLeft . '/for' . $TplRight, '<?php }?>', $FileData);
        //替换符号
        preg_match_all('/' . $TplLeft . '(.*?)' . $TplRight . '/', $FileData, $SignData);
        if (!empty($HandleSign = AutoLoad::IsArrData($SignData, 1))) {
            foreach ($HandleSign as $k => $v) {
                $FileData = str_replace($SignData[0][$k], $TplLeft . str_replace('$', '@#@', $v) . $TplRight, $FileData);
            }
        }
        //表达式处理
        $Pattern = array(
            '/\@\#\@(\w*[a-zA-Z0-9_])/',
            '/\@\#\@\:(.*?)/',
            '/\@\#\@\.(.*?)/',
            '/\$this\-\>TplInfo\[\'(\w*[a-zA-Z0-9_])\'\]+(\.|\:|\-\>)(\w*[a-zA-Z0-9])+(\.|\:|\-\>)(\w*[a-zA-Z0-9])+(\.|\:|\-\>)(\w*[a-zA-Z0-9])+(\.|\:|\-\>)(\w*[a-zA-Z0-9])/',
            '/\$this\-\>TplInfo\[\'(\w*[a-zA-Z0-9_])\'\]+(\.|\:|\-\>)(\w*[a-zA-Z0-9])+(\.|\:|\-\>)(\w*[a-zA-Z0-9])+(\.|\:|\-\>)(\w*[a-zA-Z0-9])/',
            '/\$this\-\>TplInfo\[\'(\w*[a-zA-Z0-9_])\'\]+(\.|\:|\-\>)(\w*[a-zA-Z0-9])+(\.|\:|\-\>)(\w*[a-zA-Z0-9])/',
            '/\$this\-\>TplInfo\[\'(\w*[a-zA-Z0-9_])\'\]+(\.|\:|\-\>)(\w*[a-zA-Z0-9])/',
            '/' . $TplLeft . '\$(.*?)(\=\"|\=\')(.*?)(\"|\')' . $TplRight . '/',
            '/' . $TplLeft . '\$(.*?)(\=\"|\=\')(.*?)(.*?)' . $TplRight . '/',
            '/' . $TplLeft . '\$this\-\>TplInfo(\[(.*?)])(.*?)' . $TplRight . '/',
            '/' . $TplLeft . 'if (.*?)' . $TplRight . '/',
            '/' . $TplLeft . 'elseif (.*?)' . $TplRight . '/',
            '/' . $TplLeft . 'loop \$(.*) as (\w*[a-zA-Z0-9_])' . $TplRight . '/',
            '/' . $TplLeft . 'foreach \$(.*) (\w*[a-zA-Z0-9_])\=\>(\w*[a-zA-Z0-9_])' . $TplRight . '/',
            '/' . $TplLeft . 'for (.*?)' . $TplRight . '/',
            '/' . $TplLeft . '\/\*(.*?)\*\/' . $TplRight . '/is',
            '/' . $TplLeft . '\/\/(.*?)' . $TplRight . '/',
            '/' . $TplLeft . '(.*?)' . $TplRight . '/'
        );
        $Replacement = array(
            '$this->TplInfo[\'\1\']',
            '$this->\1',
            '$\1',
            '$this->TplInfo[\'\1\'][\'\3\'][\'\5\'][\'\7\'][\'\9\']',
            '$this->TplInfo[\'\1\'][\'\3\'][\'\5\'][\'\7\']',
            '$this->TplInfo[\'\1\'][\'\3\'][\'\5\']',
            '$this->TplInfo[\'\1\'][\'\3\']',
            '<?php \$\1\2\3\4;?>',
            '<?php \$\1=\2\3;?>',
            '<?php echo \$this->TplInfo\1\3;?>',
            '<?php if(\1){?>',
            '<?php }elseif(\1){?>',
            '<?php foreach(\$\1 as \$this->TplInfo[\'\2\']) {?>',
            '<?php foreach(\$\1 as \$this->TplInfo[\'\2\']=>$this->TplInfo[\'\3\']) {?>',
            '<?php for (\1){?>',
            '<?php /*\1*/?>',
            '<?php //\1?>',
            '<?php echo \1;?>'
        );
        return preg_replace($Pattern, $Replacement, $FileData);
    }

    /**
     * 引入文件
     * @param $File
     */
    private function ReqFIle($File) {
        require $File;
    }

    /**
     * 可获得(TempDir,PhpDir,ViewDir,PublicDir)
     * @param $Name
     * @param null $Dir
     * @return string|null
     */
    private static function ConfigDir($Name, $Dir = null) {
        $Dir = $Dir ? $Dir : AutoLoad::GetConfig('ItemDir');
        if (!empty($NameDir = AutoLoad::GetConfig($Name))) {
            $Dir = $Dir . trim(trim($NameDir, '/'), '\\') . '/';
        }
        return $Dir;
    }

    /**
     * 替换路径符号
     * @param $File
     * @return mixed
     */
    private static function SepFile($File) {
        return AutoLoad::StrReplace(AutoLoad::StrReplace($File, '\\', '/'), '/', DIRECTORY_SEPARATOR);
    }
}