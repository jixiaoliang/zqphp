<?php

namespace zqphp\Lib;
class Redis
{
    public $data = [];
    public static $basearray = __DIR__ . '/../config/Redis.php';//配置信息

    /**选择数据库
     * @param $name
     * @return $this
     */
    public function select($name)
    {
        $this->data['redis']->select($name);
        return $this;
    }

    /**设置key和value的值
     * @param $key
     * @param $data
     * @return mixed BOOL 成功返回：TRUE;失败返回：FALSE
     */
    public function set($key, $data)
    {
        return $this->data['redis']->set($key, $data);
    }

    /**获取有关指定键的值
     * @param $key
     * @return mixed string或BOOL 如果键不存在，则返回 FALSE。否则，返回指定键对应的value值
     */
    public function get($key)
    {
        return $this->data['redis']->get($key);
    }

    /**删除指定的键
     * @param $key
     * @return mixed 删除的项数
     */
    public function delete($key)
    {
        return $this->data['redis']->delete($key);
    }

    /**如果在数据库中不存在该键，设置关键值参数
     * @param $key
     * @param $data
     * @return mixed BOOL 成功返回：TRUE;失败返回：FALSE
     */
    public function setnx($key, $data)
    {
        return $this->data['redis']->setnx($key, $data);
    }

    /**验证指定的键是否存在
     * @param $key
     * @return mixed Bool 成功返回：TRUE;失败返回：FALSE
     */
    public function exists($key)
    {
        return $this->data['redis']->exists($key);
    }

    /**数字递增存储键值键
     * @param $key
     * @return mixed INT the new value
     */
    public function incr($key)
    {
        return $this->data['redis']->incr($key);
    }

    /**数字递减存储键值
     * @param $key
     * @return mixed INT the new value
     */
    public function decr($key)
    {
        return $this->data['redis']->decr($key);
    }

    /**取得所有指定键的值。如果一个或多个键不存在，该数组中该键的值为假
     * @param array $key
     * @return mixed 返回包含所有键的值的数组
     */
    public function getMultiple($key)
    {
        return $this->data['redis']->getMultiple($key);
    }

    /**由列表头部添加字符串值。如果不存在该键则创建该列表。如果该键存在，而且不是一个列表，返回FALSE。
     * @param $key
     * @param $data
     * @return mixed 成功返回数组长度，失败false
     */
    public function lpush($key, $data)
    {
        return $this->data['redis']->lpush($key, $data);
    }

    /**由列表尾部添加字符串值。如果不存在该键则创建该列表。如果该键存在，而且不是一个列表，返回FALSE。
     * @param $key
     * @param $data
     * @return mixed 成功返回数组长度，失败false
     */
    public function rpush($key, $data)
    {
        return $this->data['redis']->rpush($key, $data);
    }

    /**返回和移除列表的第一个元素
     * @param $key
     * @return mixed 成功返回第一个元素的值 ，失败返回false
     */
    public function lpop($key)
    {
        return $this->data['redis']->lpop($key);
    }

    /**返回的列表的长度。如果列表不存在或为空，该命令返回0。如果该键不是列表，该命令返回FALSE。
     * @param $key
     * @return mixed 成功返回数组长度，失败false
     */
    public function lsize($key)
    {
        return $this->data['redis']->lsize($key);
    }

    /**返回的列表的长度。如果列表不存在或为空，该命令返回0。如果该键不是列表，该命令返回FALSE。
     * @param $key
     * @return mixed 成功返回数组长度，失败false
     */
    public function llen($key)
    {
        return $this->data['redis']->llen($key);
    }

    /**返回指定键存储在列表中指定的元素。 0第一个元素，1第二个… -1最后一个元素，-2的倒数第二…错误的索引或键不指向列表则返回FALSE。
     * @param $key
     * @param $iv
     * @return mixed 成功返回指定元素的值，失败false
     */
    public function lget($key, $iv)
    {
        return $this->data['redis']->lget($key, $iv);
    }

    /**为列表指定的索引赋新的值,若不存在该索引返回false.
     * @param $key
     * @param $iv
     * @param $data
     * @return mixed 成功返回true,失败false
     */
    public function lset($key, $iv, $data = '')
    {
        return $this->data['redis']->lset($key, $iv, $data);
    }

    /**返回在该区域中的指定键列表中开始到结束存储的指定元素，lGetRange(key, start, end)。0第一个元素，1第二个元素… -1最后一个元素，-2的倒数第二…
     * @param $key
     * @param $top
     * @param string $foot
     * @return mixed 成功返回查找的值，失败false
     */
    public function lgetrange($key, $top, $foot = '')
    {
        return $this->data['redis']->lgetrange($key, $top, $foot);
    }

    /**从列表中从头部开始移除count个匹配的值。如果count为零，所有匹配的元素都被删除。如果count是负数，内容从尾部开始删除。
     * @param $key
     * @param $top
     * @param string $foot
     * @return mixed 成功返回删除的个数，失败false
     */
    public function lremove($key, $top, $foot = '')
    {
        return $this->data['redis']->lremove($key, $top, $foot);
    }

    /**为一个集合Key添加一个值。如果这个值已经在这个Key中，则返回FALSE。
     * @param $key
     * @param $data
     * @return mixed 成功返回true,失败false
     */
    public function sadd($key, $data)
    {
        return $this->data['redis']->sadd($key, $data);
    }


    /**检查集合中是否存在指定的值。
     * @param $key
     * @param $data
     * @return mixed true or false
     */
    public function scontains($key, $data)
    {
        return $this->data['redis']->scontains($key, $data);
    }

    /**获取集合值
     * @param $key
     * @return mixed
     */
    public function sort($key)
    {
        return $this->data['redis']->sort($key);
    }

    /**将集合中Key1中的value移动到Key2中
     * @param $key1
     * @param $key2
     * @param $data
     * @return mixed true or false
     */
    public function smove($key1, $key2, $data)
    {
        return $this->data['redis']->smove($key1, $key2, $data);
    }

    /**返回集合中存储值的数量
     * @param $key
     * @return mixed 成功返回数组个数，失败0
     */
    public function ssize($key)
    {
        return $this->data['redis']->ssize($key);
    }

    /**随机移除并返回key中的一个值
     * @param $key
     * @return mixed 成功返回删除的值，失败false
     */
    public function spop($key)
    {
        return $this->data['redis']->spop($key);
    }

    /**返回一个所有指定键的交集。如果只指定一个键，那么这个命令生成这个集合的成员。如果不存在某个键，则返回FALSE。
     * @param $key1
     * @param $key2
     * @return mixed 成功返回数组交集，失败false
     */
    public function sinter($key1, $key2)
    {
        return $this->data['redis']->sinter($key1, $key2);
    }

    /**
     * @param string $name
     * @return bool
     * @throws \Exception
     */
    public static function my_data($name = '')
    {
        $d = self::config();
        $cname = key($d);
        $names = empty ($name) ? $cname : $name;
        $data = isset($d [$cname] [$names]) ? $d [$cname] [$names] : false;
        return $data;
    }

    /**
     * @param string $name
     * @return Redis
     * @throws \Exception
     */
    public static function link($name = '')
    {
        $data = self::my_data($name);
        $redis = new \Redis();
        if (!empty($data['server']) and !empty($data['port'])) {
            if (empty($redis->connect($data['server'], $data['port']))) self::dies('连接错误');
            if (!empty($data['password']) and empty($redis->auth($data['password']))) self::dies('密码错误');
        }
        $self = new self();
        $self->data['redis'] = $redis;
        return $self;

    }

    /**
     * @return mixed|string|void
     * @throws \Exception
     */
    public static function config()
    {
        return !empty(self::$basearray) ? (is_array(self::$basearray) ? self::$basearray : require self::$basearray) : self::dies('没有Redis配置信息');
    }

    /**
     * @param $data
     * @throws \Exception
     */
    public static function dies($data)
    {
        throw new \Exception($data);
    }

}