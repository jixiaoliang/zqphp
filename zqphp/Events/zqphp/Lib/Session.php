<?php

namespace zqphp\Lib;

class Session
{
    public static $style = 'Session';
    public static $name = 'A';
    public static $prefix = 'S';
    public static $data = [];

    /**生成名称
     * @return string
     */
    private static function getname()
    {
        ObjCount::$prefix = self::$prefix;
        return ObjCount::gen_name(self::$style, self::$name);
    }

    /**
     * @param $name
     * @param string $data
     * @return bool
     */
    public static function set($name, $data = '')
    {
        $nameid = self::getname();
        if (is_array($name)) {
            if (count($name) > 0) foreach ($name as $k => $v) {
                $_SESSION[$k] = $v;
                self::$data[$nameid][$k] = $v;
                return true;
            }
        } else if (!empty($data)) {
            $_SESSION[$name] = $data;
            self::$data[$nameid][$name] = $data;
            return true;
        } else if (!empty($name)) {
            return isset($_SESSION[$name]) ? $_SESSION[$name] : (isset(self::$data[$nameid][$name]) ? self::$data[$nameid][$name] : false);
        }
        return false;
    }

    /**获取
     * @param $name
     * @return bool
     */
    public static function get($name)
    {
        $nameid = self::getname();
        return isset($_SESSION[$name]) ? $_SESSION[$name] : (isset(self::$data[$nameid][$name]) ? self::$data[$nameid][$name] : false);
    }

    /**清空指定
     * @param $name
     */
    public static function delete($name)
    {
        if (empty($name)) return;
        $nameid = self::getname();
        if (isset($_SESSION[$name])) unset($_SESSION[$name]);
        if (isset(self::$data[$nameid][$name])) unset(self::$data[$nameid][$name]);
    }

    /**清空全部
     *关闭时清空
     */
    public static function delall()
    {
        $_SESSION = [];
        $nameid = self::getname();
        if (isset(self::$data[$nameid])) unset(self::$data[$nameid]);
        ObjCount::del_client(self::$style, 'all');
    }

}