<?php

namespace zqphp\Mid;

use zqphp\Lib\Mysql;

/**
 * 使用use复制
 * use \zqphp\Mid\My;
 *public static $name='';//数据库配置名称
 *public static $link='';//附加数据库key//主数据库请为0
 *public static $base=false;//配置信息false使用默认 字符串为调文件，数组为配置
 * Trait My
 * @package zqphp\Mid
 */
trait My
{

    /**指定配置信息
     * @return Mysql
     * @throws \Exception
     * @zqphp:hide
     */
    public static function link()
    {
        return Mysql::link(self::getconfig());
    }

    /**通过demo生成的参数执行
     * @param array $data demo返回参数
     * @param array $type 返回类型/新的邦定参数
     * @param array $new_data 新的邦定参数
     * @return array|mixed
     * @throws \Exception
     */
    public static function exec($data, $type = [], $new_data = [])
    {
        return Mysql::exec($data, $type, $new_data);
    }

    /**返回表单名称，配合tab多联跨库联查
     * @param $data
     * @param $field
     * @return string
     * @throws \Exception
     */
    public static function tabname($data, $field = '')
    {
        return self::link()->tabname($data, $field);
    }

    /**
     * @param $data
     * @param string $type
     * @param array $where
     * @param string $orand
     * @return Mysql
     * @throws \Exception
     */
    public static function join($data, $type = 'table', $where = [], $orand = 'and')
    {
        return self::link()->join($data, $type, $where, $orand);
    }

    /**使用union 前使用代替table 多表查询时使用
     * @param $data
     * @param bool $union
     * @return Mysql
     * @throws \Exception
     */
    public static function alias($data, $union = false)
    {
        return self::link()->alias($data, $union);
    }

    /**
     * @param $data
     * @param bool $type
     * @return Mysql
     * @throws \Exception
     */
    public static function union($data, $type = false)
    {
        return self::link()->union($data, $type);
    }

    /**表单加前缀
     * @param $data
     * @return \zqphp\Lib\Mysql
     * @throws \Exception
     * @zqphp:hide
     */
    public static function table($data)
    {
        return self::link()->table($data);
    }

    /**表单不加前缀
     * @param $data
     * @return Mysql
     * @throws \Exception
     * @zqphp:hide
     */
    public static function tab($data)
    {
        return self::link()->tab($data);
    }

    /**开启事务
     * @throws \Exception
     */
    public static function beg()
    {
        self::link()->affair_pdo()->beginTransaction();
    }

    /**提交事务
     * @throws \Exception
     * @zqphp:hide
     */
    public static function com()
    {
        self::link()->affair_pdo()->commit();
    }

    /**回滚事务
     * @throws \Exception
     * @zqphp:hide
     */
    public static function rol()
    {
        self::link()->affair_pdo()->rollBack();
    }

    /**事务闭包处理
     * @param $method
     * @return bool
     * @throws \Exception
     * @zqphp:hide
     */
    public static function affair($method)
    {
        return self::link()->affair($method);
    }

    /**要复制到指定数据库
     * @param $name
     * @param string $link
     * @return Mysql
     * @throws \Exception
     * @zqphp:hide
     */
    public static function copy($name, $link = '')
    {
        return self::link()->copy($name, $link);
    }

    /**复制创建数据库表单
     * @param $tab_1
     * @param $tab_2
     * @return array|mixed|string
     * @throws \Exception
     * @zqphp:hide
     */
    public static function creste($tab_1, $tab_2)
    {
        return self::link()->creste($tab_1, $tab_2);
    }

    /**判断mysql数据库中表单是否存在
     * @param $table
     * @return bool
     * @throws \Exception
     * @zqphp:hide
     */
    public static function mysql_if_table($table)
    {
        return self::link()->mysql_if_table($table);
    }

    /**判断sqlite数据库中表单是否存在
     * @param $table
     * @return bool
     * @throws \Exception
     * @zqphp:hide
     */
    public static function sqlite_if_table($table)
    {
        return self::link()->sqlite_if_table($table);
    }

    /**预处理语句及绑定参数执行方式
     * @param string $sql 预处理语句
     * @param array $data 绑定参数
     * @param string $type 返回类型(现在有list/id/row)
     * @return array|mixed
     * @throws \Exception
     * @zqphp:hide
     */
    public static function prepare($sql, $data = [], $type = '')
    {
        return Mysql::prepare($sql, $data, $type, self::getconfig(0)[0]);
    }

    /**
     * @param string $sql 执行的sql语句
     * @param string $type 返回类型(现在有list/id/row)
     * @return mixed
     * @throws \Exception
     * @zqphp:hide
     */
    public static function query($sql, $type = '')
    {
        return Mysql::query($sql, $type, self::getconfig(0)[0]);
    }

    /**exec执行
     * @param string $sql 执行的sql语句
     * @return int
     * @throws \Exception
     * @zqphp:hide
     */
    public static function execs($sql)
    {
        return Mysql::execs($sql, self::getconfig(0)[0]);
    }

    /**设置数据库配置信息
     * @return array|mixed
     * @zqphp:hide
     */
    private static function config()
    {
        return [!empty(self::$name) ? self::$name : 'user', !empty(self::$link) ? self::$link : false, !empty(self::$base) ? self::$base : false];
    }

    /**
     * @param string $key
     * @return array|mixed
     * @zqphp:hide
     */
    private static function getconfig($key = '')
    {
        $data = self::config();
        if (isset($data[2]) and !empty($data[2])) Mysql::$basearray = $data[2];
        return !empty($key) ? (isset($data[$key]) ? $data[$key] : $data) : $data;
    }
}
