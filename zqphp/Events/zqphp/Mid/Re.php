<?php

namespace zqphp\Mid;

use zqphp\Lib\Redis;

/**
 * 使用use复制
 * use \zqphp\Mid\Re;
 * public static $name='';//数据库配置名称
 * public static $link='';//附加数据库key//主数据库请为0
 * public static $base=false;//配置信息false使用默认 字符串为调文件，数组为配置
 * Trait Re
 * @package zqphp\Redis
 */
trait Re
{

    /**指定配置信息
     * @return Redis
     * @throws \Exception
     * @zqphp:hide
     */
    public static function link()
    {
        $data = self::getconfig();
        return Redis::link($data[0])->select($data[1]);
    }

    /**设置
     * @param $name
     * @param $data
     * @return mixed
     * @throws \Exception
     * @zqphp:hide
     */
    public static function set($name, $data)
    {
        return self::link()->set($name, $data);
    }

    /**获取
     * @param $name
     * @return mixed
     * @throws \Exception
     * @zqphp:hide
     */
    public static function get($name)
    {
        return self::link()->get($name);
    }

    /**设置数据库配置信息
     * @return array|mixed
     * @zqphp:hide
     */
    public static function config()
    {
        return [!empty(self::$name) ? self::$name : 'all', !empty(self::$link) ? self::$link : 1, !empty(self::$base) ? self::$base : false];
    }

    /**设置配置信息
     * @param $key
     * @return array|mixed
     * @zqphp:hide
     */
    public static function getconfig($key = '')
    {
        $data = self::config();
        if (isset($data[2]) and !empty($data[2])) Redis::$basearray = $data[2];
        return !empty($key) ? (isset($data[$key]) ? $data[$key] : $data) : $data;
    }
}
