<?php

use zqphp\AutoLoad;

class Request {

    /**
     * 获取GET
     * @param null $Key
     * @param null $Default
     * @return mixed
     */
    public static function get($Key = null, $Default = null) {
        return AutoLoad::SelfFun('Get', [$Key, $Default]);
    }

    /**
     * 获取POST
     * @param null $Key
     * @param null $Default
     * @return mixed
     */
    public static function post($Key = null, $Default = null) {
        return AutoLoad::SelfFun('Post', [$Key, $Default]);
    }

    /**
     * 获取当前域名
     * @param bool $Type //true不带端口
     * @return mixed
     */
    public static function host($Type = false) {
        return AutoLoad::SelfFun('GetHost', [$Type]);
    }

    /**
     * 获取来源域名
     * @param bool $Type //true只返回域名
     * @return string
     */
    public static function referer($Type = false) {
        return AutoLoad::GetReferer($Type);
    }

    /**
     * 获取Ip
     * @return mixed
     */
    public static function getip() {
        return AutoLoad::SelfFun('GetIp');
    }

    /**
     * 获取本地ip
     * @return array|string
     */
    public static function localip() {
        return AutoLoad::SelfFun('GetLocalIp');
    }

    /**
     * 获取头部信息
     * @param null $Key
     * @param null $Default
     * @return array|string
     */
    public static function header($Key = null, $Default = null) {
        return AutoLoad::SelfFun('GetHeader', [$Key, $Default]);
    }

    /**
     * 获取上传流
     * @return false|string
     */
    public static function input() {
        return AutoLoad::SelfFun('GetInput');
    }

    /**
     * 获取上传文件
     * @param null $Key
     * @param null $Default
     * @return array|string
     */
    public static function file($Key = null, $Default = null) {
        return AutoLoad::SelfFun('GetFile', [$Key, $Default]);
    }


    /**
     * 获取协议
     * @return array|string
     */
    public static function scheme() {
        return AutoLoad::SelfFun('GetScheme');
    }

    /**
     * 获取请求HTTP版本
     */
    public static function protocolversion() {
        return AutoLoad::SelfFun('GetProtocolVersion');
    }

    /**
     * 获取当前URl get.php?uid=10&type=2时将返回get.php?uid=10&type=2
     * @return mixed
     */
    public static function uri() {
        return AutoLoad::SelfFun('GetUri');
    }

    /**
     * 获取当前URl 请求路径 get.php?uid=10&type=2时将返回/user/get.php
     * @return string
     */
    public static function path() {
        return AutoLoad::SelfFun('GetPath');
    }

    /**
     * 获取当前URl get.php?uid=10&type=2时将返回uid=10&type=2
     * @return mixed
     */
    public static function querystring() {
        return AutoLoad::SelfFun('QueryString');
    }

    /**
     * 获取请求方法 返回值可能是GET、POST、PUT、DELETE、OPTIONS、HEAD中的一个
     * @return mixed
     */
    public static function method() {
        return AutoLoad::SelfFun('GetMethod');
    }
}