<?php

use zqphp\AutoLoad;

class Session {
    /**
     * 单个存储
     * 多个存储格式 ['名称'=>'内容','名称'=>'内容','名称'=>'内容']
     * @param null|array|string $Key
     * @param null $Data
     */
    public static function set($Key, $Data = null) {
        AutoLoad::SelfFun('SetSession', [$Key, $Data]);
    }

    /**
     * 获取
     * @param null|string $Key //null获取全部
     * @param null $Default
     * @return mixed
     */
    public static function get($Key = null, $Default = null) {
        return AutoLoad::SelfFun('GetSession', [$Key, $Default]);
    }

    /**
     * 单个、多个、删除数据
     * @param null|array $Key //null=全部删除
     */
    public static function delete($Key = null) {
        AutoLoad::SelfFun('DeleteSession', [$Key]);
    }

    /**
     * 获取并删除
     * @param null $Key
     * @return mixed
     */
    public static function pull($Key) {
        return AutoLoad::SelfFun('PullSession', [$Key]);
    }

    /**
     * 判断是否存在
     * @param null $Key //不管有没值，key存在就返回true
     * @return mixed
     */
    public static function has($Key) {
        return AutoLoad::SelfFun('HasSession', [$Key]);
    }

    /**
     * 获取SessionId
     * @return mixed
     */
    public static function id() {
        return AutoLoad::SelfFun('SessionId');
    }
}