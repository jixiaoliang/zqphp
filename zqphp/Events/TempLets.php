<?php

use zqphp\AutoLoad;

trait TempLets {

    protected $TplData = [];//模板参数

    /**
     * 分配参数
     * @param string|array $Name
     * @param $Data
     * @param $Type //true=使用a.b.c.d.f创建数组,不能存对像
     */
    protected function assign($Name, $Data = null, $Type = false) {
        if (empty(is_array($Name))) {
            if ($Type && empty(is_object($Data))) {
                $this->TplData = array_merge_recursive($this->TplData, AutoLoad::StrArr($Name, $Data));
            } else {
                $this->TplData[$Name] = $Data;
            }
        } else if (!empty($Name)) {
            foreach ($Name as $k => $v) {
                $this->TplData[$k] = $v;
            }
        }
    }

    /**
     * 获得处理
     * @param string|null $Name //支持aa.bb.cc.dd这样获取数组内容
     * @param $Default
     * @return bool|mixed
     */
    protected function obtain($Name = null, $Default = null) {
        if (!isset($Name)) {
            return $this->TplData;
        }
        return isset($this->TplData[$Name]) ? $this->TplData[$Name] : AutoLoad::ObtainArr($this->TplData, $Name, $Default);
    }

    /**
     * 普通引入(不编译模板)
     * @param null $File
     * @param bool $Type
     */
    protected function show($File = null, $Type = false) {
        $this->ReqFile(AutoLoad::HandleShow($File, $Type));
    }

    /**
     * 使用模板引入(编译模板保存编译文件引入)
     * @param null|string|bool|array $File //支持array多个文件引入
     * @param bool $Type //是否生成静态
     */
    protected function html($File = null, $Type = false) {
        $this->ReqFile(AutoLoad::HandleHtml($File, $Type));
    }

    /**
     * 引入文件
     * @param $File
     */
    private function ReqFile($File) {
        if (empty(is_array($File))) {
            require $File;
        } elseif (!empty($File)) {
            foreach ($File as $v) {
                require $v;
            }
        }
    }
}