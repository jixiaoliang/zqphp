<?php

namespace zqphp;
class AutoLoad {
    private static $Listen;
    private static $Context = [];
    private static $IsCli = false;
    private static $CliData = [];
    private static $ItemConfig = [];
    private static $MimeData = [];
    private static $ErrInfo = [];
    private static $RunType = 'Ani';
    private static $SetWorker = [];
    private static $SetFileClass = [];
    private static $SetDirClass = [];
    private static $SetRouting = [];
    private static $AccessData = [];
    private static $AliasData = [];
    private static $RoutingStop = [];
    private $RunObj;
    private $RunReq;
    private $RunRes;
    private $RunData = [];
    private $RoutingData = [];
    private $IsLoad = false;
    private $HeaderInfo = [];
    private $HeaderStatus = null;
    private $HeadSend = [];
    private $IsHtml = false;
    private $ViewFileList = [];
    private $StartTime;

    public function __construct($ItemDir = null, $Type = 'array') {
        if (empty(self::$ItemConfig)) {
            $this->StartTime = microtime(true);
            self::CliProcessData();
            $ItemDir = isset($ItemDir) ? self::RTrim($ItemDir, true) : self::StartDir();
            $DefaultConfigFile = self::LoadDir() . 'Config/Config.php';
            $DefaultConfig = self::RetFile($DefaultConfigFile);
            if (is_string($Type)) {
                if ($Type == 'json') {
                    $ItemConfigFile = $ItemDir . 'Config/Config.Cache';
                    if (empty(is_file($ItemConfigFile))) {
                        self::MkDir(self::DelDir($ItemConfigFile));
                        file_put_contents($ItemConfigFile, self::JsonFormat($DefaultConfig, true));
                    }
                    $ItemConfig = self::IsJson(file_get_contents($ItemConfigFile), true);
                } else {
                    $ItemConfigFile = $ItemDir . 'Config/Config.php';
                    if (empty(is_file($ItemConfigFile))) {
                        self::MkDir(self::DelDir($ItemConfigFile));
                        file_put_contents($ItemConfigFile, file_get_contents($DefaultConfigFile));
                    }
                    $ItemConfig = self::RetFile($ItemConfigFile);
                }
            } else if (empty($Type)) {
                $ItemConfig['PhpDir'] = null;
            }
            $ItemConfig['ItemDir'] = $ItemDir;
            self::$ItemConfig = array_merge($DefaultConfig, $ItemConfig);
            if (!empty($PublicFile = array_merge(glob(self::LoadDir() . 'Public/*.php'), glob(self::ConfigDir('PublicDir') . '*.php')))) {
                foreach ($PublicFile as $PhpFile) {
                    self::RetFile($PhpFile);
                }
            }
            spl_autoload_register('self::WorkerAutoloadFile');
        }
    }

    public function Run($Listen = null, $Context = []) {
        self::$Listen = $Listen ? $Listen : 'http://0.0.0.0:2020';
        self::$Context = $Context;
        $Type = self::IsArrData(self::$CliData, 't');
        self::$RunType = self::$IsCli ? (!empty(self::$CliData) ? ($Type ? $Type : 'Cli') : 'Worker') : self::$RunType;
        $this->FunName('Start');
    }

    public static function LoadDir() {
        return __DIR__ . self::Separator();
    }

    public static function OpenLoad($ItemDir = null, $Type = null) {
        $Self = new self($ItemDir, $Type);
        \Workerman\Worker::$logFile = self::StartDir() . 'Worker.log';
        return $Self;
    }

    public static function AniSetCookie($Key, $Value = null, $MaxAge = 0, $Path = '', $Domain = '', $Secure = false, $Only = false) {
        if (empty(is_array($Key))) {
            setcookie($Key, $Value, (time() + $MaxAge), $Path, $Domain, $Secure, $Only);
        } else if (!empty($Key)) {
            foreach ($Key as $k => $v) {
                call_user_func_array('self::AniSetCookie', $v);
            }
        }
    }

    private static function AniGetCookie($Key = null, $Default = null) {
        return self::IsArrData($_COOKIE, $Key, $Default);
    }

    private static function AniDeleteCookie($Key = null) {
        if (!isset($Key)) {
            if (!empty($_COOKIE)) {
                foreach ($_COOKIE as $k => $v) {
                    self::AniSetCookie($k, null, -10);
                }
            }
        } elseif (empty(is_array($Key))) {
            if (isset($_COOKIE[$Key])) {
                self::AniSetCookie($Key, null, -10);
            }
        } else if (!empty($Key)) {
            foreach ($Key as $v) {
                self::AniSetCookie($v, null, -10);
            }
        }
    }

    private static function AniPullCookie($Key, $Default = null) {
        $GetSession = self::AniGetCookie($Key, $Default);
        self::AniDeleteCookie($Key);
        return $GetSession;
    }

    private static function AniHasCookie($Key) {
        return isset($_COOKIE[$Key]) ? true : false;
    }

    private function AniStart() {
        $this->RunAll();
    }

    private function AniRunAll() {
        $Content = self::ObStart(function () {
            $Data = self::TryCatch(function () {
                $this->SaveUrl();
                if (strtolower(trim($this->RunData['Url'], '/')) == 'favicon.ico') {
                    self::ErrHtml();
                    return;
                }
                if ($this->CensorHtmlTime()) return;
                $this->HandleRouting();
                $this->ProcessLoad();
            });
            $this->ErrInfo($Data);
        });
        if (empty($this->HeadSend)) {
            if (!empty($this->HeaderInfo) || !empty($this->HeaderStatus)) {
                self::AniHeader();
            }
            $this->SaveHtml($Content);
            echo $Content;
        } else {
            self::SelfFun(['self', 'AniSendFileData'], $this->HeadSend);
        }
    }

    private static function AniGetUrl() {
        $Path = ltrim(self::AniGetPath(), '/');
        return !empty($Path) ? self::PathRoot($Path) : '/';
    }

    private static function GetOldPath() {
        $Path = ltrim(parse_url(self::IsArrData($_SERVER, 'REQUEST_URI'), PHP_URL_PATH), '/');
        $Path = self::DelUrlHomeFile($Path, true);
        return !empty($Path) ? $Path : '/';
    }

    private static function AniGetIp() {
        $arr_ip_header = ['HTTP_CDN_SRC_IP', 'HTTP_PROXY_CLIENT_IP', 'HTTP_WL_PROXY_CLIENT_IP', 'HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'REMOTE_ADDR'];
        $client_ip = 'unknown';
        foreach ($arr_ip_header as $key) {
            $Ip = self::IsArrData($_SERVER, $key);
            if ($Ip && $Ip != $client_ip) {
                return $Ip;
            }
        }
        return $client_ip;
    }

    private static function AniGetHost($Type = false) {
        return $Type ? self::IsArrData($_SERVER, 'SERVER_NAME', false) : self::IsArrData($_SERVER, 'HTTP_HOST', false);
    }

    private static function AniGet($Key = null, $Default = null) {
        return self::IsArrData($_GET, $Key, $Default);
    }

    private static function AniPost($Key = null, $Default = null) {
        return self::IsArrData($_POST, $Key, $Default);
    }

    private static function AniGetHeader($Key = null, $Default = null, $HeadData = []) {
        foreach ($_SERVER as $k => $v) {
            if (substr($k, 0, 5) == 'HTTP_') {
                $HeadData[str_replace(' ', '-', strtolower(str_replace('_', ' ', substr($k, 5))))] = $v;
            }
        }
        return self::IsArrData($HeadData, ($Key ? strtolower(self::StrReplace($Key, '_', '-')) : $Key), $Default);
    }

    private static function AniGetUri() {
        return self::IsArrData($_SERVER, 'REQUEST_URI', '');
    }

    private static function AniGetPath() {
        if (isset($_SERVER['REDIRECT_URL'])) {
            return $_SERVER['REDIRECT_URL'];
        } else if (isset($_SERVER['PATH_INFO'])) {
            return $_SERVER['PATH_INFO'];
        }
        return '/';
    }

    private static function AniGetQueryString() {
        return self::IsArrData($_SERVER, 'REDIRECT_QUERY_STRING', '');
    }

    private static function AniGetMethod() {
        return self::IsArrData($_SERVER, 'REQUEST_METHOD', 'GET');
    }

    private static function AniGetInput() {
        return file_get_contents("php://input");
    }

    private static function AniGetFile($Key = null, $Default = null) {
        return self::IsArrData($_FILES, $Key, $Default);
    }

    private static function AniGetLocalIp() {
        return self::IsArrData($_SERVER, 'SERVER_ADDR');
    }

    private static function AniGetScheme() {
        return self::IsArrData($_SERVER, 'REQUEST_SCHEME');
    }

    private static function AniGetProtocolVersion() {
        $Version = self::IsArrData($_SERVER, 'SERVER_PROTOCOL');
        $VersionData = substr(strstr($Version, 'HTTP/'), 5);
        return $VersionData ? $VersionData : '1.0';
    }

    private static function AniSendFileData($FileData, $Size, $Mime, $NewFile, $Type, $SendSize = 0) {
        if ($Type) {
            if (self::HeaderModified($FileData)) {
                self::AniHeader(304);
                return;
            }
            if (is_numeric($SendSize)) {
                $HeadInfo = self::BigHeadInfo($FileData, $Mime, $Size, $NewFile);
                self::AniHeader($HeadInfo['Status'], $HeadInfo['HeaderData']);
                self::AniSendBigData($FileData, $SendSize, $HeadInfo['Is'], $HeadInfo['Begin'], $HeadInfo['End']);
                return;
            }
            self::AniHeader(self::HeaderContent(['size' => $Size, 'type' => $Mime, 'file' => $NewFile, 'last' => $FileData]));
            echo readfile($FileData);
            return;
        }
        self::AniHeader(self::HeaderContent(['size' => $Size, 'type' => $Mime, 'file' => $NewFile]));
        echo $FileData;
    }

    private static function AniSendBigData($File, $SendSize, $Is, $Begin, $End) {
        if (empty($Is)) {
            $Fp = fopen($File, 'rb');
            fseek($Fp, $Begin);
            $P = $SendSize;
            while (!feof($Fp) && ($Current = ftell($Fp)) <= $End) {
                if ($Current + $P > $End) {
                    $P = $End - $Current + 1;
                }
                $Buffer = fread($Fp, $P);
                if ($Buffer === '' || $Buffer === false) {
                    return;
                }
                echo $Buffer;
            }
            fclose($Fp);
        }
    }

    private static function AniHeader($Status = null, $Header = []) {
        $Status = $Status ? $Status : self::GetThis('HeaderStatus');
        if (is_numeric($Status)) {
            header(self::HeaderServer($Status));
        } elseif (!empty($Status) && is_array($Status)) {
            $Header = $Status;
        }
        $HeaderInfo = array_merge($Header, self::GetThis('HeaderInfo'));
        if (!empty($HeaderInfo)) {
            foreach ($HeaderInfo as $k => $v) {
                header($k . ':' . $v);
            }
        }
    }

    private static function AniSetSession($Key, $Value = null) {
        if (!isset($_SESSION)) session_start();
        if (empty(is_array($Key))) {
            $_SESSION[$Key] = $Value;
        } else if (!empty($Key)) {
            foreach ($Key as $k => $v) {
                $_SESSION[$k] = $v;
            }
        }
    }

    private static function AniGetSession($Key = null, $Default = null) {
        if (!isset($_SESSION)) session_start();
        return self::IsArrData($_SESSION, $Key, $Default);
    }

    private static function AniDeleteSession($Key = null) {
        if (!isset($_SESSION)) session_start();
        if (!isset($Key)) {
            $_SESSION = [];
        } elseif (empty(is_array($Key))) {
            if (isset($_SESSION[$Key])) {
                unset($_SESSION[$Key]);
            }
        } else if (!empty($Key)) {
            foreach ($Key as $v) {
                unset($_SESSION[$v]);
            }
        }
    }

    private static function AniPullSession($Key, $Default = null) {
        $GetSession = self::AniGetSession($Key, $Default);
        self::AniDeleteSession($Key);
        return $GetSession;
    }

    private static function AniHasSession($Key) {
        if (!isset($_SESSION)) session_start();
        return isset($_SESSION[$Key]) ? true : false;
    }

    private static function AniSessionId() {
        if (!isset($_SESSION)) session_start();
        return session_id();
    }

    private function CliStart() {
        $this->RunAll();
    }

    private function CliRunAll() {
        if (empty($this->OrderK())) {
            $Content = self::ObStart(function () {
                $Data = self::TryCatch(function () {
                    $this->SaveUrl();
                    if ($this->CensorHtmlTime()) return;
                    $this->HandleRouting();
                    $this->ProcessLoad();
                });
                $this->ErrInfo($Data);
            });
            if (empty($this->HeadSend)) {
                if (!empty($this->HeaderInfo) || !empty($this->HeaderStatus)) {
                    self::AniHeader();
                }
                $this->SaveHtml($Content);
                $Sort = $this->RunData['Sort'];
                $Method = $this->RunData['Method'];
                $AccessType = $this->RunData['AccessType'];
                if ($AccessType == 'View') {
                    echo "视图文件：" . $this->RunData['ViewFile'] . "\r\n";
                } else if ($AccessType == 'Php') {
                    echo "加载类名：" . $Sort . "\r\n";
                    echo "加载方法：" . $Method . "\r\n";
                    echo "加载文件：" . self::SepFile($this->RunData['PhpFile']) . "\r\n";
                } else if ($AccessType == 'Html') {
                    echo "加载类名：" . $Sort . "\r\n";
                    echo "加载方法：" . $Method . "\r\n";
                    echo "静态文件：" . $this->HtmlFile() . "\r\n";
                } else if (!empty($AccessType)) {
                    echo $AccessType . "方法访问\r\n";
                }
                if (!empty($Process = $this->RunData['Process'])) {
                    echo "路由别名：" . $Process . "\r\n";
                }
                self::CliPrint($Content);
            } else {
                self::SelfFun(['self', 'CliSendFileData'], $this->HeadSend);
            }
        }
    }

    private static function CliProcessData() {
        self::$IsCli = self::IsCli();
        if (self::$IsCli) {
            global $argv;
            self::$CliData = self::HandleCliData(getopt('u:k:n:t:'), $argv);
        }
    }

    private static function HandleCliData($GetOpt, $Argv) {
        $ArgvOne = self::IsArrData($Argv, 1);
        if (!empty($ArgvOne) && empty(self::StrPos('start,stop,restart,reload,status,connections', $ArgvOne))) {
            $ArgvTwo = self::IsArrData($Argv, 2);
            switch (strtolower($ArgvOne)) {
                case 'swoole':
                    $GetOpt['t'] = ucfirst($ArgvOne);
                    $GetOpt['k'] = $ArgvTwo;
                    break;
                case 'mime':
                    $GetOpt['k'] = $ArgvOne;
                    $GetOpt['n'] = $ArgvTwo;
                    break;
                case 'config':
                case 'deploy':
                case 'error':
                case 'help':
                    $GetOpt['k'] = $ArgvOne;
                    break;
                default:
                    if (empty(self::StrPos('-u,-k,-n,-t', $ArgvOne))) {
                        $GetOpt['u'] = $ArgvOne;
                    }
                    break;
            }
        }
        return $GetOpt;
    }

    private function OrderK() {
        $k = self::IsArrData(self::$CliData, 'k');
        if (empty($k)) {
            return false;
        }
        $n = self::IsArrData(self::$CliData, 'n', null);
        switch (strtolower($k)) {
            case 'config':
                self::CliPrint(self::GetConfig());
                break;
            case 'mime':
                self::CliPrint(self::GetMime($n));
                break;
            case 'error':
                self::CliPrint(self::ErrData());
                break;
            case 'help':
            default:
                $CliData = "Workerman命令：start -d|start|stop|restart|reload|status|connections\r\n";
                $CliData .= "-k  命令(string)   (help,config,mime,error) 命令有冲突时使用-k\r\n";
                $CliData .= "-u  地址(string)   访问地址和命令有冲突时使用-u\r\n";
                $CliData .= "-n  参数(string)   Mime查询的参数";
                self::CliPrint($CliData);
        }
        return true;
    }

    private static function CliPrint($Data) {
        echo "----------------------------------Zqphp----------------------------------\r\n";
        print_r($Data);
        echo "\r\n---------------------------" . date('Y-m-d H:i:s') . "---------------------------\r\n";
    }

    private static function CliGetUrl() {
        $Path = ltrim(self::CliGetUri(), '/');
        return !empty($Path) ? $Path : '/';
    }

    private static function CliGetUri() {
        return self::IsArrData(self::$CliData, 'u', '/');
    }

    private static function CliGetPath() {
        return parse_url(self::CliGetUri(), PHP_URL_PATH);
    }

    private static function CliGetQueryString() {
        return parse_url(self::CliGetUri(), PHP_URL_QUERY);
    }

    private static function CLiGetMethod() {
        return 'GET';
    }

    private static function CliSendFileData($FileData, $Size, $Mime, $NewFile, $Type, $SendSize = 0) {
        if ($Type) {
            echo '文件：' . $FileData . "\r\n";
        }
        echo '头部：' . $Mime . "\r\n";
        if ($NewFile) {
            echo '下载：' . $NewFile . "\r\n";
        }
        echo '大小：' . $Size . "\r\n";
    }

    private function DebugShow($ErrorData = null) {
        $Data = $this->DebugInfo();
        require self::LoadDir() . 'Config/Debug.php';
    }

    private function DebugInfo() {
        $DebugData['LoadTime'] = $this->DebugTime();
        $DebugData['Req'] = $this->DebugReq($DebugData['LoadTime']);
        $DebugData['Ram'] = $this->DebugRam();
        $DebugData['Header'] = self::ObStart(function () {
            print_r(self::SelfFun('GetHeader'));
        });
        $DebugData['Info'] = date('Y-m-d H:i:s') . ' HTTP/' . self::SelfFun('GetProtocolVersion') . ' ' . self::SelfFun('GetMethod') . '：' . self::SelfFun('GetUri');
        $DebugData['Process'] = $this->RunData['Process'];
        $DebugData['LoadMode'] = $this->LoadMode();
        $DebugData['FileList'] = $this->DebugLoadFile();
        $DebugData['Amount'] = count($DebugData['FileList']);
        return $DebugData;
    }

    private function DebugTime() {
        return number_format((microtime(true) - $this->StartTime), 6);
    }

    private function DebugReq($LoadTime) {
        return number_format(1 / $LoadTime, 2);
    }

    private function DebugRam() {
        return number_format(memory_get_usage() / 1024, 2);
    }

    private function DebugLoadFile($Data = []) {
        $FileList = get_included_files();
        foreach ($FileList as $k => $v) {
            $Data[] = ($k + 1) . ' : ' . $v . ' ( ' . (is_file($v) ? (number_format(filesize($v) / 1024, 2)) . ' KB' : 'file does not exist') . ' )';
        }
        return $Data;
    }

    private function LoadMode() {
        $AccessType = $this->RunData['AccessType'];
        if ($AccessType == 'View') {
            return [self::SepFile($this->RunData['ViewFile'])];
        } else if ($AccessType == 'Php') {
            return [self::SepFile($this->RunData['PhpFile']), $this->RunData['Sort'], $this->RunData['Method']];
        } else if ($AccessType == 'Html') {
            return [self::SepFile($this->HtmlFile()), $this->RunData['Sort'], $this->RunData['Method']];
        } else if (!empty($AccessType)) {
            return [$AccessType . "方法访问"];
        }
    }

    private static function ErrData($Name = null, $Key = null, $Default = null) {
        if (empty(self::$ErrInfo)) {
            self::$ErrInfo = self::RetFile(self::LoadDir() . 'Config/ErrInfo.php');
        }
        $K = key(self::$ErrInfo);
        if (!isset($Name)) {
            return self::$ErrInfo[$K];
        }
        $ErrShow = self::IsArrData(self::$ErrInfo[$K], $Name, []);
        return self::IsArrData($ErrShow, (isset($Key) ? $Key : $K), $Default);
    }

    private static function TryCatch($Method) {
        try {
            return $Method();
        } catch (\Exception $E) {
            return $E;
        } catch (\Error $E) {
            return $E;
        }
    }

    private static function ThrowNew($Name, $Data = null) {
        $ErrorData = self::ErrData($Name, 'code') . '|' . self::ErrData($Name) . (isset($Data) ? '(' . $Data . ')' : '');
        throw new \Exception($ErrorData, 22);
    }

    public static function Come($Data = null) {
        throw new \Exception($Data, 18);
    }

    private function ErrInfo($Data = null) {
        if (!empty($ErrorGetLast = error_get_last())) {
            $this->ErrHandle($ErrorGetLast, 1);
        } else {
            $this->ErrHandle($Data, 2);
        }
    }

    private function ErrHandle($E, $Type) {
        if ($Type == 2 && !empty($E)) {
            if ($E->getCode() == 18) {
                if (!empty($Content = $E->getMessage())) {
                    echo $Content;
                }
                return;
            } else if ($E->getCode() == 22) {
                $Type = 3;
                $getCode = explode('|', $E->getMessage());
                $ErrorData = ['type' => self::IsArrData($getCode, 0, $E->getCode()), 'message' => self::IsArrData($getCode, 1, $E->getMessage()),];
            } else {
                $ErrorData = ['type' => $E->getCode(), 'message' => $E->getMessage(), 'file' => $E->getFile(), 'line' => $E->getLine()];
            }
        }
        $ErrorData = !empty($ErrorData) ? $ErrorData : $E;
        if (self::GetConfig('DebugSwitch') && self::$RunType != 'Cli') {
            $this->DebugShow($ErrorData);
        } else if (!empty($ErrorData)) {
            self::SaveLog($ErrorData, $Type, $E);
        }
    }

    private static function SaveLog($Data, $Type, $E) {
        $ErrType = self::IsArrData($Data, 'type');
        if (!empty(self::GetConfig('IsLog', false))) {
            $LogDir = self::HtmlLogDir('LogDir');
            self::MkDir($LogDir);
            $LogSize = self::GetConfig('LogSize', 1024) * 1024;
            $ErrMessage = self::IsArrData($Data, 'message');
            $ErrFile = self::IsArrData($Data, 'file');
            $ErrLine = self::IsArrData($Data, 'line');
            $LogData = date('Y-m-d H:i:s') . ' code ' . $ErrType . ' ip ' . self::SelfFun('GetIp') . ' ' . self::$RunType . ' access  "' . $ErrMessage;
            switch ($Type) {
                case 1:
                    $LogFile = $LogDir . 'general';
                    $LogData .= ' ' . $ErrFile . ' on line ' . $ErrLine . '"';
                    break;
                case 2:
                    $LogFile = $LogDir . 'system';
                    $LogData .= ' ' . $ErrFile . ' on line ' . $ErrLine . '"';
                    break;
                default:
                    $LogFile = $LogDir . 'program';
                    break;
            }
            $NewLogFiles = $LogFile . '.log';
            if (is_file($NewLogFiles) && filesize($NewLogFiles) >= $LogSize) rename($NewLogFiles, $LogFile . '_' . date('YmdHis') . '.log');
            $FileObj = fopen($NewLogFiles, 'a');
            fwrite($FileObj, $LogData . "\r\n");
            fclose($FileObj);
        }
        $Error = self::GetConfig('Error');
        if (is_string($Error)) {
            echo "<script language=\"javascript\">top.location='" . $Error . "';</script>";
        } else if ($Error === true) {
            self::ps($Data);
        } else if ($Error === false) {
            self::ErrHtml($ErrType);
        } else {
            self::ErrHtml();
        }
    }

    private static function ErrHtml($Code = '') {
        if (!empty($ErrHtml = self::GetConfig('ErrHtml'))) {
            $ErrFile = self::SepFile(self::DirName($ErrHtml, self::GetConfig('ItemDir') . 'Config/'));
            if (is_file($ErrFile)) {
                require $ErrFile;
                return;
            }
        }
        $HtmlData = "<html><head>\r\n";
        $HtmlData .= "<title>404 File not found</title>\r\n";
        $HtmlData .= "</head><body>\r\n";
        $HtmlData .= "<h3>404 Not Found{$Code}</h3>\r\n";
        $HtmlData .= "</body></html>\r\n";
        echo $HtmlData;
    }

    private function SaveUrl() {
        $Routing = strtoupper(self::GetConfig('Routing'));
        $Mode = strtoupper(self::SelfFun('GetMethod'));
        $URL = (self::$RunType == 'Cli' || self::StrPos($Routing, $Mode)) ? $this->ObtainUrl($Mode) : '/';
        $RoutingData = $this->DomainRouting($URL);
        $this->RunData = $this->UrlHandle($RoutingData, $Mode);
    }

    private function ObtainUrl($Mode) {
        switch ($Mode) {
            case 'GET':
                $Url = self::SelfFun('GetUrl');
                break;
            case 'POST':
                $Url = self::GainData(self::SelfFun('Post'));
                $Url = self::ObtainGETUrl($Url);
                break;
            case 'HEAD':
                $Url = self::GainData(self::SelfFun('GetHeader'));
                $Url = self::ObtainGETUrl($Url);
                break;
        }
        return !empty($Url) ? $Url : '/';
    }

    private static function ObtainGETUrl($URL) {
        if ($URL == '/' && self::StrPos(strtoupper(self::GetConfig('Routing')), 'GET')) {
            $URL = self::SelfFun('GetUrl');
        }
        return $URL;
    }

    private static function GainData($Data) {
        $Field = explode(',', self::GetConfig('CallField'));
        $Place = self::IsArrData($Field, 0, 'place');
        $Url = ltrim(self::IsArrData($Data, trim($Place), ''), '/');
        $Matter = self::IsArrData($Field, 1, false);
        $Method = $Matter ? self::IsArrData($Data, trim($Matter), '') : '';
        if ($Method) $Url = $Url . '/' . ltrim($Method, '/');
        return !empty($Url) ? $Url : '/';
    }

    private function DomainRouting($Url, $Data = null) {
        if (!empty(self::$SetRouting)) {
            $Sort = self::GetConfig('Sort', 'index');
            $Host = self::SelfFun('GetHost');
            foreach (self::$SetRouting as $m) {
                if (is_callable($m)) {
                    $InfoArr = $m($this, $this->RunData);
                    if (is_array($InfoArr) && !empty($InfoArr)) {
                        $NewArr = self::IsTwoArr($InfoArr);
                        foreach ($NewArr as $v) {
                            if (!empty(self::DomainPos(self::IsArrData($v, 1, ''), $Host))) {
                                $Url = trim(self::StrReplace(self::IsArrData($v, 0), '\\', '/'), '/') . '/' . (($Url == '/' || $Url == $Sort) ? $Sort . '/' : $Url);
                                $Data = self::IsArrData($v, 2);
                                break;
                            }
                        }
                    }
                }
            }
        }
        return [$Data, $Url];
    }

    private function UrlHandle($RoutingData, $Mode, $Path = '/', $Get = []) {
        $Sort = self::GetConfig('Sort', 'index');
        $Method = self::GetConfig('Method', 'main');
        $Url = self::IsArrData($RoutingData, 1);
        $NewUrl = trim($Url, '/');
        $Format = self::PathInfo($NewUrl);
        if (!empty($NewUrl)) {
            $Parameter = self::GetConfig('Parameter');
            $DelUrlFormat = self::DelPath($Url);
            $UrlFile = self::BaseName($DelUrlFormat);
            $DelUrlDir = self::DelDir($DelUrlFormat);
            $Gets = explode($Parameter, trim($UrlFile));
            $Sort = $Gets[key($Gets)];
            $Path = ($Sort != self::GetConfig('Sort', 'index')) ? trim($DelUrlDir . '/' . $Sort, '/') : '/';
            $Get = (count($Gets) > 1 ? array_slice($Gets, 1) : []);
            if (count(explode('/', $NewUrl)) > 1) {
                if (substr($Url, -1) == '/') {
                    $Sort = trim($DelUrlDir . '/' . $UrlFile, '/');
                    $Get = [];
                } else {
                    $Method = $Sort;
                    $Sort = $DelUrlDir;
                }
            }
        }
        return ['Sort' => $Sort, 'Method' => $Method, 'Path' => $Path, 'Mode' => $Mode, 'Url' => $Url, 'Format' => $Format, 'Get' => $Get, 'Data' => self::IsArrData($RoutingData, 0), 'AccessType' => null, 'Process' => null];
    }

    public function SetAlias($NewName, $OldName = null, $Type = '') {
        if (is_array($NewName)) {
            if (!empty($NewName)) {
                foreach ($NewName as $k => $v) {
                    $this->SetAlias($k, self::IsArrData($v, 0), self::IsArrData($v, 1));
                }
            }
            return $this;
        }
        self::$AliasData = $this->ProcessSetRoutingData($NewName, $OldName, $Type, self::$AliasData);
        return $this;
    }

    public function SetStop($Name, $Type = '') {
        if (is_array($Name)) {
            if (!empty($Name)) {
                foreach ($Name as $k => $v) {
                    if (is_numeric($k)) {
                        $this->SetStop($v, false);
                    } else {
                        $this->SetStop($k, $v);
                    }
                }
            }
            return $this;
        }
        self::$RoutingStop = $this->ProcessSetRoutingData($Name, null, $Type, self::$RoutingStop);
        return $this;
    }

    public function Access($Name, $Method = null, $Type = '') {
        if (is_array($Name)) {
            if (!empty($Name)) {
                foreach ($Name as $k => $v) {
                    $this->Access($k, self::IsArrData($v, 0), self::IsArrData($v, 1));
                }
            }
            return $this;
        }
        self::$AccessData = $this->ProcessSetRoutingData($Name, $Method, $Type, self::$AccessData);
        return $this;
    }

    public function SetRouting($Method) {
        self::$SetRouting = array_merge(self::$SetRouting, (is_array($Method) ? $Method : [$Method]));
        return $this;
    }

    public function SetWorker($Name, $Data = []) {
        self::$SetWorker = array_merge(self::$SetWorker, (is_array($Name) ? $Name : [$Name => $Data]));
        return $this;
    }

    public static function SetClassFile($Name, $Data = null) {
        if (is_array($Name)) {
            if (!empty($Name)) {
                foreach ($Name as $k => $v) {
                    self::SetClassFile($k, $v);
                }
            }
            return;
        }
        self::$SetFileClass[trim(self::StrReplace($Name, '/', '\\'), '\\')] = self::DirName($Data);
    }

    public static function SetClassDir($Name, $Data = null) {
        if (is_array($Name)) {
            if (!empty($Name)) {
                foreach ($Name as $k => $v) {
                    self::SetClassDir($k, $v);
                }
            }
            return;
        }
        self::$SetDirClass[trim(self::StrReplace($Name, '/', '\\'), '\\')] = $Data;
    }

    private static $WorkerLoadDir;

    public static function setRootPath($root_path) {
        self::$WorkerLoadDir = $root_path;
    }

    public static function WorkerAutoloadFile($File) {
        if (empty(self::GetThis('IsLoad'))) {
            if (self::IsReqOnceFile(self::ConfigDir('PhpDir') . self::StrReplace($File, '\\', '/') . '.php')) {
                return true;
            } elseif (self::IsReqOnceFile(self::LoadDir() . 'Events/' . $File . '.php')) {
                return true;
            } elseif (self::LoadFileClass($File)) {
                return true;
            } elseif (self::LoadDirClass($File)) {
                return true;
            } else if (self::LoadWorkFile($File)) {
                return true;
            }
        }
        return false;
    }

    private static function LoadFileClass($File) {
        if (!empty($SetFileClass = self::$SetFileClass)) {
            if (!empty($PhpFile = self::IsArrData($SetFileClass, $File))) {
                if (self::IsReqOnceFile($PhpFile)) {
                    return true;
                }
            }
        }
        return false;
    }

    private static function LoadDirClass($File) {
        if (!empty($SetDirClass = self::$SetDirClass)) {
            $FileArr = explode('\\', $File);
            $OneName = $FileArr[key($FileArr)];
            if (!empty($Data = self::IsArrData($SetDirClass, $OneName))) {
                $File = self::Trim(substr($File, strlen($OneName))) . '.php';
                if (empty(is_array($Data))) {
                    if (self::IsReqOnceFile(self::RTrim(self::DirName($Data), true) . $File)) {
                        return true;
                    }
                    return false;
                } elseif (!empty($Data)) {
                    foreach ($Data as $v) {
                        if (self::IsReqOnceFile(self::RTrim(self::DirName($v), true) . $File)) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    public static function LoadWorkFile($File) {
        if (!empty($WorkerLoadDir = self::$WorkerLoadDir)) {
            if (self::IsReqOnceFile($WorkerLoadDir . '/' . $File . '.php')) {
                return true;
            }
        }
        return false;
    }

    private function ProcessLoad() {
        if (!empty($SortPrefix = self::GetConfig('SortPrefix'))) {
            $this->RunData['Sort'] = self::StrReplace(self::DelDir($this->RunData['Sort']), '/', '\\') . '\\' . $SortPrefix . self::BaseName($this->RunData['Sort']);
        } else {
            $this->RunData['Sort'] = self::StrReplace($this->RunData['Sort'], '/', '\\');
        }
        $this->RunData['AccessType'] = 'Php';
        $this->RunData['Method'] = self::GetConfig('MethodPrefix') . $this->RunData['Method'];
        $this->RunData['PhpFile'] = self::ConfigDir('PhpDir') . $this->RunData['Sort'] . '.php';
        if (empty(self::IsReqOnceFile($this->RunData['PhpFile']))) {
            if (empty($this->ProcessLoadView())) {
                self::ThrowNew('NotFile', $this->RunData['PhpFile']);
            }
            return;
        }
        $this->IsLoad = true;
        if (empty(is_callable([$this->RunData['Sort'], $this->RunData['Method']]))) {
            if (empty($this->ProcessLoadView())) {
                self::ThrowNew('ErrClass', $this->RunData['Sort'] . ':' . $this->RunData['Method']);
            }
            return;
        }
        $this->IsLoad = false;
        call_user_func_array([new $this->RunData['Sort'], $this->RunData['Method']], []);
    }

    private function ProcessLoadView() {
        if (!empty(self::GetConfig('IsView'))) {
            if ($this->RunData['Path'] == '/') {
                $ViewFile = self::SepFile(self::ConfigDir('ViewDir') . self::GetConfig('Sort') . '.' . self::GetConfig('ViewFormat'));
            } else {
                $ViewFile = self::SepFile(self::ConfigDir('ViewDir') . $this->RunData['Path'] . '.' . self::GetConfig('ViewFormat'));
            }
            if (is_file($ViewFile)) {
                $this->IsLoad = false;
                $this->RunData['AccessType'] = 'View';
                $this->RunData['ViewFile'] = $ViewFile;
                self::ReqFIle($ViewFile);
                return true;
            }
        }
        return false;
    }

    public static function SendFile($FilePlace, $FileName = false, $SendSize = null) {
        if (is_array($FilePlace)) {
            $Mime = self::IsArrData($FilePlace, 1);
            $FilePlace = self::IsArrData($FilePlace, 0);
        }
        $FilePlace = self::DirName($FilePlace);
        if (empty(is_file($FilePlace))) {
            self::ThrowNew('SendFile', $FilePlace);
        }
        if ($FileName === true) {
            $FileName = self::BaseName($FilePlace);
        }
        if (empty($Mime)) {
            $NewFileName = $FileName ? $FileName : $FilePlace;
            $FileFormat = self::PathInfo($NewFileName);
            $Mime = self::GetMime($FileFormat);
            if (empty($Mime)) {
                self::ThrowNew('ErrMime', $FileFormat);
            }
        }
        $Size = filesize($FilePlace);
        self::SetThis('HeadSend', [$FilePlace, $Size, $Mime, $FileName, true, $SendSize]);
    }

    public static function FileStream($FilePlace, $FileName = false, $SendSize = 8192) {
        self::SendFile($FilePlace, $FileName, $SendSize);
    }

    public static function SendData($FileData, $Format = null) {
        if (is_array($FileData)) {
            $Mime = self::IsArrData($FileData, 1);
            $FileData = self::IsArrData($FileData, 0);
        }
        $FileFormat = self::PathInfo($Format);
        if (empty($Mime)) {
            $NewFormat = $FileFormat ? $FileFormat : $Format;
            $Mime = self::GetMime($NewFormat);
            if (empty($Mime)) {
                self::ThrowNew('ErrMime', $NewFormat);
            }
        }
        $Size = strlen($FileData);
        $FileName = $FileFormat ? $Format : false;
        self::SetThis('HeadSend', [$FileData, $Size, $Mime, $FileName, false]);
    }

    public static function SetStatus($Data = 200) {
        self::SetThis('HeaderStatus', $Data);
    }

    public static function SetMime($Data) {
        $Mime = self::GetMime($Data);
        if (empty($Mime)) {
            self::ThrowNew('ErrMime', $Data);
        }
        self::SetThis('HeaderInfo', ['Content-Type' => $Mime], true);
    }

    public static function SetHead($Name, $Data = null) {
        if (empty(is_array($Name))) {
            self::SetThis('HeaderInfo', [$Name => $Data], true);
        } else if (!empty($Name)) {
            foreach ($Name as $k => $v) {
                self::SetThis('HeaderInfo', [$k => $v], true);
            }
        }
    }

    public static function HeaderModified($File) {
        if (!empty($if_modified_since = self::SelfFun('GetHeader', ['IF_MODIFIED_SINCE']))) {
            if ($if_modified_since === date('D, d M Y H:i:s', filemtime($File)) . ' ' . date_default_timezone_get()) {
                return true;
            }
        }
        return false;
    }

    private static function HeaderContent($ArrHead, $Data = null, $DataHead = []) {
        if (is_string($ArrHead) && !empty($Data)) {
            switch ($ArrHead) {
                case 'type':
                    $DataHead['Content-Type'] = $Data;
                    break;
                case 'last':
                    $DataHead['Last-Modified'] = gmdate('D, d M Y H:i:s', filemtime($Data)) . " " . date_default_timezone_get();
                    break;
                case 'conn':
                    $DataHead['Connection'] = $Data;
                    break;
                case 'range':
                    $DataHead['Content-Range'] = $Data;
                    break;
                case 'ranges':
                    $DataHead['Accept-Ranges'] = $Data;
                    break;
                case 'size':
                    $DataHead['Content-Length'] = $Data;
                    break;
                case 'file':
                    $DataHead['Content-Disposition'] = 'attachment;filename=' . $Data;
                    break;
            }
            return $DataHead;
        } else if (is_array($ArrHead) && !empty($ArrHead)) {
            foreach ($ArrHead as $k => $v) {
                $DataHead = array_merge($DataHead, self::HeaderContent($k, $v));
            }
        }
        return $DataHead;
    }

    public static function BigHeadInfo($File, $Mime, $Size, $NewFile, $Begin = 0) {
        $End = $Size - 1;
        $Status = 200;
        if (!empty($HttpRange = self::SelfFun('GetHeader', ['RANGE']))) {
            $Status = 206;
            $Range = explode("=", $HttpRange);
            list($Begin, $End) = explode("-", $Range[1]);
            if ($End == 0) {
                $End = $Size - 1;
            }
        }
        if ($Begin > $End || $Begin >= $Size || $End >= $Size) {
            $Is = true;
            $Status = 416;
            $HeaderData = self::HeaderContent(['range' => 'bytes ' . $Begin . '-' . $End . '/' . $Size, 'last' => $File, 'file' => $NewFile,]);
        } else {
            $Is = false;
            $HeaderData = self::HeaderContent(['conn' => 'keep-alive', 'size' => ($End - $Begin + 1), 'type' => $Mime, 'ranges' => '0-' . $Size, 'range' => 'bytes ' . $Begin . '-' . $End . '/' . $Size, 'last' => $File, 'file' => $NewFile,]);
        }
        return ['Is' => $Is, 'End' => $End, 'Begin' => $Begin, 'Status' => $Status, 'HeaderData' => $HeaderData];
    }

    public static function HeaderServer($Type = 200) {
        $header = [100 => "HTTP/1.1 100 Continue", 101 => "HTTP/1.1 101 Switching Protocols", 200 => "HTTP/1.1 200 OK", 201 => "HTTP/1.1 201 Created", 202 => "HTTP/1.1 202 Accepted", 203 => "HTTP/1.1 203 Non-Authoritative Information", 204 => "HTTP/1.1 204 No Content", 205 => "HTTP/1.1 205 Reset Content", 206 => "HTTP/1.1 206 Partial Content", 300 => "HTTP/1.1 300 Multiple Choices", 301 => "HTTP/1.1 301 Moved Permanently", 302 => "HTTP/1.1 302 Found", 303 => "HTTP/1.1 303 See Other", 304 => "HTTP/1.1 304 Not Modified", 305 => "HTTP/1.1 305 Use Proxy", 307 => "HTTP/1.1 307 Temporary Redirect", 400 => "HTTP/1.1 400 Bad Request", 401 => "HTTP/1.1 401 Unauthorized", 402 => "HTTP/1.1 402 Payment Required", 403 => "HTTP/1.1 403 Forbidden", 404 => "HTTP/1.1 404 Not Found", 405 => "HTTP/1.1 405 Method Not Allowed", 406 => "HTTP/1.1 406 Not Acceptable", 407 => "HTTP/1.1 407 Proxy Authentication Required", 408 => "HTTP/1.1 408 Request Time-out", 409 => "HTTP/1.1 409 Conflict", 410 => "HTTP/1.1 410 Gone", 411 => "HTTP/1.1 411 Length Required", 412 => "HTTP/1.1 412 Precondition Failed", 413 => "HTTP/1.1 413 Request Entity Too Large", 414 => "HTTP/1.1 414 Request-URI Too Large", 415 => "HTTP/1.1 415 Unsupported Media Type", 416 => "HTTP/1.1 416 Requested range not satisfiable", 417 => "HTTP/1.1 417 Expectation Failed", 500 => "HTTP/1.1 500 Internal Server Error", 501 => "HTTP/1.1 501 Not Implemented", 502 => "HTTP/1.1 502 Bad Gateway", 503 => "HTTP/1.1 503 Service Unavailable", 504 => "HTTP/1.1 504 Gateway Time-out"];
        return self::IsArrData($header, $Type, $header[200]);
    }

    private static function ProcessMime() {
        if (empty(self::$MimeData)) {
            $mime_file = self::LoadDir() . 'Events/Workerman/Protocols/Http/mime.types';
            $items = \file($mime_file, \FILE_IGNORE_NEW_LINES | \FILE_SKIP_EMPTY_LINES);
            if (!empty($items)) {
                foreach ($items as $content) {
                    if (\preg_match("/\s*(\S+)\s+(\S.+)/", $content, $match)) {
                        $mime_type = $match[1];
                        $extension_var = $match[2];
                        $extension_array = \explode(' ', \substr($extension_var, 0, -1));
                        foreach ($extension_array as $file_extension) {
                            self::$MimeData[$file_extension] = $mime_type;
                        }
                    }
                }
            }
            $MimeData = self::IsJson(file_get_contents(self::LoadDir() . 'Config/Mime.Types'), true);
            $ItemMime = self::GetConfig('ItemDir') . 'Config/Mime.Types';
            if (is_file($ItemMime)) {
                $MimeData = array_merge($MimeData, self::IsJson(file_get_contents($ItemMime), true));
            }
            if (!empty($MimeData)) {
                foreach ($MimeData as $k => $v) {
                    $VArr = explode(',', $v);
                    foreach ($VArr as $var) {
                        self::$MimeData[$var] = $k;
                    }
                }
            }
        }
    }

    private function HandleRouting() {
        if (empty(self::StrPos(self::GetConfig('Format'), $this->RunData['Format']))) {
            self::ThrowNew('UrlFormat', $this->RunData['Url']);
        }
        $this->HandleAlias();
        $this->HandleStop();
        $this->HandleAccess();
        $this->RoutingData = [];
    }

    private function HandleAlias() {
        if (!empty($AliasData = self::$AliasData)) {
            $Mode = $this->RunData['Mode'];
            $Path = $this->RunData['Path'];
            if (isset($AliasData['Back'][$Mode][$Path])) {
                $this->AliasSuccess($Path);
            } else if (self::$RunType == 'Cli') {
                if (isset($AliasData['Obj'][$Path])) {
                    $this->AliasSuccess($Path);
                }
            } else if (!empty($NoBack = self::IsArrData($AliasData, 'NoBack'))) {
                if (!empty($Data = self::IsArrData($NoBack, $Mode))) {
                    $UrlData = $this->CustomizeUrl($Mode);
                    $Path = self::IsArrData($UrlData, 'Path');
                    if (isset($Data[$Path])) {
                        $this->AliasSuccess($Path);
                    }
                }
            }
        }
    }

    private function AliasSuccess($Path) {
        $NewPath = self::$AliasData['Obj'][$Path];
        $PathArr = explode('/', $Path);
        $PathCount = Count($PathArr);
        $this->RunData['Path'] = $NewPath;
        $this->RunData['Url'] = $NewPath . substr($this->RunData['Url'], strlen($Path));
        $this->RunData['Sort'] = $NewPath;
        if ($PathCount > 1) {
            $this->RunData['Sort'] = self::DelDir($NewPath);
            $this->RunData['Method'] = self::BaseName($NewPath);
        }
        $this->RunData['Process'] = $Path . ' As ' . $NewPath;
        $this->RoutingData = $this->RunData;
    }

    private function HandleStop() {
        if (!empty($RoutingStop = self::$RoutingStop)) {
            $Mode = $this->RunData['Mode'];
            $Path = $this->RunData['Path'];
            if (isset($RoutingStop['Back'][$Mode])) {
                $this->StopSuccess($RoutingStop['Back'][$Mode], $Path);
            } else if (self::$RunType == 'Cli') {
                if (isset($RoutingStop['Obj'][$Path])) {
                    $this->StopSuccess($RoutingStop['Obj'], $Path);
                }
            } else if (!empty($NoBack = self::IsArrData($RoutingStop, 'NoBack'))) {
                if (!empty($Data = self::IsArrData($NoBack, $Mode))) {
                    $UrlData = $this->CustomizeUrl($Mode);
                    $Path = self::IsArrData($UrlData, 'Path');
                    $this->StopSuccess($Data, $Path);
                }
            }
        }
    }

    private function StopSuccess($Data, $Path) {
        if (!empty(self::IsArrData($Data, $Path))) {
            self::ThrowNew('StopDirClass', $Path);
        }
        $PathArr = explode('/', $Path);
        $PathCount = count($PathArr);
        foreach ($Data as $k => $v) {
            $KArr = explode('/', $k);
            $KCount = count($KArr);
            if ($PathCount >= $KCount && join('/', array_slice($PathArr, 0, $KCount)) == $k) {
                self::ThrowNew('StopDirClass', $k);
            }
        }
    }

    private function HandleAccess() {
        if (!empty($AccessData = self::$AccessData)) {
            $Mode = $this->RunData['Mode'];
            $Path = $this->RunData['Path'];
            if (isset($AccessData['Back'][$Mode][$Path])) {
                $this->AccessSuccess($Mode, $Path);
            } else if (self::$RunType == 'Cli') {
                if (isset($AccessData['Obj'][$Path])) {
                    $this->AccessSuccess($Mode, $Path);
                }
            } else if (!empty($NoBack = self::IsArrData($AccessData, 'NoBack'))) {
                if (!empty($Data = self::IsArrData($NoBack, $Mode))) {
                    $UrlData = $this->CustomizeUrl($Mode);
                    $Path = self::IsArrData($UrlData, 'Path');
                    if (isset($Data[$Path])) {
                        $this->AccessSuccess($Mode, $Path);
                    }
                }
            }
        }
    }

    private function AccessSuccess($Mode, $Path) {
        $this->RunData['AccessType'] = 'Access：' . $Mode;
        $Method = self::$AccessData['Obj'][$Path];
        $Method($this, !empty($this->RoutingData) ? $this->RoutingData : $this->RunData);
        self::Come();
    }

    private function CustomizeUrl($Mode) {
        if (!empty($RoutingData = $this->RoutingData)) {
            return $RoutingData;
        }
        $Mode = strtoupper($Mode);
        $URL = $this->ObtainUrl($Mode);
        $RoutingData = $this->DomainRouting($URL);
        $this->RoutingData = $UrlData = $this->UrlHandle($RoutingData, $Mode);
        return $UrlData;
    }

    public static function HandleShow($File, $Type, $FileArr = []) {
        if (empty(is_array($File))) {
            return self::ViewFile($File, $Type);
        } elseif (!empty($File)) {
            foreach ($File as $k => $v) {
                $FileArr[] = self::HandleShow($v, $Type);
            }
        }
        return $FileArr;
    }

    public static function HandleHtml($File, $Type, $FileArr = []) {
        if (empty(is_array($File))) {
            return self::HandleTemplateFile(self::SepFile(self::ConfigDir('ViewDir')), self::ViewFile($File, $Type), $Type);
        } elseif (!empty($File)) {
            foreach ($File as $k => $v) {
                $FileArr[] = self::HandleHtml($v, $Type);
            }
        }
        return $FileArr;
    }

    private static function HandleTemplateFile($ViewDir, $ViewFile, $Type) {
        $TemplateTime = self::GetConfig('TemplateTime');
        $TemplateFile = self::HtmlLogDir('TemplateDir') . substr($ViewFile, strlen($ViewDir));
        if (is_file($TemplateFile)) {
            if ($TemplateTime === true || filemtime($TemplateFile) >= time()) {
                return $TemplateFile;
            }
        }
        self::MkDir(self::DelDir($TemplateFile));
        $FileData = self::ProcessCourse($ViewFile, $ViewDir, $Type);
        file_put_contents($TemplateFile, $FileData);
        touch($TemplateFile, time() + (is_numeric($TemplateTime) ? $TemplateTime : 0));
        return $TemplateFile;
    }

    private static function ProcessCourse($ViewFile, $ViewDir, $Type) {
        $FileData = file_get_contents($ViewFile);
        $LeftSign = self::GetConfig('LeftSign');
        $RightSign = self::GetConfig('RightSign');
        $SignLeft = self::GetConfig('SignLeft', '@');
        $FileData = self::ReplaceTsg($FileData, $LeftSign . $SignLeft, $RightSign);
        $FileData = self::ReplaceTsg($FileData, $LeftSign, $RightSign);
        $FileData = self::ProcessScript($ViewFile, $ViewDir, $Type, $FileData, '\\' . $LeftSign, '\\' . $RightSign, '\\' . $SignLeft);
        $FileData = self::ProcessHTML($ViewFile, $ViewDir, $Type, $FileData, '\\' . $LeftSign, '\\' . $RightSign);
        return $FileData;
    }

    private static function ProcessScript($ViewFile, $ViewDir, $Type, $FileData, $LeftSign, $RightSign, $SignLeft) {
        $PregMatch = '@';
        $PregMatch .= '(<script(.*?)<\/script>)';
        $PregMatch .= '|(<style(.*?)<\/style>)';
        $PregMatch .= '|(javascript(.*?))';
        $PregMatch .= '|(' . $LeftSign . ':(.*?)' . $RightSign . ')';
        $PregMatch .= '|(' . $LeftSign . '"(.*?)' . $RightSign . ')';
        $PregMatch .= '|(' . $LeftSign . '\'(.*?)' . $RightSign . ')';
        $PregMatch .= '@is';
        preg_match_all($PregMatch, $FileData, $PregInfo);
        if (!empty($InfoData = array_filter($PregInfo[key($PregInfo)]))) {
            if (!empty($Arr = self::PregMatchAll(join("\r\n", $InfoData), $LeftSign . $SignLeft, $RightSign))) {
                $FileData = self::ProcessAll($FileData, $Arr, $ViewFile, $ViewDir, $Type);
            }
        }
        return $FileData;
    }

    private static function ProcessHTML($ViewFile, $ViewDir, $Type, $FileData, $LeftSign, $RightSign) {
        $LeachData = preg_replace(['@<script(.*?)</script>@is', '@<style(.*?)</style>@is', '@' . $LeftSign . ':(.*?)' . $RightSign . '@is', '@' . $LeftSign . '"(.*?)' . $RightSign . '@is', '@' . $LeftSign . '\'(.*?)' . $RightSign . '@is', '@javascript(.*?);@is',], "", $FileData);
        if (!empty($Arr = self::PregMatchAll($LeachData, $LeftSign, $RightSign))) {
            $FileData = self::ProcessAll($FileData, $Arr, $ViewFile, $ViewDir, $Type);
        }
        return $FileData;
    }

    private static function PregMatchAll($FileData, $LeftSign, $RightSign, $Data = []) {
        $PregMatch = '#';
        $PregMatch .= '(' . $LeftSign . 'file=(\"|\'|)(.*?)(\"|\'|)' . $RightSign . ')';
        $PregMatch .= '|(' . $LeftSign . 'foreach (.*?)' . $RightSign . ')';
        $PregMatch .= '|(' . $LeftSign . 'for (.*?)' . $RightSign . ')';
        $PregMatch .= '|(' . $LeftSign . 'elseif (.*?)' . $RightSign . ')';
        $PregMatch .= '|(' . $LeftSign . 'if (.*?)' . $RightSign . ')';
        $PregMatch .= '|(' . $LeftSign . '(.*?)' . $RightSign . ')';
        $PregMatch .= '#i';
        preg_match_all($PregMatch, $FileData, $PregInfo);
        if (!empty(array_filter($PregInfo[key($PregInfo)]))) {
            $Data = ['all' => array_filter(self::IsArrData($PregInfo, 0, [])), 'file' => array_filter(self::IsArrData($PregInfo, 3, [])), 'foreach' => array_filter(self::IsArrData($PregInfo, 6, [])), 'for' => array_filter(self::IsArrData($PregInfo, 8, [])), 'elseif' => array_filter(self::IsArrData($PregInfo, 10, [])), 'if' => array_filter(self::IsArrData($PregInfo, 12, [])), 'other' => array_filter(self::IsArrData($PregInfo, 14, [])),];
        }
        return $Data;
    }

    private static function ProcessAll($FileData, $PregInfo, $ViewFile, $ViewDir, $Type) {
        $all = $PregInfo['all'];
        $FileData = self::ProcessFile($PregInfo['file'], $all, $FileData, $ViewFile, $ViewDir, $Type);
        $FileData = self::ProcessForeach($PregInfo['foreach'], $all, $FileData);
        $FileData = self::ProcessFor($PregInfo['for'], $all, $FileData);
        $FileData = self::ProcessIfElseIf($PregInfo['elseif'], $all, $FileData, '}elseif');
        $FileData = self::ProcessIfElseIf($PregInfo['if'], $all, $FileData, 'if');
        $FileData = self::ProcessOther($PregInfo['other'], $all, $FileData);
        return $FileData;
    }

    private static function ProcessFile($Arr, $all, $FileData, $ViewFile, $ViewDir, $Type) {
        if (!empty($Arr)) {
            self::SetThis('ViewFileList', [$ViewFile], true);
            foreach ($Arr as $k => $v) {
                $v = trim($v);
                $PullFileName = !empty(self::PathInfo($v)) ? $v : $v . '.' . self::PathInfo($ViewFile);
                if (substr($v, 0, 1) == '/') {
                    $PullFile = self::SepFile($ViewDir . trim($PullFileName, '/'));
                } else {
                    $PullFile = self::SepFile(self::DirName($PullFileName, self::RTrim(self::DelDir($ViewFile), true)));
                }
                if (in_array($PullFile, self::GetThis('ViewFileList'))) {
                    $Info = "\r\n<?php //(" . $PullFile . ") Cannot be introduced repeatedly ?>\r\n";
                } else if (empty(is_file($PullFile))) {
                    $Info = "\r\n<?php //(" . $PullFile . ") File does not exist ?>\r\n";
                } else {
                    $Info = "\r\n<?php //(" . $PullFile . ") Begin?>\r\n" . self::ProcessCourse($PullFile, $ViewDir, $Type) . "\r\n<?php //(" . $PullFile . ") End?>\r\n";
                }
                $FileData = str_replace($all[$k], $Info, $FileData);
            }
        }
        return $FileData;
    }

    private static function ProcessForeach($Arr, $all, $FileData) {
        if (!empty($Arr)) {
            foreach ($Arr as $k => $v) {
                $v = trim($v);
                $VArr = explode('=', self::BlankSystem($v));
                $Info = '<?php foreach(';
                $Info .= self::HandleVariable(self::IsArrData($VArr, 0), true) . ' as ';
                $Info .= self::HandleVariable(self::IsArrData($VArr, 1), true, true);
                if (!empty($Fv = self::IsArrData($VArr, 2))) {
                    $Info .= '=>' . self::HandleVariable($Fv, true, true);
                }
                $Info .= '){?>';
                $FileData = str_replace($all[$k], $Info, $FileData);
            }
        }
        return $FileData;
    }

    private static function ProcessFor($Arr, $all, $FileData) {
        if (!empty($Arr)) {
            foreach ($Arr as $k => $v) {
                $v = trim($v);
                $Info = '<?php for(';
                $Info .= self::HandleVariable($v, false, true);
                $Info .= '){?>';
                $FileData = str_replace($all[$k], $Info, $FileData);
            }
        }
        return $FileData;
    }

    private static function ProcessIfElseIf($Arr, $all, $FileData, $Type) {
        if (!empty($Arr)) {
            foreach ($Arr as $k => $v) {
                $v = trim($v);
                $Info = '<?php ' . $Type;
                if (substr($v, 0, 1) != '(') {
                    $Info .= '(';
                }
                $Info .= self::HandleVariable($v);
                if (substr($v, -1) != ')') {
                    $Info .= ')';
                }
                $Info .= '{?>';
                $FileData = str_replace($all[$k], $Info, $FileData);
            }
        }
        return $FileData;
    }

    private static function ProcessOther($Arr, $all, $FileData) {
        if (!empty($Arr)) {
            foreach ($Arr as $k => $v) {
                $v = trim($v);
                $One = substr($v, 0, 1);
                $Two = substr($v, 0, 2);
                if ($One == '.' || substr_count($v, '=') > 0) {
                    if ($One == '.') {
                        $v = substr($v, 1);
                    }
                    $VTwo = substr($v, 0, 2);
                    $Attribute = explode('=', $v);
                    $AttOne = self::IsArrData($Attribute, 0);
                    $AttTwo = self::IsArrData($Attribute, 1);
                    if (empty($AttTwo) || substr($v, 0, 1) != '$') {
                        $Info = '<?php ' . self::HandleVariable($v) . ';?>';
                    } else if ($VTwo == '$.' || $VTwo == '$$') {
                        $Info = '<?php ' . self::HandleVariable($AttOne) . '=' . self::HandleVariable($AttTwo) . ';?>';
                    } else {
                        $Info = '<?php $this->assign(\'' . trim($AttOne, '$') . '\',' . self::HandleVariable($AttTwo) . ',true);?>';
                    }
                } elseif ($One != '/') {
                    $Info = '<?php echo ' . self::HandleVariable($v) . ';?>';
                } else if ($Two == '/*') {
                    $Info = '<?php /** ' . substr($v, 2, (strlen($v) - 4)) . '*/?>';
                } else if ($Two == '//') {
                    $Info = '<?php //' . substr($v, 2) . '?>';
                } else {
                    $Info = '<?php /*' . $v . ';Not supported*/?>';
                }
                $FileData = str_replace($all[$k], $Info, $FileData);
            }
        }
        return $FileData;
    }

    private static function HandleVariable($Data, $Type = false, $Mode = false) {
        if ($Type && substr($Data, 0, 1) != '$') {
            $Data = '$' . $Data;
        }
        $Data = str_replace('$$', '$.this->', $Data);
        preg_match_all('/(\$(\w[a-zA-Z0-9.]*)|\$\.(\w[a-zA-Z0-9.]*))/', $Data, $ArrInfo);
        if (!empty($InfoData = self::IsArrData($ArrInfo, 0))) {
            $InfoData = self::ArrLenSort($InfoData, true);
            foreach ($InfoData as $k => $v) {
                $v = trim($v);
                if (substr($v, 0, 2) == '$.') {
                    $Info = '$' . trim(substr($v, 2));
                } elseif (substr($v, 0, 1) == '$' || $Type) {
                    $Info = $Mode ? self::TplDataReplace(trim($v, '$')) : '$this->obtain(\'' . trim($v, '$') . '\')';
                } else {
                    $Info = $Data;
                }
                $Data = str_replace($v, $Info, $Data);
            }
        }
        return $Data;
    }

    private static function TplDataReplace($Data) {
        $Data = trim($Data);
        if (!empty($DataArr = explode('.', $Data))) {
            $Code = '';
            foreach ($DataArr as $k => $v) {
                $Code .= '[\'' . trim($v) . '\']';
            }
            return '$this->TplData' . $Code;
        }
        return $Data;
    }

    private static function ReplaceTsg($FileData, $LeftSign, $RightSign) {
        return str_replace([$LeftSign . '/foreach' . $RightSign, $LeftSign . 'foreach' . $RightSign, $LeftSign . '/for' . $RightSign, $LeftSign . 'for' . $RightSign, $LeftSign . '/if' . $RightSign, $LeftSign . 'if' . $RightSign], '<?php }?>', str_replace([$LeftSign . '/else' . $RightSign, $LeftSign . 'else' . $RightSign], '<?php }else{?>', $FileData));
    }

    private static function BlankSystem($Data) {
        return str_replace(' ', '=', self::MergeSpaces(str_replace('=', ' ', str_replace('=>', ' ', str_replace('as', '', $Data)))));
    }

    private static function DomainPos($List, $Str) {
        if (self::StrPos($List, $Str)) {
            return $Str;
        } else if (strpos($List, '*.') !== false) {
            $Arr = (is_array($List) ? $List : explode(',', $List));
            foreach ($Arr as $V) {
                if (substr($V, 0, 1) == '*') {
                    $StrArr = explode('.', $Str);
                    $StrCount = count($StrArr);
                    $NewArr = ($StrCount >= 3) ? array_slice($StrArr, 1) : $StrArr;
                    $Str = join('.', $NewArr);
                    if ($Str == substr($V, 2)) {
                        return $V;
                    }
                } else if ($V == $Str) {
                    return $V;
                }
            }
        }
        return false;
    }

    private static function DirName($File, $Dir = null) {
        if (substr($File, 0, 1) == '@') {
            $File = self::LoadDir() . self::Trim(trim($File, '@'));
        } else if (substr($File, 0, 1) == '#') {
            $File = self::StartDir() . self::Trim(trim($File, '#'));
        } else if (substr($File, 0, 1) == '~') {
            $File = self::GetConfig('ItemDir') . self::Trim(trim($File, '~'));
        } else if (!empty($Dir)) {
            $File = $Dir . self::Trim(trim($File));
        }
        return $File;
    }

    private static function RetFile($File) {
        return require $File;
    }

    private static function ReqFile($File) {
        require $File;
    }

    private static function Separator() {
        return DIRECTORY_SEPARATOR;
    }

    private static function Trim($Str, $LTrim = false, $RTrim = false) {
        return self::RTrim(self::LTrim($Str, $LTrim), $RTrim);
    }

    private static function LTrim($Str, $Type = false) {
        if (empty($Str)) {
            return $Str;
        }
        $End = substr($Str, 0, 1);
        if ($End == '/' || $End == '\\') {
            $Str = substr($Str, 1);
        }
        return ($Type ? self::Separator() : '') . $Str;
    }

    private static function RTrim($Str, $Type = false) {
        if (empty($Str)) {
            return $Str;
        }
        $End = substr($Str, -1);
        if ($End == '/' || $End == '\\') {
            $Str = substr($Str, 0, strlen($Str) - 1);
        }
        return $Str . ($Type ? self::Separator() : '');
    }

    private static function IsFile($File) {
        if (is_file($File)) {
            return self::SepFile($File);
        }
        return false;
    }

    private static function SepFile($File) {
        return self::StrReplace(self::StrReplace($File, '\\', '/'), '/', self::Separator());
    }

    private static function IsReqOnceFile($File) {
        $File = self::SepFile($File);
        if (is_file($File)) {
            require_once $File;
            return true;
        }
        return false;
    }

    private static function SignSort($a, $b) {
        $a = strval($a);
        $b = strval($b);
        $len_a = strlen($a);
        $len_b = strlen($b);
        $sort_arr = ['$$', '$', '$.'];
        $tmp_key_a = mb_substr($a, 0, 2);
        $tmp_key_a = preg_replace('/[a-zA-Z]/', '', $tmp_key_a);
        $tmp_key_b = mb_substr($b, 0, 2);
        $tmp_key_b = preg_replace('/[a-zA-Z]/', '', $tmp_key_b);
        $key_a = array_search($tmp_key_a, $sort_arr);
        $key_b = array_search($tmp_key_b, $sort_arr);
        $res = $key_a <=> $key_b;
        if ($res != 0) {
            return $res;
        }
        $res = $len_a <=> $len_b;
        if ($res == 0) {
            return $a > $b ? 1 : -1;
        }
        return $res;
    }

    public static function IsTwoArr($Data, $Key = null) {
        if (!empty($Data)) {
            if (empty(is_array($Data[($Key ? $Key : key($Data))]))) {
                $Data = [$Data];
            }
        }
        return $Data;
    }

    public static function IsArrData($Data, $Key = null, $Default = null) {
        if (!isset($Key)) {
            return $Data;
        }
        return isset($Data[$Key]) ? $Data[$Key] : $Default;
    }

    public static function BaseName($Str, $Format = null) {
        return basename(rtrim($Str, '/'), $Format);
    }

    public static function DelDir($Str, $Iv = 1) {
        if ($Iv == 1) {
            $Str = dirname(rtrim($Str, '/'));
        } else if ($Iv > 1) {
            for ($i = 0; $i < $Iv; ++$i) {
                $Str = dirname(rtrim($Str, '/'));
            }
        }
        return (($Str != '.') ? $Str : '');
    }

    public static function MkDir($Dir) {
        return (empty(is_dir($Dir)) ? mkdir($Dir, 0777, true) : true);
    }

    public static function PathInfo($Str) {
        return pathinfo($Str, PATHINFO_EXTENSION);
    }

    public static function DelPath($Str, $Format = null) {
        $Format = !isset($Format) ? self::PathInfo($Str) : $Format;
        return trim(($Format ? substr($Str, 0, (strlen($Str) - strlen($Format) - 1)) : $Str), '.');
    }

    public static function StrReplace($Str, $Old, $New) {
        return strtr($Str, $Old, $New);
    }

    public static function MergeSpaces($Str) {
        return preg_replace("/\s(?=\s)/", "\\1", $Str);
    }

    public static function StrPos($Str, $k) {
        return ((strpos(',' . $Str . ',', ',' . $k . ',') !== false) ? true : false);
    }

    public static function SignData($Str, $One = '(', $Two = ')') {
        $OnePos = stripos($Str, $One);
        $TwoPos = stripos($Str, $Two);
        if (($OnePos === false || $TwoPos === false) || $OnePos >= $TwoPos) {
            return false;
        }
        $Str = substr($Str, ($OnePos + 1), ($TwoPos - $OnePos - 1));
        return $Str;
    }

    public static function IsCli() {
        return preg_match("/cli/i", php_sapi_name()) ? true : false;
    }

    public static function StartDir() {
        return getcwd() . self::Separator();
    }

    public static function StrArr($Name, $Data) {
        $Data = is_array($Data) ? json_encode($Data) : (is_string($Data) ? '"' . $Data . '"' : $Data);
        $Json = '{"' . str_replace('.', '":{"', $Name) . '":' . $Data . str_repeat('}', count(explode('.', $Name)));
        return self::IsJson($Json);
    }

    public static function ObtainArr($Data, $Name, $Default = null) {
        $NameArr = explode('.', $Name);
        if (count($NameArr) > 1) {
            foreach ($NameArr as $k => $v) {
                $Data = isset($Data[$v]) ? $Data[$v] : $Default;
            }
            return $Data;
        }
        return $Default;
    }

    public static function IsJson($Data, $Type = true) {
        $Data = json_decode($Data, $Type);
        return (($Data && is_object($Data)) || (is_array($Data) && $Data)) ? $Data : [];
    }

    public static function JsonFormat($Data, $Type = false) {
        array_walk_recursive($Data, function (&$val) {
            if ($val !== true && $val !== false && !is_numeric($val)) $val = urlencode($val);
        });
        $Data = (empty($Type)) ? json_encode($Data, JSON_PRETTY_PRINT) : json_encode($Data, JSON_PRETTY_PRINT | JSON_NUMERIC_CHECK);
        $Data = urldecode($Data);
        return $Data;
    }

    public static function dumpJson($Data, $Type = true) {
        if (empty(is_array($Data))) {
            $Data = self::IsJson($Data);
        }
        $Content = JsonFormat($Data);
        if (self::$RunType != 'Cli') {
            $Content = highlight_string($Content, true);
        } else {
            $Content = "\r\n" . $Content . "\r\n";
        }
        if (empty($Type)) {
            return $Content;
        }
        echo $Content;
    }

    public static function dump($Data, $Type = true) {
        $Content = preg_replace('/\]\=\>\n(\s+)/m', '] => ', self::ObStart(function () use ($Data) {
            var_dump($Data);
        }));;
        if (self::$RunType != 'Cli') {
            $Content = highlight_string($Content, true);
        } else {
            $Content = "\r\n" . $Content . "\r\n";
        }
        if (empty($Type)) {
            return $Content;
        }
        echo $Content;
    }

    public static function ps($Data, $Type = true) {
        $Content = '<pre>' . print_r($Data, true) . '</pre>';
        if (empty($Type)) {
            return $Content;
        }
        echo $Content;
    }

    public static function ts($Data = null, $row = 30, $cols = 200) {
        echo '<textarea rows="' . $row . '" cols="' . $cols . '">' . $Data . '</textarea>';
    }

    public static function ObStart($Method) {
        ob_start();
        if (is_callable($Method)) {
            $Method();
        } else {
            echo $Method;
        }
        $Content = ob_get_contents();
        ob_end_clean();
        return $Content;
    }

    public static function ArrLenSort($Data, $Type = false) {
        usort($Data, function ($a, $b) use ($Type) {
            return ($Type ? strlen($a) - strlen($b) : strlen($b) - strlen($a));
        });
        return $Data;
    }

    private function RunAll($Obj = null, $Req = null, $Res = null) {
        global $LoadingObjects;
        $LoadingObjects = new self();
        $LoadingObjects->RunObj = $Obj;
        $LoadingObjects->RunReq = $Req;
        $LoadingObjects->RunRes = $Res;
        $LoadingObjects->StartTime = $this->StartTime;
        $LoadingObjects->FunName('RunAll');
    }

    private function FunName($Name, $Parametric = []) {
        return call_user_func_array(is_array($Name) ? $Name : self::MethodName($this, $Name), $Parametric);
    }

    private static function ThisFun($Name, $Parametric = []) {
        return call_user_func_array((is_array($Name) ? $Name : self::MethodName(self::ThisObj(), $Name)), $Parametric);
    }

    private function ProcessSetRoutingData($Name, $Method, $Type, $SetData) {
        $Name = trim(self::StrReplace($Name, '\\', '/'), '/');
        $Name = !empty($Name) ? $Name : '/';
        $Data['Obj'][$Name] = $Method;
        $Routing = self::GetConfig('Routing');
        $TypeArr = explode(',', strtoupper(!empty($Type) ? $Type : $Routing));
        foreach ($TypeArr as $v) {
            if (self::StrPos($Routing, $v)) {
                $Data['Back'][$v][$Name] = true;
            } else {
                $Data['NoBack'][$v][$Name] = false;
            }
        }
        return array_merge_recursive($SetData, $Data);
    }

    private function GetViewFile() {
        return self::SepFile(self::ConfigDir('ViewDir') . $this->RunData['Sort'] . self::Separator() . $this->RunData['Method'] . '.' . self::GetConfig('ViewFormat'));
    }

    private function HtmlFile() {
        return self::SepFile(self::HtmlLogDir('HtmlDir') . $this->RunData['Sort'] . self::Separator() . $this->RunData['Method'] . self::Separator() . substr(md5($this->RunData['Url'] . join('', $this->RunData['Get'])), 8, 16) . '.html');
    }

    private static function ConfigDir($Name, $Dir = null) {
        $Dir = $Dir ? $Dir : self::GetConfig('ItemDir');
        if (!empty($NameDir = self::GetConfig($Name))) {
            $Dir = $Dir . self::Trim($NameDir, false, true);
        }
        return $Dir;
    }

    private static function HtmlLogDir($Name) {
        return self::ConfigDir($Name, self::ConfigDir('TempDir'));
    }

    private static function ThisObj() {
        global $LoadingObjects;
        return $LoadingObjects;
    }

    private static function SetThis($Key, $Data, $Type = null) {
        if (!empty($Obj = self::ThisObj())) {
            $LoadData = $Obj->$Key;
            if (!empty($Type) && is_array($LoadData) && is_array($Data)) {
                $Data = array_merge($LoadData, (is_array($Data) ? $Data : [$Data]));
            }
            $Obj->$Key = $Data;
        }
    }

    private static function GetThis($Name) {
        if (!empty($Obj = self::ThisObj())) {
            return $Obj->$Name;
        }
        return false;
    }

    private static function MethodName($Sort, $Name) {
        $Method = self::$RunType . $Name;
        if (self::$RunType == 'Cli') {
            if (empty(method_exists($Sort, $Method))) {
                $Method = 'Ani' . $Name;
            }
        }
        return [$Sort, $Method];
    }

    private function CensorHtmlTime() {
        $HtmlFile = $this->HtmlFile();
        $HtmlTime = self::GetConfig('HtmlTime');
        if (is_file($HtmlFile) && $HtmlTime) {
            if ($HtmlTime === true || filemtime($HtmlFile) >= time()) {
                $this->RunData['AccessType'] = 'Html';
                self::ReqFile($HtmlFile);
                return true;
            }
            unlink($HtmlFile);
            return false;
        }
    }

    private function SaveHtml($Content) {
        $HtmlFile = $this->HtmlFile();
        $HtmlTime = self::GetConfig('HtmlTime');
        if ($this->IsHtml && $HtmlTime) {
            self::MkDir(self::DelDir($HtmlFile));
            file_put_contents($HtmlFile, $Content);
            touch($HtmlFile, time() + (($HtmlTime === true) ? 1 : $HtmlTime));
        }
    }

    private static function DelUrlHomeFile($Path, $Type = false) {
        if (!empty($Path) && !empty($HomeFile = self::IsArrData($_SERVER, 'SCRIPT_FILENAME'))) {
            if ($Type) {
                $Path = self::PathRoot($Path);
            }
            $HomeFile = self::BaseName($HomeFile);
            $HomeLen = strlen($HomeFile);
            if (strlen($Path) >= $HomeLen) {
                if ($HomeFile == substr($Path, 0, $HomeLen)) {
                    $Path = ltrim(substr($Path, strlen($HomeFile)), '/');
                }
            }
        }
        return $Path;
    }

    private static function PathRoot($Path) {
        if (!empty($Root = self::IsArrData($_SERVER, 'DOCUMENT_ROOT'))) {
            $GetCwd = rtrim(strtr(getcwd(), '\\', '/'), '/');
            $Root = rtrim(strtr($Root, '\\', '/'), '/');
            if ($GetCwd != $Root) {
                $NewRoot = trim(substr($GetCwd, strlen($Root)), '/');
                if (substr($Path, 0, strlen($NewRoot)) == $NewRoot) {
                    $Path = ltrim(substr($Path, strlen($NewRoot)), '/');
                }
            }
        }
        return $Path;
    }

    public function __destruct() {
        global $LoadingObjects, $argv;
        unset($LoadingObjects, $argv);
    }

    public static function GetReferer($Type = false) {
        $referer = self::SelfFun('GetHeader', ['referer']);
        if (!empty($Type) && !empty($referer)) {
            $RArr = explode('://', $referer);
            $ReArr = explode('/', $RArr[1]);
            $referer = $ReArr[key($ReArr)];
        }
        return $referer;
    }

    public static function ViewFile($File = null, $Type = false) {
        if ($File === true) {
            $File = null;
            $Type = true;
        } else if ($File === false) {
            $File = null;
            $Type = false;
        }
        self::SetThis('IsHtml', $Type);
        $ViewFile = self::ThisFun([self::ThisObj(), 'GetViewFile']);
        if ($File) {
            $File = self::StrReplace($File, '\\', '/');
            $FileFormat = self::PathInfo($File) ? self::Trim($File) : self::Trim($File) . '.' . self::PathInfo($ViewFile);
            if (substr($File, 0, 1) == '/') {
                $ViewFile = self::ConfigDir('ViewDir') . $FileFormat;
            } else {
                $ViewFile = self::DirName($FileFormat, self::RTrim(self::DelDir($ViewFile), true));
            }
        }
        $ViewFile = self::SepFile($ViewFile);
        if (is_file($ViewFile)) {
            return $ViewFile;
        }
        self::ThrowNew('NotViewFile', $ViewFile);
    }

    public static function GetWeb($Key = null, $Default = null) {
        $Data = self::GetRunData('Get', []);
        if (!isset($Key)) {
            return $Data;
        }
        return isset($Data[$Key]) ? urldecode($Data[$Key]) : $Default;
    }

    public static function GetConfig($Key = null, $Default = null) {
        return self::IsArrData(self::$ItemConfig, $Key, $Default);
    }

    public static function GetRunData($Key = null, $Default = null) {
        return self::IsArrData(self::GetThis('RunData'), $Key, $Default);
    }

    public static function RoutingData($Default = null) {
        return self::GetRunData('Data', $Default);
    }

    public static function GetMime($Key = null, $Default = null) {
        self::ProcessMime();
        return self::IsArrData(self::$MimeData, $Key, $Default);
    }

    public static function SelfFun($Name, $Parametric = []) {
        return call_user_func_array((is_array($Name) ? $Name : self::MethodName(__CLASS__, $Name)), $Parametric);
    }

    public static function GetStatic($Name = null, $Default = null) {
        $Self = new \ReflectionClass(new self());
        $Data = $Self->getStaticProperties();
        if (!isset($Name)) {
            return $Data;
        }
        return self::IsArrData($Data, $Name, $Default);
    }

    private function SwooleStart() {
        echo "Swoole启动;开发中.....\r\n";
    }

    private static function WorkerSetCookie($Key, $Value = null, $MaxAge = 0, $Path = '', $Domain = '', $Secure = false, $Only = false) {
        if (empty(is_array($Key))) {
            self::WorkerRunRes('cookie', [$Key, $Value, $MaxAge, $Path, $Domain, $Domain, $Secure, $Secure, $Only]);
        } else if (!empty($Key)) {
            foreach ($Key as $k => $v) {
                call_user_func_array('self::WorkerSetCookie', $v);
            }
        }
    }

    private static function WorkerGetCookie($Key = null, $Default = null) {
        return self::IsArrData(self::WorkerRunReq('cookie'), $Key, $Default);
    }

    private static function WorkerDeleteCookie($Key = null) {
        if (!isset($Key)) {
            if (!empty($Cookie = self::WorkerGetCookie())) {
                foreach ($Cookie as $k => $v) {
                    self::WorkerSetCookie($k, null, -10);
                }
            }
        } elseif (empty(is_array($Key))) {
            self::WorkerSetCookie($Key, null, -10);
        } else if (!empty($Key)) {
            foreach ($Key as $v) {
                self::WorkerSetCookie($v, null, -10);
            }
        }
    }

    private static function WorkerPullCookie($Key, $Default = null) {
        $Data = self::WorkerGetCookie($Key, $Default);
        self::WorkerSetCookie($Key, null, -10);
        return !empty($Data) ? $Data : $Default;
    }

    private static function WorkerHasCookie($Key) {
        $Cookie = self::WorkerRunReq('cookie');
        return isset($Cookie[$Key]) ? true : false;
    }

    private function WorkerStart() {
        $worker = new \Workerman\Worker(self::$Listen, self::$Context);
        \Workerman\Worker::$logFile = self::StartDir() . 'Worker.log';
        $SetWork = self::$SetWorker;
        if (empty(isset($SetWork['count']))) {
            $SetWork['count'] = 4;
        }
        if (empty(isset($SetWork['name']))) {
            $SetWork['name'] = 'ZqHttp';
        }
        if (!empty(self::$Context) && empty(isset($SetWork['transport']))) {
            $SetWork['name'] = 'ssl';
        }
        $SetWork['onMessage'] = function ($Obj, $Req) {
            $this->StartTime = microtime(true);
            $Res = new \Workerman\Protocols\Http\Response();
            $this->RunAll($Obj, $Req, $Res);
        };
        foreach ($SetWork as $k => $v) {
            $worker->$k = $v;
        }
        if (!defined('GLOBAL_START')) {
            \Workerman\Worker::runAll();
        }
    }

    private function WorkerRunAll() {
        $this->SaveUrl();
        $Content = self::ObStart(function () {
            $Data = self::TryCatch(function () {
                if (empty($this->WorkerStaticFile())) {
                    if ($this->CensorHtmlTime()) return;
                    $this->HandleRouting();
                    $this->ProcessLoad();
                }
            });
            $this->ErrInfo($Data);
        });
        if (empty($this->HeadSend)) {
            $this->SaveHtml($Content);
            if (!empty($this->HeaderInfo) || !empty($this->HeaderStatus)) {
                self::WorkerHeader();
            }
            self::WorkerSend(self::WorkerRunRes('withBody', [$Content]));
        } else {
            self::SelfFun(['self', 'WorkerSendFileData'], $this->HeadSend);
        }
    }

    private static function WorkerRunObj($Name, $Data = []) {
        return self::SelfFun([self::GetThis('RunObj'), $Name], $Data);
    }

    private static function WorkerRunReq($Name, $Data = []) {
        return self::SelfFun([self::GetThis('RunReq'), $Name], $Data);
    }

    private static function WorkerRunRes($Name, $Data = []) {
        return self::SelfFun([self::GetThis('RunRes'), $Name], $Data);
    }

    private static function WorkerGetUrl() {
        $Path = self::DelUrlHomeFile(ltrim(self::WorkerGetPath(), '/'));
        return !empty($Path) ? $Path : '/';
    }

    public static function LinuxStartAll($DirFile) {
        ini_set('display_errors', 'on');
        if (strpos(strtolower(PHP_OS), 'win') === 0) {
            exit("start.php not support windows, please use start_for_win.bat\n");
        } else if (!extension_loaded('pcntl')) {
            exit("Please install pcntl extension. See http://doc3.workerman.net/appendices/install-extension.html\n");
        } else if (!extension_loaded('posix')) {
            exit("Please install posix extension. See http://doc3.workerman.net/appendices/install-extension.html\n");
        }
        if (!empty($DirFile = (is_array($DirFile) ? $DirFile : glob($DirFile)))) {
            define('GLOBAL_START', 1);
            self::OpenLoad();
            foreach ($DirFile as $start_file) {
                self::ReqFile($start_file);
            }
            \Workerman\Worker::runAll();
        }
    }

    private static function WorkerGetIp() {
        return self::WorkerRunObj('getRemoteIp');
    }

    public static function WorkerGetHost($Type = false) {
        return self::WorkerRunReq('host', [$Type]);
    }

    private static function WorkerGet($Key = null, $Default = null) {
        return self::WorkerRunReq('get', [$Key, $Default]);
    }

    private static function WorkerPost($Key = null, $Default = null) {
        return self::WorkerRunReq('post', [$Key, $Default]);
    }

    private static function WorkerGetHeader($Key = null, $Default = null) {
        return self::WorkerRunReq('header', [($Key ? strtolower(self::StrReplace($Key, '_', '-')) : $Key), $Default]);
    }

    private static function WorkerGetUri() {
        return self::WorkerRunReq('uri');
    }

    private static function WorkerGetPath() {
        return self::WorkerRunReq('path');
    }

    private static function WorkerGetQueryString() {
        return self::WorkerRunReq('queryString');
    }

    private static function WorkerGetMethod() {
        return self::WorkerRunReq('method');
    }

    private static function WorkerGetInput() {
        return self::WorkerRunReq('rawBody');
    }

    private static function WorkerGetFile($Key = null, $Default = null) {
        return self::IsArrData(self::WorkerRunReq('file'), $Key, $Default);
    }

    private static function WorkerGetLocalIp() {
        return self::WorkerRunObj('getLocalIp');
    }

    private static function WorkerGetScheme() {
        return !empty(self::$Context) ? 'https' : 'http';
    }

    private static function WorkerGetProtocolVersion() {
        return self::WorkerRunReq('protocolVersion');
    }

    private static function WorkerSendFileData($FileData, $Size, $Mime, $NewFile, $Type, $SendSize = 0) {
        if ($Type) {
            if (self::HeaderModified($FileData)) {
                self::WorkerSend(self::WorkerRunRes('withStatus', [304]));
                return;
            }
            if (is_numeric($SendSize)) {
                self::WorkerHeader(self::HeaderContent(['type' => $Mime, 'file' => $NewFile]));
                if (preg_match('/bytes=(\d*)-(\d*)/i', self::SelfFun('GetHeader', ['range']), $match)) {
                    $offset_start = (int)$match[1];
                    $offset_end = $match[2];
                    $length = $offset_end === '' ? 0 : $offset_end - $offset_start + 1;
                    $Res = self::WorkerRunRes('withFile', [$FileData, $offset_start, $length]);
                } else {
                    $Res = self::WorkerRunRes('withFile', [$FileData]);
                }
                self::WorkerSend($Res);
                return;
            }
            self::WorkerHeader(self::HeaderContent(['size' => $Size, 'type' => $Mime, 'file' => $NewFile, 'last' => $FileData]));
            self::WorkerSend(self::WorkerRunRes('withFile', [$FileData]));
            return;
        }
        self::WorkerHeader(self::HeaderContent(['size' => $Size, 'type' => $Mime, 'file' => $NewFile]));
        self::WorkerSend(self::WorkerRunRes('withBody', [$FileData]));
    }

    private static function WorkerHeader($Status = null, $Header = []) {
        $Status = $Status ? $Status : self::GetThis('HeaderStatus');
        if (is_numeric($Status)) {
            self::WorkerRunRes('withStatus', [$Status]);
        } elseif (!empty($Status) && is_array($Status)) {
            $Header = $Status;
        }
        self::WorkerRunRes('withHeaders', [array_merge($Header, self::GetThis('HeaderInfo'))]);
    }

    private function WorkerStaticFile() {
        $StaticDir = self::DirName(self::GetConfig('StaticDir'));
        $Url = trim($this->RunData['Url'], '/');
        $StaticFormat = self::PathInfo($Url);
        if ($StaticFormat) {
            if (self::StrPos(strtolower(join(',', array_keys(self::GetMime()))), strtolower($StaticFormat))) {
                $StaticFile = $StaticDir . $Url;
                if (is_file($StaticFile)) {
                    $Type = self::StrPos(strtolower(trim(self::GetConfig('DownFormat'), ',')), strtolower($StaticFormat));
                    if (self::StrPos(strtolower(trim(self::GetConfig('StreamFormat'), ',')), strtolower($StaticFormat))) {
                        self::FileStream($StaticFile, $Type);
                    } else {
                        self::SendFile($StaticFile, $Type);
                    }
                    return true;
                }
                if (strtolower($Url) == 'favicon.ico') {
                    self::WorkerSend(self::WorkerRunRes('withBody', [self::ObStart(function () {
                        self::ErrHtml();
                    })]));
                    return true;
                }
            }
        }
        return false;
    }

    private static function WorkerSend($Data) {
        self::SelfFun([self::GetThis('RunObj'), 'send'], [$Data]);
    }

    private static function WorkerSession($Name, $Data = []) {
        return self::SelfFun([self::WorkerRunReq('session'), $Name], $Data);
    }

    private static function WorkerSetSession($Key, $Value) {
        if (empty(is_array($Key))) {
            self::WorkerSession('set', [$Key, $Value]);
        } else if (!empty($Key)) {
            self::WorkerSession('put', [$Key]);
        }
    }

    private static function WorkerGetSession($Key = null, $Default = null) {
        return self::IsArrData(self::WorkerSession('all'), $Key, $Default);
    }

    private static function WorkerDeleteSession($Key = null) {
        if (!isset($Key)) {
            self::WorkerSession('flush');
        } else {
            self::WorkerSession('forget', [$Key]);
        }
    }

    private static function WorkerPullSession($Key, $Default = null) {
        $Data = self::WorkerSession('pull', [$Key]);
        return !empty($Data) ? $Data : $Default;
    }

    private static function WorkerHasSession($Key) {
        $Session = self::WorkerSession('all');
        return isset($Session[$Key]) ? true : false;
    }

    private static function WorkerSessionId() {
        return self::WorkerRunReq('sessionId');
    }
}