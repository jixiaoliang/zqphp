<?php
return [

    //默认PHP类名
    'Sort' => 'index',

    //方法名  访问格式后面带/的则自动访问此方法
    'Method' => 'main',

    //对外开放类名加前缀 (可为空)
    'SortPrefix' => '',

    //推荐对外开放方法名加前缀 (可为空)
    'MethodPrefix' => '',

    //项目内的PHP文件夹,Lib第一层文件不用命名空间，第二层起要加命名空间
    'PhpDir' => 'Lib',

    // 项目内的视图文件夹,加载Lib文件不存在时加载此目录
    'ViewDir' => 'View',

    //视图文件格式
    'ViewFormat' => 'php',

    //访问默认支持格式,前面|后面多一个逗号表示支持空格式
    'Format' => 'php,html,',

    //加载php文件不存在或者类方法不存在是否加载View文件(通过访问ptah加载)
    'IsView' => true,

    //项目方法文件夹
    'PublicDir' => 'Public',

    //临时目录(生成静态，错误日志、编译PHP都在此目录)
    'TempDir' => 'temp',

    //编译PHP模板目录
    'TemplateDir' => 'Template',

    //编译PHP模板有效时间(false调试每次更新,true长期有效,数值为多久更新1次)
    'TemplateTime' => false,

    //模板左边符号 (如有报错就是不能使用此符号)
    'LeftSign' => '{',

    //模板右边符号(如有报错就是不能使用此符号)
    'RightSign' => '}',

    //Css JS中使用符号 ，JS CSS中这样使用{@$aa} {@$$bb}
    'SignLeft' => '#',

    //生成静态目录
    'HtmlDir' => 'html',

    //静态有效时间/秒 ,false关闭模式,true长期有效,数值为多久更新1次
    'HtmlTime' => false,

    //调试开关,开启后，不保存日志，页面内不输出错误 (CLi屏蔽此功能)
    'DebugSwitch' => true,

    //true=详细报错，false=代号报错(请看zpphp/Config/ErrInfo.php的Code)，null=404报错，string=网址跳转
    'Error' => true,

    //404报错时显示的页面，默认在项目/Config (参数$Code报错代号，可以文件中调用)
    //如在第一位加上：@=zqphp框架目录，#=启动目录，~=项目目录
    'ErrHtml' => 'Error.html',

    //错误日志目录 名称为：general=普通错误  system=代码错误  program=程序访问错误
    'LogDir' => 'log',

    //错误日志开关
    'IsLog' => true,

    //错误日志大小,超过此值生成新文件
    'LogSize' => 1024,

    //参数分隔符
    'Parameter' => '_',

    //路由方式GET POST  HEAD(选择多个半角逗号分开)
    'Routing' => 'GET,POST,HEAD',

    //(POST,HEAD)访问参数string半角逗号分开place类名,matter方法名
    'CallField' => 'place, matter',

    //添加静态文件支持请在Config/Mime.Types中添加格式(也可以在项目中创建Config/Mime.Types)
    //Worker 方式启动，可以设置静态目录
    //路径第一位 @=zqphp框架目录，#=启动目录，~=项目目录
    'StaticDir' => '#',

    // Worker 方式启动下载格式，访问静态文件时，那些格式可直接发下载
    'DownFormat' => 'doc,exe,apk,zip,rar,mp4',

    // Worker 方式启动文件流格式，访问静态文件时，那些格式可文件流(Safari浏览器只支持流播放)
    'StreamFormat' => 'mp3'
];