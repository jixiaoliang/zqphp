<?php
return [
    'title' => [
        'LoadError' => [
            'code' => 0,
            'title' => '系统错误',
            'info' => 'system error'
        ],
        'UrlFormat' => [
            'code' => 1,
            'title' => '访问格式错误',
            'info' => 'Access format error'
        ],
        'StopDirClass' => [
            'code' => 2,
            'title' => '对外禁止文件夹访问,禁止类访问',
            'info' => 'No Access'
        ],
        'NotFile' => [
            'code' => 3,
            'title' => '加载PHP文件不存在',
            'info' => 'Loading PHP file does not exist'
        ],
        'ErrClass' => [
            'code' => 4,
            'title' => '加载类或者方法不存在',
            'info' => 'Loading class does not exist'
        ],
        'NotViewFile' => [
            'code' => 5,
            'title' => '加载视图文件不存在',
            'info' => 'Loading view file does not exist'
        ],
        'SendFile' => [
            'code' => 6,
            'title' => '发送文件不存在',
            'info' => 'File does not exist'
        ],
        'ErrMime' => [
            'code' => 7,
            'title' => 'Mime类型不存在',
            'info' => 'Mime type does not exist'
        ]
    ]
];