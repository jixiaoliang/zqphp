<?php if (empty($Data)) return;?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>
<style>
    .content {
        position: fixed;
        bottom: 0;
        right: 0;
        font-size: 14px;
        width: 100%;
        z-index: 999999;
        color: #000;
        text-align: left;
        font-family: '微软雅黑';
    }

    #tracetab {
        display: none;
        background: white;
        margin: 0;
        height: 250px;
    }

    #tracetit {
        border-bottom: 1px solid #ececec;
        border-top: 1px solid #ececec;
        font-size: 16px;
    }

    #tracetit span {
        color: #000;
        padding-left: 18px;
        height: 30px;
        line-height: 30px;
        display: inline-block;
        margin-right: 3px;
        cursor: pointer;
        font-weight: 700;
    }

    #tracetitcont {
        overflow: auto;
        height: 212px;
        padding: 0;
        line-height: 24px;
    }

    #tracetitcont div {
        display: none;;
    }

    #tracetitcont div ul {
        padding: 0;
        margin: 0;
    }

    #tracetitcont div ul li {
        border-bottom: 1px solid #EEE;
        font-size: 14px;
        padding: 0 12px;
    }

    #traceclose {
        display: none;
        text-align: left;
        height: 30px;
        position: absolute;
        top: -30px;
        left: 2px;
        cursor: pointer;
    }

    #traceclose span {
        color: #F00;
        padding-right: 12px;
        height: 30px;
        line-height: 30px;
        display: inline-block;
        margin-right: 3px;
        cursor: pointer;
        font-weight: 700;
        font-size: 25px;
    }

    #traceopen {
        height: 30px;
        float: left;
        text-align: left;
        overflow: hidden;
        position: fixed;
        bottom: 0;
        left: 0;
        color: #000;
        line-height: 30px;
        cursor: pointer;
    }

    #traceopen div {
        background: #190aec;
        color: #FFF;
        padding: 0 6px;
        float: left;
        line-height: 30px;
        font-size: 14px;
    }
</style>
<body>
<div class="content">
    <div id="tracetab">
        <div id="tracetit">
            <span>基本</span>
            <span>文件<?php echo $Data['Amount'] ? '(' . $Data['Amount'] . ')' : '' ?></span>
            <span>SQL</span>
        </div>
        <div id="tracetitcont">
            <div>
                <ul>
                    <?php
                    echo ' <li>请求信息 : ' . $Data['Info'] . '</li>';
                    echo ' <li>运行时间 : ' . $Data['LoadTime'] . 's&nbsp;&nbsp;吞吐率 : ' . $Data['Req'] . 'req/s&nbsp;&nbsp;内存消耗 : ' . $Data['Ram'] . '</li>';
                    if (!empty($LoadFile = self::IsArrData($Data['LoadMode'], 0))) {
                        echo '<li>项目文件 : ' . $LoadFile . '</li>';
                    }
                    if (!empty($Sort = self::IsArrData($Data['LoadMode'], 1))) {
                        echo '<li>加载类名 : ' . $Sort . (!empty($Method = self::IsArrData($Data['LoadMode'], 2)) ? '&nbsp;&nbsp;加载函数 : ' . $Method : '') . ($Data['Process'] ? '&nbsp;&nbsp;路由别名 : ' . $Data['Process'] : '') . '</li>';
                    }
                    if (isset($ErrorData['message'])) {
                        echo '<li><span style="color:#F00">错误提示 : ' . $ErrorData['message'].'&nbsp;&nbsp;' . (isset($ErrorData['file']) ? '[' . $ErrorData['file'] . '] : ' . $ErrorData['line'] : '') . '</span></li>';
                    }
                    echo ' <li>HEADER : ' . $Data['Header'] . '</li>';
                    ?>
                </ul>
            </div>
            <div>
                <ul>
                    <?php
                    foreach ($Data['FileList'] as $v) {
                        echo '<li>' . $v . '</li>';
                    }
                    ?>
            </div>
            <div>
                <ul>
                    <li>ZqphpMysql开发中</li>
                </ul>
            </div>
        </div>
    </div>
    <div id="traceclose"><span>×</span></div>
</div>
<div id="traceopen">
    <div><?php echo $Data['LoadTime'] ?></div>
</div>
<script type="text/javascript">
    (function () {
        var tab_tit = document.getElementById('tracetit').getElementsByTagName('span');
        var tab_cont = document.getElementById('tracetitcont').getElementsByTagName('div');
        var open = document.getElementById('traceopen');
        var close = document.getElementById('traceclose').children[0];
        var trace = document.getElementById('tracetab');
        var cookie = document.cookie.match(/showpagetrace=(\d\|\d)/);
        var history = (cookie && typeof cookie[1] != 'undefined' && cookie[1].split('|')) || [0, 0];
        open.onclick = function () {
            trace.style.display = 'block';
            this.style.display = 'none';
            close.parentNode.style.display = 'block';
            history[0] = 1;
            document.cookie = 'showpagetrace=' + history.join('|')
        }
        close.onclick = function () {
            trace.style.display = 'none';
            this.parentNode.style.display = 'none';
            open.style.display = 'block';
            history[0] = 0;
            document.cookie = 'showpagetrace=' + history.join('|')
        }
        for (var i = 0; i < tab_tit.length; i++) {
            tab_tit[i].onclick = (function (i) {
                return function () {
                    for (var j = 0; j < tab_cont.length; j++) {
                        tab_cont[j].style.display = 'none';
                        tab_tit[j].style.color = '#999';
                    }
                    tab_cont[i].style.display = 'block';
                    tab_tit[i].style.color = '#000';
                    history[1] = i;
                    document.cookie = 'showpagetrace=' + history.join('|')
                }
            })(i)
        }
        parseInt(history[0]) && open.click();
        tab_tit[history[1]].click();
    })();
</script>
</body>
</html>