1. 帮助文档：[http://zqphp.zhqing.com](http://zqphp.zhqing.com)
2. zqphp结合workerman从4.x版本开发。
3.  支持Workerman Apache Nginx iis  Cli 访问
*****
 简单启动例
```
<?php
//引加文件
require __DIR__ . '/zqphp/AutoLoad.php';

$Obj = new \zqphp\AutoLoad(__DIR__ . '/application');

$Obj->Run('http://0.0.0.0:8080');
```
*****
创建文件application/Lib/index.php，可通过/index/main访问
```
<?php
class index{
     public function main() {
         echo 'Hello';
     }
}
```